/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "IMPORTATION_REPORT", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImportationReport.findAll", query = "SELECT i FROM ImportationReport i"),
    @NamedQuery(name = "ImportationReport.findById", query = "SELECT i FROM ImportationReport i WHERE i.id = :id"),
    @NamedQuery(name = "ImportationReport.findByCreatedDate", query = "SELECT i FROM ImportationReport i WHERE i.createdDate = :createdDate"),
    @NamedQuery(name = "ImportationReport.findByNote", query = "SELECT i FROM ImportationReport i WHERE i.note = :note")})
public class ImportationReport implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(nullable = false, length = 1000)
    private String note;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "importationReportId")
    private List<ProductImportationReport> productImportationReportList;
    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;

    public ImportationReport() {
    }

    public ImportationReport(BigDecimal id) {
        this.id = id;
    }

    public ImportationReport(BigDecimal id, Date createdDate, String note) {
        this.id = id;
        this.createdDate = createdDate;
        this.note = note;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @XmlTransient
    public List<ProductImportationReport> getProductImportationReportList() {
        return productImportationReportList;
    }

    public void setProductImportationReportList(List<ProductImportationReport> productImportationReportList) {
        this.productImportationReportList = productImportationReportList;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportationReport)) {
            return false;
        }
        ImportationReport other = (ImportationReport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ImportationReport[ id=" + id + " ]";
    }
    
}
