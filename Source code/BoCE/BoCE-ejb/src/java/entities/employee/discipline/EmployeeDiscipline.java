/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.employee.discipline;

import entities.employee.Employee;
import entities.employee.discipline.Discipline;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "EMPLOYEE_DISCIPLINE", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeeDiscipline.findAll", query = "SELECT e FROM EmployeeDiscipline e"),
    @NamedQuery(name = "EmployeeDiscipline.findById", query = "SELECT e FROM EmployeeDiscipline e WHERE e.id = :id"),
    @NamedQuery(name = "EmployeeDiscipline.findByDisciplineDate", query = "SELECT e FROM EmployeeDiscipline e WHERE e.disciplineDate = :disciplineDate"),
    @NamedQuery(name = "EmployeeDiscipline.findByDescription", query = "SELECT e FROM EmployeeDiscipline e WHERE e.description = :description"),
    @NamedQuery(name = "EmployeeDiscipline.findByForfeit", query = "SELECT e FROM EmployeeDiscipline e WHERE e.forfeit = :forfeit")})
public class EmployeeDiscipline implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DISCIPLINE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date disciplineDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(nullable = false, length = 1000)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double forfeit;
    @JoinColumn(name = "DISCIPLINE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Discipline disciplineId;
    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;

    public EmployeeDiscipline() {
    }

    public EmployeeDiscipline(BigDecimal id) {
        this.id = id;
    }

    public EmployeeDiscipline(BigDecimal id, Date disciplineDate, String description, double forfeit) {
        this.id = id;
        this.disciplineDate = disciplineDate;
        this.description = description;
        this.forfeit = forfeit;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getDisciplineDate() {
        return disciplineDate;
    }

    public void setDisciplineDate(Date disciplineDate) {
        this.disciplineDate = disciplineDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getForfeit() {
        return forfeit;
    }

    public void setForfeit(double forfeit) {
        this.forfeit = forfeit;
    }

    public Discipline getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(Discipline disciplineId) {
        this.disciplineId = disciplineId;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeDiscipline)) {
            return false;
        }
        EmployeeDiscipline other = (EmployeeDiscipline) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.EmployeeDiscipline[ id=" + id + " ]";
    }
    
}
