/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.employee;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salary.findAll", query = "SELECT s FROM Salary s"),
    @NamedQuery(name = "Salary.findBySalaryLevel", query = "SELECT s FROM Salary s WHERE s.salaryLevel = :salaryLevel"),
    @NamedQuery(name = "Salary.findByBasicSalary", query = "SELECT s FROM Salary s WHERE s.basicSalary = :basicSalary"),
    @NamedQuery(name = "Salary.findBySalaryRate", query = "SELECT s FROM Salary s WHERE s.salaryRate = :salaryRate"),
    @NamedQuery(name = "Salary.findByAllowanceRate", query = "SELECT s FROM Salary s WHERE s.allowanceRate = :allowanceRate")})
public class Salary implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_LEVEL", nullable = false, precision = 38, scale = 0)
    private BigDecimal salaryLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BASIC_SALARY", nullable = false)
    private double basicSalary;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_RATE", nullable = false)
    private double salaryRate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ALLOWANCE_RATE", nullable = false)
    private double allowanceRate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salaryLevel")
    private List<Position> positionList;

    public Salary() {
    }

    public Salary(BigDecimal salaryLevel) {
        this.salaryLevel = salaryLevel;
    }

    public Salary(BigDecimal salaryLevel, double basicSalary, double salaryRate, double allowanceRate) {
        this.salaryLevel = salaryLevel;
        this.basicSalary = basicSalary;
        this.salaryRate = salaryRate;
        this.allowanceRate = allowanceRate;
    }

    public BigDecimal getSalaryLevel() {
        return salaryLevel;
    }

    public void setSalaryLevel(BigDecimal salaryLevel) {
        this.salaryLevel = salaryLevel;
    }

    public double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(double basicSalary) {
        this.basicSalary = basicSalary;
    }

    public double getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryRate(double salaryRate) {
        this.salaryRate = salaryRate;
    }

    public double getAllowanceRate() {
        return allowanceRate;
    }

    public void setAllowanceRate(double allowanceRate) {
        this.allowanceRate = allowanceRate;
    }

    @XmlTransient
    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salaryLevel != null ? salaryLevel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salary)) {
            return false;
        }
        Salary other = (Salary) object;
        if ((this.salaryLevel == null && other.salaryLevel != null) || (this.salaryLevel != null && !this.salaryLevel.equals(other.salaryLevel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Salary[ salaryLevel=" + salaryLevel + " ]";
    }
    
}
