/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.transaction.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "PURCHASE_METHOD", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PurchaseMethod.findAll", query = "SELECT p FROM PurchaseMethod p"),
    @NamedQuery(name = "PurchaseMethod.findById", query = "SELECT p FROM PurchaseMethod p WHERE p.id = :id"),
    @NamedQuery(name = "PurchaseMethod.findByPurchaseMethod", query = "SELECT p FROM PurchaseMethod p WHERE p.purchaseMethod = :purchaseMethod")})
public class PurchaseMethod implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "PURCHASE_METHOD", nullable = false, length = 500)
    private String purchaseMethod;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purchaseMethodId")
    private List<Orders> ordersList;

    public PurchaseMethod() {
    }

    public PurchaseMethod(BigDecimal id) {
        this.id = id;
    }

    public PurchaseMethod(BigDecimal id, String purchaseMethod) {
        this.id = id;
        this.purchaseMethod = purchaseMethod;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    @XmlTransient
    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseMethod)) {
            return false;
        }
        PurchaseMethod other = (PurchaseMethod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PurchaseMethod[ id=" + id + " ]";
    }
    
}
