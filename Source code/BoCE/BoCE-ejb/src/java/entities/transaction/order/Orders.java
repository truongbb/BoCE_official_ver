/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.transaction.order;

import entities.transaction.bill.OnlineBill;
import entities.customer.Customer;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o"),
    @NamedQuery(name = "Orders.findById", query = "SELECT o FROM Orders o WHERE o.id = :id"),
    @NamedQuery(name = "Orders.findByAdditionalCharge", query = "SELECT o FROM Orders o WHERE o.additionalCharge = :additionalCharge"),
    @NamedQuery(name = "Orders.findByCash", query = "SELECT o FROM Orders o WHERE o.cash = :cash"),
    @NamedQuery(name = "Orders.findByReceivingAddress", query = "SELECT o FROM Orders o WHERE o.receivingAddress = :receivingAddress"),
    @NamedQuery(name = "Orders.findByExpextedReceivingDate", query = "SELECT o FROM Orders o WHERE o.expextedReceivingDate = :expextedReceivingDate")})
public class Orders implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADDITIONAL_CHARGE", nullable = false)
    private double additionalCharge;
    
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double cash;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "RECEIVING_ADDRESS", nullable = false, length = 500)
    private String receivingAddress;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "EXPEXTED_RECEIVING_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expextedReceivingDate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderId")
    private List<OnlineBill> onlineBillList;
    
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Customer customerId;
    
    @JoinColumn(name = "PURCHASE_METHOD_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private PurchaseMethod purchaseMethodId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ordersId")
    private List<OrdersProduct> ordersProductList;

    public Orders() {
    }

    public Orders(BigDecimal id) {
        this.id = id;
    }

    public Orders(BigDecimal id, double additionalCharge, double cash, String receivingAddress, Date expextedReceivingDate) {
        this.id = id;
        this.additionalCharge = additionalCharge;
        this.cash = cash;
        this.receivingAddress = receivingAddress;
        this.expextedReceivingDate = expextedReceivingDate;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public double getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(double additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public String getReceivingAddress() {
        return receivingAddress;
    }

    public void setReceivingAddress(String receivingAddress) {
        this.receivingAddress = receivingAddress;
    }

    public Date getExpextedReceivingDate() {
        return expextedReceivingDate;
    }

    public void setExpextedReceivingDate(Date expextedReceivingDate) {
        this.expextedReceivingDate = expextedReceivingDate;
    }

    @XmlTransient
    public List<OnlineBill> getOnlineBillList() {
        return onlineBillList;
    }

    public void setOnlineBillList(List<OnlineBill> onlineBillList) {
        this.onlineBillList = onlineBillList;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public PurchaseMethod getPurchaseMethodId() {
        return purchaseMethodId;
    }

    public void setPurchaseMethodId(PurchaseMethod purchaseMethodId) {
        this.purchaseMethodId = purchaseMethodId;
    }

    @XmlTransient
    public List<OrdersProduct> getOrdersProductList() {
        return ordersProductList;
    }

    public void setOrdersProductList(List<OrdersProduct> ordersProductList) {
        this.ordersProductList = ordersProductList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Orders[ id=" + id + " ]";
    }
    
}
