/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.transaction.bill;

import entities.product.Product;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "OFFLINE_BILL_PRODUCT", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfflineBillProduct.findAll", query = "SELECT o FROM OfflineBillProduct o"),
    @NamedQuery(name = "OfflineBillProduct.findById", query = "SELECT o FROM OfflineBillProduct o WHERE o.id = :id"),
    @NamedQuery(name = "OfflineBillProduct.findByQuantity", query = "SELECT o FROM OfflineBillProduct o WHERE o.quantity = :quantity")})
public class OfflineBillProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private BigInteger quantity;
    @JoinColumn(name = "OFFLINE_BILL_ID", referencedColumnName = "OFFLINE_BILL_ID", nullable = false)
    @ManyToOne(optional = false)
    private OfflineBill offlineBillId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @ManyToOne(optional = false)
    private Product productId;

    public OfflineBillProduct() {
    }

    public OfflineBillProduct(BigDecimal id) {
        this.id = id;
    }

    public OfflineBillProduct(BigDecimal id, BigInteger quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }

    public OfflineBill getOfflineBillId() {
        return offlineBillId;
    }

    public void setOfflineBillId(OfflineBill offlineBillId) {
        this.offlineBillId = offlineBillId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfflineBillProduct)) {
            return false;
        }
        OfflineBillProduct other = (OfflineBillProduct) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OfflineBillProduct[ id=" + id + " ]";
    }
    
}
