/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.transaction.bill;

import entities.transaction.order.Orders;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "ONLINE_BILL", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OnlineBill.findAll", query = "SELECT o FROM OnlineBill o"),
    @NamedQuery(name = "OnlineBill.findByOnlineBillId", query = "SELECT o FROM OnlineBill o WHERE o.onlineBillId = :onlineBillId"),
    @NamedQuery(name = "OnlineBill.findByReceivedDate", query = "SELECT o FROM OnlineBill o WHERE o.receivedDate = :receivedDate")})
public class OnlineBill implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ONLINE_BILL_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal onlineBillId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RECEIVED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedDate;
    @JoinColumn(name = "BILL_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Bill billId;
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Orders orderId;

    public OnlineBill() {
    }

    public OnlineBill(BigDecimal onlineBillId) {
        this.onlineBillId = onlineBillId;
    }

    public OnlineBill(BigDecimal onlineBillId, Date receivedDate) {
        this.onlineBillId = onlineBillId;
        this.receivedDate = receivedDate;
    }

    public BigDecimal getOnlineBillId() {
        return onlineBillId;
    }

    public void setOnlineBillId(BigDecimal onlineBillId) {
        this.onlineBillId = onlineBillId;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Bill getBillId() {
        return billId;
    }

    public void setBillId(Bill billId) {
        this.billId = billId;
    }

    public Orders getOrderId() {
        return orderId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (onlineBillId != null ? onlineBillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OnlineBill)) {
            return false;
        }
        OnlineBill other = (OnlineBill) object;
        if ((this.onlineBillId == null && other.onlineBillId != null) || (this.onlineBillId != null && !this.onlineBillId.equals(other.onlineBillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OnlineBill[ onlineBillId=" + onlineBillId + " ]";
    }
    
}
