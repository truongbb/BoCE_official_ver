/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PRODUCT_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b"),
    @NamedQuery(name = "Book.findByBookId", query = "SELECT b FROM Book b WHERE b.bookId = :bookId"),
    @NamedQuery(name = "Book.findByName", query = "SELECT b FROM Book b WHERE b.name = :name"),
    @NamedQuery(name = "Book.findByPublishedYear", query = "SELECT b FROM Book b WHERE b.publishedYear = :publishedYear"),
    @NamedQuery(name = "Book.findByTotalPage", query = "SELECT b FROM Book b WHERE b.totalPage = :totalPage")})
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "BOOK_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal bookId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(nullable = false, length = 500)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PUBLISHED_YEAR", nullable = false)
    private BigInteger publishedYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_PAGE", nullable = false)
    private BigInteger totalPage;
    @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Author authorId;
    @JoinColumn(name = "BOOK_CATEGORY_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private BookCategory bookCategoryId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @OneToOne(optional = false)
    private Product productId;
    @JoinColumn(name = "PUBLISHER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Publisher publisherId;

    public Book() {
    }

    public Book(BigDecimal bookId) {
        this.bookId = bookId;
    }

    public Book(BigDecimal bookId, String name, BigInteger publishedYear, BigInteger totalPage) {
        this.bookId = bookId;
        this.name = name;
        this.publishedYear = publishedYear;
        this.totalPage = totalPage;
    }

    public BigDecimal getBookId() {
        return bookId;
    }

    public void setBookId(BigDecimal bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(BigInteger publishedYear) {
        this.publishedYear = publishedYear;
    }

    public BigInteger getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(BigInteger totalPage) {
        this.totalPage = totalPage;
    }

    public Author getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Author authorId) {
        this.authorId = authorId;
    }

    public BookCategory getBookCategoryId() {
        return bookCategoryId;
    }

    public void setBookCategoryId(BookCategory bookCategoryId) {
        this.bookCategoryId = bookCategoryId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Publisher getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Publisher publisherId) {
        this.publisherId = publisherId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookId != null ? bookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.bookId == null && other.bookId != null) || (this.bookId != null && !this.bookId.equals(other.bookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Book[ bookId=" + bookId + " ]";
    }
    
}
