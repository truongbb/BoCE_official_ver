/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "PRODUCT_IMPORTATION_REPORT", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductImportationReport.findAll", query = "SELECT p FROM ProductImportationReport p"),
    @NamedQuery(name = "ProductImportationReport.findById", query = "SELECT p FROM ProductImportationReport p WHERE p.id = :id"),
    @NamedQuery(name = "ProductImportationReport.findByImportedQuantity", query = "SELECT p FROM ProductImportationReport p WHERE p.importedQuantity = :importedQuantity"),
    @NamedQuery(name = "ProductImportationReport.findByUnitPrice", query = "SELECT p FROM ProductImportationReport p WHERE p.unitPrice = :unitPrice")})
public class ProductImportationReport implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPORTED_QUANTITY", nullable = false)
    private BigInteger importedQuantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UNIT_PRICE", nullable = false)
    private double unitPrice;
    @JoinColumn(name = "IMPORTATION_REPORT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ImportationReport importationReportId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @ManyToOne(optional = false)
    private Product productId;
    @JoinColumn(name = "SUPPLIER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Supplier supplierId;
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Unit unitId;

    public ProductImportationReport() {
    }

    public ProductImportationReport(BigDecimal id) {
        this.id = id;
    }

    public ProductImportationReport(BigDecimal id, BigInteger importedQuantity, double unitPrice) {
        this.id = id;
        this.importedQuantity = importedQuantity;
        this.unitPrice = unitPrice;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getImportedQuantity() {
        return importedQuantity;
    }

    public void setImportedQuantity(BigInteger importedQuantity) {
        this.importedQuantity = importedQuantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public ImportationReport getImportationReportId() {
        return importationReportId;
    }

    public void setImportationReportId(ImportationReport importationReportId) {
        this.importationReportId = importationReportId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Supplier getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Supplier supplierId) {
        this.supplierId = supplierId;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductImportationReport)) {
            return false;
        }
        ProductImportationReport other = (ProductImportationReport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductImportationReport[ id=" + id + " ]";
    }
    
}
