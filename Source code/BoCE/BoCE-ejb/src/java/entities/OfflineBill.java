/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "OFFLINE_BILL", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfflineBill.findAll", query = "SELECT o FROM OfflineBill o"),
    @NamedQuery(name = "OfflineBill.findByOfflineBillId", query = "SELECT o FROM OfflineBill o WHERE o.offlineBillId = :offlineBillId"),
    @NamedQuery(name = "OfflineBill.findByAdditionalCharge", query = "SELECT o FROM OfflineBill o WHERE o.additionalCharge = :additionalCharge"),
    @NamedQuery(name = "OfflineBill.findByCash", query = "SELECT o FROM OfflineBill o WHERE o.cash = :cash")})
public class OfflineBill implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "OFFLINE_BILL_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal offlineBillId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADDITIONAL_CHARGE", nullable = false)
    private double additionalCharge;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double cash;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offlineBillId")
    private List<OfflineBillProduct> offlineBillProductList;
    @JoinColumn(name = "BILL_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Bill billId;

    public OfflineBill() {
    }

    public OfflineBill(BigDecimal offlineBillId) {
        this.offlineBillId = offlineBillId;
    }

    public OfflineBill(BigDecimal offlineBillId, double additionalCharge, double cash) {
        this.offlineBillId = offlineBillId;
        this.additionalCharge = additionalCharge;
        this.cash = cash;
    }

    public BigDecimal getOfflineBillId() {
        return offlineBillId;
    }

    public void setOfflineBillId(BigDecimal offlineBillId) {
        this.offlineBillId = offlineBillId;
    }

    public double getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(double additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    @XmlTransient
    public List<OfflineBillProduct> getOfflineBillProductList() {
        return offlineBillProductList;
    }

    public void setOfflineBillProductList(List<OfflineBillProduct> offlineBillProductList) {
        this.offlineBillProductList = offlineBillProductList;
    }

    public Bill getBillId() {
        return billId;
    }

    public void setBillId(Bill billId) {
        this.billId = billId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (offlineBillId != null ? offlineBillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfflineBill)) {
            return false;
        }
        OfflineBill other = (OfflineBill) object;
        if ((this.offlineBillId == null && other.offlineBillId != null) || (this.offlineBillId != null && !this.offlineBillId.equals(other.offlineBillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OfflineBill[ offlineBillId=" + offlineBillId + " ]";
    }
    
}
