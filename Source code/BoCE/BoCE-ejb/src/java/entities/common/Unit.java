/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.common;

import entities.exportation.ProductExportationReport;
import entities.importation.ProductImportationReport;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unit.findAll", query = "SELECT u FROM Unit u"),
    @NamedQuery(name = "Unit.findById", query = "SELECT u FROM Unit u WHERE u.id = :id"),
    @NamedQuery(name = "Unit.findByValue", query = "SELECT u FROM Unit u WHERE u.value = :value")})
public class Unit implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String value;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitId")
    private List<ProductImportationReport> productImportationReportList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitId")
    private List<ProductExportationReport> productExportationReportList;

    public Unit() {
    }

    public Unit(BigDecimal id) {
        this.id = id;
    }

    public Unit(BigDecimal id, String value) {
        this.id = id;
        this.value = value;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @XmlTransient
    public List<ProductImportationReport> getProductImportationReportList() {
        return productImportationReportList;
    }

    public void setProductImportationReportList(List<ProductImportationReport> productImportationReportList) {
        this.productImportationReportList = productImportationReportList;
    }

    @XmlTransient
    public List<ProductExportationReport> getProductExportationReportList() {
        return productExportationReportList;
    }

    public void setProductExportationReportList(List<ProductExportationReport> productExportationReportList) {
        this.productExportationReportList = productExportationReportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unit)) {
            return false;
        }
        Unit other = (Unit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Unit[ id=" + id + " ]";
    }
    
}
