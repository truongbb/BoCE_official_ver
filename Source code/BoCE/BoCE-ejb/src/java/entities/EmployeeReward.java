/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "EMPLOYEE_REWARD", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeeReward.findAll", query = "SELECT e FROM EmployeeReward e"),
    @NamedQuery(name = "EmployeeReward.findById", query = "SELECT e FROM EmployeeReward e WHERE e.id = :id"),
    @NamedQuery(name = "EmployeeReward.findByRewardDate", query = "SELECT e FROM EmployeeReward e WHERE e.rewardDate = :rewardDate"),
    @NamedQuery(name = "EmployeeReward.findByDescription", query = "SELECT e FROM EmployeeReward e WHERE e.description = :description"),
    @NamedQuery(name = "EmployeeReward.findByBonus", query = "SELECT e FROM EmployeeReward e WHERE e.bonus = :bonus")})
public class EmployeeReward implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REWARD_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date rewardDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(nullable = false, length = 1000)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double bonus;
    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;
    @JoinColumn(name = "REWARD_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Reward rewardId;

    public EmployeeReward() {
    }

    public EmployeeReward(BigDecimal id) {
        this.id = id;
    }

    public EmployeeReward(BigDecimal id, Date rewardDate, String description, double bonus) {
        this.id = id;
        this.rewardDate = rewardDate;
        this.description = description;
        this.bonus = bonus;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getRewardDate() {
        return rewardDate;
    }

    public void setRewardDate(Date rewardDate) {
        this.rewardDate = rewardDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public Reward getRewardId() {
        return rewardId;
    }

    public void setRewardId(Reward rewardId) {
        this.rewardId = rewardId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeReward)) {
            return false;
        }
        EmployeeReward other = (EmployeeReward) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.EmployeeReward[ id=" + id + " ]";
    }
    
}
