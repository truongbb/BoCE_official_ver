/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "CLOTHES_SIZE", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClothesSize.findAll", query = "SELECT c FROM ClothesSize c"),
    @NamedQuery(name = "ClothesSize.findById", query = "SELECT c FROM ClothesSize c WHERE c.id = :id"),
    @NamedQuery(name = "ClothesSize.findByClothesSize", query = "SELECT c FROM ClothesSize c WHERE c.clothesSize = :clothesSize")})
public class ClothesSize implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CLOTHES_SIZE", nullable = false, length = 10)
    private String clothesSize;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clothesSizeId")
    private List<Clothes> clothesList;

    public ClothesSize() {
    }

    public ClothesSize(BigDecimal id) {
        this.id = id;
    }

    public ClothesSize(BigDecimal id, String clothesSize) {
        this.id = id;
        this.clothesSize = clothesSize;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClothesSize() {
        return clothesSize;
    }

    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize;
    }

    @XmlTransient
    public List<Clothes> getClothesList() {
        return clothesList;
    }

    public void setClothesList(List<Clothes> clothesList) {
        this.clothesList = clothesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClothesSize)) {
            return false;
        }
        ClothesSize other = (ClothesSize) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ClothesSize[ id=" + id + " ]";
    }
    
}
