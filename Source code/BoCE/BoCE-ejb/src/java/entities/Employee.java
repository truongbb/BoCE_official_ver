/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PERSON_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findById", query = "SELECT e FROM Employee e WHERE e.id = :id"),
    @NamedQuery(name = "Employee.findByStartWorkingDate", query = "SELECT e FROM Employee e WHERE e.startWorkingDate = :startWorkingDate")})
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_WORKING_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startWorkingDate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private List<EmployeeDiscipline> employeeDisciplineList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private List<Bill> billList;
    
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Department departmentId;
    
    @JoinColumn(name = "ETHNIC_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Ethnic ethnicId;
    
    @JoinColumn(name = "LITERACY_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Literacy literacyId;
    
    @JoinColumn(name = "PERSON_ID", referencedColumnName = "ID", nullable = false)
    @OneToOne(optional = false)
    private Person personId;
    
    @JoinColumn(name = "POSITION_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Position positionId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private List<ImportationReport> importationReportList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private List<ExportationReport> exportationReportList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private List<EmployeeReward> employeeRewardList;

    public Employee() {
    }

    public Employee(BigDecimal id) {
        this.id = id;
    }

    public Employee(BigDecimal id, Date startWorkingDate) {
        this.id = id;
        this.startWorkingDate = startWorkingDate;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getStartWorkingDate() {
        return startWorkingDate;
    }

    public void setStartWorkingDate(Date startWorkingDate) {
        this.startWorkingDate = startWorkingDate;
    }

    @XmlTransient
    public List<EmployeeDiscipline> getEmployeeDisciplineList() {
        return employeeDisciplineList;
    }

    public void setEmployeeDisciplineList(List<EmployeeDiscipline> employeeDisciplineList) {
        this.employeeDisciplineList = employeeDisciplineList;
    }

    @XmlTransient
    public List<Bill> getBillList() {
        return billList;
    }

    public void setBillList(List<Bill> billList) {
        this.billList = billList;
    }

    public Department getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Department departmentId) {
        this.departmentId = departmentId;
    }

    public Ethnic getEthnicId() {
        return ethnicId;
    }

    public void setEthnicId(Ethnic ethnicId) {
        this.ethnicId = ethnicId;
    }

    public Literacy getLiteracyId() {
        return literacyId;
    }

    public void setLiteracyId(Literacy literacyId) {
        this.literacyId = literacyId;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }

    public Position getPositionId() {
        return positionId;
    }

    public void setPositionId(Position positionId) {
        this.positionId = positionId;
    }

    @XmlTransient
    public List<ImportationReport> getImportationReportList() {
        return importationReportList;
    }

    public void setImportationReportList(List<ImportationReport> importationReportList) {
        this.importationReportList = importationReportList;
    }

    @XmlTransient
    public List<ExportationReport> getExportationReportList() {
        return exportationReportList;
    }

    public void setExportationReportList(List<ExportationReport> exportationReportList) {
        this.exportationReportList = exportationReportList;
    }

    @XmlTransient
    public List<EmployeeReward> getEmployeeRewardList() {
        return employeeRewardList;
    }

    public void setEmployeeRewardList(List<EmployeeReward> employeeRewardList) {
        this.employeeRewardList = employeeRewardList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Employee[ id=" + id + " ]";
    }
    
}
