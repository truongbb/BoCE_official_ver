/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.exportation;

import entities.product.Product;
import entities.common.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "PRODUCT_EXPORTATION_REPORT", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductExportationReport.findAll", query = "SELECT p FROM ProductExportationReport p"),
    @NamedQuery(name = "ProductExportationReport.findById", query = "SELECT p FROM ProductExportationReport p WHERE p.id = :id"),
    @NamedQuery(name = "ProductExportationReport.findByExportedQuantity", query = "SELECT p FROM ProductExportationReport p WHERE p.exportedQuantity = :exportedQuantity"),
    @NamedQuery(name = "ProductExportationReport.findByUnitPrice", query = "SELECT p FROM ProductExportationReport p WHERE p.unitPrice = :unitPrice")})
public class ProductExportationReport implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EXPORTED_QUANTITY", nullable = false)
    private BigInteger exportedQuantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UNIT_PRICE", nullable = false)
    private double unitPrice;
    @JoinColumn(name = "EXPORTED_REPORT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ExportationReport exportedReportId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @ManyToOne(optional = false)
    private Product productId;
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Unit unitId;

    public ProductExportationReport() {
    }

    public ProductExportationReport(BigDecimal id) {
        this.id = id;
    }

    public ProductExportationReport(BigDecimal id, BigInteger exportedQuantity, double unitPrice) {
        this.id = id;
        this.exportedQuantity = exportedQuantity;
        this.unitPrice = unitPrice;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getExportedQuantity() {
        return exportedQuantity;
    }

    public void setExportedQuantity(BigInteger exportedQuantity) {
        this.exportedQuantity = exportedQuantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public ExportationReport getExportedReportId() {
        return exportedReportId;
    }

    public void setExportedReportId(ExportationReport exportedReportId) {
        this.exportedReportId = exportedReportId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductExportationReport)) {
            return false;
        }
        ProductExportationReport other = (ProductExportationReport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductExportationReport[ id=" + id + " ]";
    }
    
}
