/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.product;

import entities.product.book.Book;
import entities.product.clothes.Clothes;
import entities.product.electronics.Electronics;
import entities.transaction.bill.OfflineBillProduct;
import entities.transaction.order.OrdersProduct;
import entities.exportation.ProductExportationReport;
import entities.importation.ProductImportationReport;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p"),
    @NamedQuery(name = "Product.findByProductId", query = "SELECT p FROM Product p WHERE p.productId = :productId"),
    @NamedQuery(name = "Product.findByUnitPrice", query = "SELECT p FROM Product p WHERE p.unitPrice = :unitPrice"),
    @NamedQuery(name = "Product.findByDescription", query = "SELECT p FROM Product p WHERE p.description = :description"),
    @NamedQuery(name = "Product.findByQuantity", query = "SELECT p FROM Product p WHERE p.quantity = :quantity"),
    @NamedQuery(name = "Product.findByPictureThumb", query = "SELECT p FROM Product p WHERE p.pictureThumb = :pictureThumb")})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal productId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UNIT_PRICE", nullable = false)
    private double unitPrice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(nullable = false, length = 2000)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private BigInteger quantity;
    @Size(max = 500)
    @Column(name = "PICTURE_THUMB", length = 500)
    private String pictureThumb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private List<ProductImportationReport> productImportationReportList;
    @JoinColumn(name = "DISCOUNT_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Discount discountId;
    @JoinColumn(name = "PRODUCT_TYPE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ProductType productTypeId;
    @JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Warehouse warehouseId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "productId")
    private Clothes clothes;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "productId")
    private Electronics electronics;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private List<OrdersProduct> ordersProductList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private List<OfflineBillProduct> offlineBillProductList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "productId")
    private Book book;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private List<ProductExportationReport> productExportationReportList;

    public Product() {
    }

    public Product(BigDecimal productId) {
        this.productId = productId;
    }

    public Product(BigDecimal productId, double unitPrice, String description, BigInteger quantity) {
        this.productId = productId;
        this.unitPrice = unitPrice;
        this.description = description;
        this.quantity = quantity;
    }

    public BigDecimal getProductId() {
        return productId;
    }

    public void setProductId(BigDecimal productId) {
        this.productId = productId;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }

    public String getPictureThumb() {
        return pictureThumb;
    }

    public void setPictureThumb(String pictureThumb) {
        this.pictureThumb = pictureThumb;
    }

    @XmlTransient
    public List<ProductImportationReport> getProductImportationReportList() {
        return productImportationReportList;
    }

    public void setProductImportationReportList(List<ProductImportationReport> productImportationReportList) {
        this.productImportationReportList = productImportationReportList;
    }

    public Discount getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Discount discountId) {
        this.discountId = discountId;
    }

    public ProductType getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(ProductType productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Warehouse getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Warehouse warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Clothes getClothes() {
        return clothes;
    }

    public void setClothes(Clothes clothes) {
        this.clothes = clothes;
    }

    public Electronics getElectronics() {
        return electronics;
    }

    public void setElectronics(Electronics electronics) {
        this.electronics = electronics;
    }

    @XmlTransient
    public List<OrdersProduct> getOrdersProductList() {
        return ordersProductList;
    }

    public void setOrdersProductList(List<OrdersProduct> ordersProductList) {
        this.ordersProductList = ordersProductList;
    }

    @XmlTransient
    public List<OfflineBillProduct> getOfflineBillProductList() {
        return offlineBillProductList;
    }

    public void setOfflineBillProductList(List<OfflineBillProduct> offlineBillProductList) {
        this.offlineBillProductList = offlineBillProductList;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @XmlTransient
    public List<ProductExportationReport> getProductExportationReportList() {
        return productExportationReportList;
    }

    public void setProductExportationReportList(List<ProductExportationReport> productExportationReportList) {
        this.productExportationReportList = productExportationReportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", unitPrice=" + unitPrice + ", description=" + description + ", quantity="
                + quantity + ", pictureThumb=" + pictureThumb + ", productImportationReportList=" + productImportationReportList
                + ", discountId=" + discountId + ", productTypeId=" + productTypeId + ", warehouseId=" + warehouseId + ", clothes="
                + clothes + ", electronics=" + electronics + ", ordersProductList=" + ordersProductList + ", offlineBillProductList="
                + offlineBillProductList + ", book=" + book + ", productExportationReportList=" + productExportationReportList + '}';
    }

}
