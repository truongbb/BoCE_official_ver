/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.product.electronics;

import entities.product.Product;
import entities.common.Color;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PRODUCT_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Electronics.findAll", query = "SELECT e FROM Electronics e"),
    @NamedQuery(name = "Electronics.findByElectronicsId", query = "SELECT e FROM Electronics e WHERE e.electronicsId = :electronicsId"),
    @NamedQuery(name = "Electronics.findByName", query = "SELECT e FROM Electronics e WHERE e.name = :name"),
    @NamedQuery(name = "Electronics.findByWeight", query = "SELECT e FROM Electronics e WHERE e.weight = :weight")})
public class Electronics implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ELECTRONICS_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal electronicsId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(nullable = false, length = 500)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double weight;
    @JoinColumn(name = "COLOR_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Color colorId;
    @JoinColumn(name = "ELECTRONICS_CATEGORY_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ElectronicsCategory electronicsCategoryId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @OneToOne(optional = false)
    private Product productId;

    public Electronics() {
    }

    public Electronics(BigDecimal electronicsId) {
        this.electronicsId = electronicsId;
    }

    public Electronics(BigDecimal electronicsId, String name, double weight) {
        this.electronicsId = electronicsId;
        this.name = name;
        this.weight = weight;
    }

    public BigDecimal getElectronicsId() {
        return electronicsId;
    }

    public void setElectronicsId(BigDecimal electronicsId) {
        this.electronicsId = electronicsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Color getColorId() {
        return colorId;
    }

    public void setColorId(Color colorId) {
        this.colorId = colorId;
    }

    public ElectronicsCategory getElectronicsCategoryId() {
        return electronicsCategoryId;
    }

    public void setElectronicsCategoryId(ElectronicsCategory electronicsCategoryId) {
        this.electronicsCategoryId = electronicsCategoryId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electronicsId != null ? electronicsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Electronics)) {
            return false;
        }
        Electronics other = (Electronics) object;
        if ((this.electronicsId == null && other.electronicsId != null) || (this.electronicsId != null && !this.electronicsId.equals(other.electronicsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Electronics[ electronicsId=" + electronicsId + " ]";
    }
    
}
