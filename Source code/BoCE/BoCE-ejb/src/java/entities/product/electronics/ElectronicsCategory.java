/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.product.electronics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zhang
 */
@Entity
@Table(name = "ELECTRONICS_CATEGORY", catalog = "", schema = "BOCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ElectronicsCategory.findAll", query = "SELECT e FROM ElectronicsCategory e"),
    @NamedQuery(name = "ElectronicsCategory.findById", query = "SELECT e FROM ElectronicsCategory e WHERE e.id = :id"),
    @NamedQuery(name = "ElectronicsCategory.findByName", query = "SELECT e FROM ElectronicsCategory e WHERE e.name = :name")})
public class ElectronicsCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(nullable = false, length = 500)
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "electronicsCategoryId")
    private List<Electronics> electronicsList;

    public ElectronicsCategory() {
    }

    public ElectronicsCategory(BigDecimal id) {
        this.id = id;
    }

    public ElectronicsCategory(BigDecimal id, String name) {
        this.id = id;
        this.name = name;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Electronics> getElectronicsList() {
        return electronicsList;
    }

    public void setElectronicsList(List<Electronics> electronicsList) {
        this.electronicsList = electronicsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElectronicsCategory)) {
            return false;
        }
        ElectronicsCategory other = (ElectronicsCategory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ElectronicsCategory[ id=" + id + " ]";
    }
    
}
