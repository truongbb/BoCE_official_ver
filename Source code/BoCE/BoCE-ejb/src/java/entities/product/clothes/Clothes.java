/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.product.clothes;

import entities.product.Product;
import entities.common.Color;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zhang
 */
@Entity
@Table(catalog = "", schema = "BOCE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PRODUCT_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Clothes.findAll", query = "SELECT c FROM Clothes c"),
    @NamedQuery(name = "Clothes.findByClothesId", query = "SELECT c FROM Clothes c WHERE c.clothesId = :clothesId"),
    @NamedQuery(name = "Clothes.findByName", query = "SELECT c FROM Clothes c WHERE c.name = :name"),
    @NamedQuery(name = "Clothes.findByDescription", query = "SELECT c FROM Clothes c WHERE c.description = :description")})
public class Clothes implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOTHES_ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal clothesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(nullable = false, length = 500)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(nullable = false, length = 1000)
    private String description;
    @JoinColumn(name = "CLOTHES_CATEGORY_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ClothesCategory clothesCategoryId;
    @JoinColumn(name = "CLOTHES_SIZE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private ClothesSize clothesSizeId;
    @JoinColumn(name = "COLOR_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Color colorId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
    @OneToOne(optional = false)
    private Product productId;
    @JoinColumn(name = "STYLE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Style styleId;

    public Clothes() {
    }

    public Clothes(BigDecimal clothesId) {
        this.clothesId = clothesId;
    }

    public Clothes(BigDecimal clothesId, String name, String description) {
        this.clothesId = clothesId;
        this.name = name;
        this.description = description;
    }

    public BigDecimal getClothesId() {
        return clothesId;
    }

    public void setClothesId(BigDecimal clothesId) {
        this.clothesId = clothesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClothesCategory getClothesCategoryId() {
        return clothesCategoryId;
    }

    public void setClothesCategoryId(ClothesCategory clothesCategoryId) {
        this.clothesCategoryId = clothesCategoryId;
    }

    public ClothesSize getClothesSizeId() {
        return clothesSizeId;
    }

    public void setClothesSizeId(ClothesSize clothesSizeId) {
        this.clothesSizeId = clothesSizeId;
    }

    public Color getColorId() {
        return colorId;
    }

    public void setColorId(Color colorId) {
        this.colorId = colorId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Style getStyleId() {
        return styleId;
    }

    public void setStyleId(Style styleId) {
        this.styleId = styleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clothesId != null ? clothesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clothes)) {
            return false;
        }
        Clothes other = (Clothes) object;
        if ((this.clothesId == null && other.clothesId != null) || (this.clothesId != null && !this.clothesId.equals(other.clothesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Clothes[ clothesId=" + clothesId + " ]";
    }
    
}
