/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.Ethnic;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface EthnicFacadeLocal {

    void create(Ethnic ethnic);

    void edit(Ethnic ethnic);

    void remove(Ethnic ethnic);

    Ethnic find(Object id);

    List<Ethnic> findAll();

    List<Ethnic> findRange(int[] range);

    int count();
    
}
