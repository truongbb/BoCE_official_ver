/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.product.electronics;

import entities.product.electronics.ElectronicsCategory;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ElectronicsCategoryFacadeLocal {

    void create(ElectronicsCategory electronicsCategory);

    void edit(ElectronicsCategory electronicsCategory);

    void remove(ElectronicsCategory electronicsCategory);

    ElectronicsCategory find(Object id);

    List<ElectronicsCategory> findAll();

    List<ElectronicsCategory> findRange(int[] range);

    int count();
    
}
