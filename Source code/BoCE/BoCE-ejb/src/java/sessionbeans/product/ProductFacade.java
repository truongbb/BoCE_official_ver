/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.product;

import entities.product.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import sessionbeans.AbstractFacade;

/**
 *
 * @author Zhang
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> implements ProductFacadeLocal {

    @PersistenceContext(unitName = "BoCE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    public List<Product> searchBookByName(String name) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        Predicate bookName = criteriaBuilder.like(root.<String>get("book").get("name"), "%" + name + "%");
        query.where(bookName);
        return getEntityManager().createQuery(query).getResultList();
    }

    public List<Product> searchElectronicsByName(String name) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        Predicate electronicsName = criteriaBuilder.like(root.<String>get("electronics").get("name"), "%" + name + "%");
        query.where(electronicsName);

        return getEntityManager().createQuery(query).getResultList();
    }

    public List<Product> searchClothesByName(String name) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        Predicate clothesName = criteriaBuilder.like(root.<String>get("clothes").get("name"), "%" + name + "%");
        query.where(clothesName);
        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public List<Product> searchByName(String name) {

        List<Product> books = searchBookByName(name);
        List<Product> clothes = searchClothesByName(name);
        List<Product> electronics = searchElectronicsByName(name);

        List<Product> products = new ArrayList<>();

        products.addAll(books);
        products.addAll(clothes);
        products.addAll(electronics);
        
        return products;

    }

}
