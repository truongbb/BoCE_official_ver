/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.employee.reward;

import entities.employee.reward.RewardType;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface RewardTypeFacadeLocal {

    void create(RewardType rewardType);

    void edit(RewardType rewardType);

    void remove(RewardType rewardType);

    RewardType find(Object id);

    List<RewardType> findAll();

    List<RewardType> findRange(int[] range);

    int count();
    
}
