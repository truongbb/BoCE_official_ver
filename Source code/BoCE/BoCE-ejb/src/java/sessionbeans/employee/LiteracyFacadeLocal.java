/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.employee;

import entities.employee.Literacy;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface LiteracyFacadeLocal {

    void create(Literacy literacy);

    void edit(Literacy literacy);

    void remove(Literacy literacy);

    Literacy find(Object id);

    List<Literacy> findAll();

    List<Literacy> findRange(int[] range);

    int count();
    
}
