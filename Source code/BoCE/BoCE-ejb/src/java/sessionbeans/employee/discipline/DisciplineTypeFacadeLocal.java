/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.employee.discipline;

import entities.employee.discipline.DisciplineType;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface DisciplineTypeFacadeLocal {

    void create(DisciplineType disciplineType);

    void edit(DisciplineType disciplineType);

    void remove(DisciplineType disciplineType);

    DisciplineType find(Object id);

    List<DisciplineType> findAll();

    List<DisciplineType> findRange(int[] range);

    int count();
    
}
