/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.employee.discipline;

import entities.employee.discipline.EmployeeDiscipline;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface EmployeeDisciplineFacadeLocal {

    void create(EmployeeDiscipline employeeDiscipline);

    void edit(EmployeeDiscipline employeeDiscipline);

    void remove(EmployeeDiscipline employeeDiscipline);

    EmployeeDiscipline find(Object id);

    List<EmployeeDiscipline> findAll();

    List<EmployeeDiscipline> findRange(int[] range);

    int count();
    
}
