/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.BookCategory;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface BookCategoryFacadeLocal {

    void create(BookCategory bookCategory);

    void edit(BookCategory bookCategory);

    void remove(BookCategory bookCategory);

    BookCategory find(Object id);

    List<BookCategory> findAll();

    List<BookCategory> findRange(int[] range);

    int count();
    
}
