/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.OfflineBill;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface OfflineBillFacadeLocal {

    void create(OfflineBill offlineBill);

    void edit(OfflineBill offlineBill);

    void remove(OfflineBill offlineBill);

    OfflineBill find(Object id);

    List<OfflineBill> findAll();

    List<OfflineBill> findRange(int[] range);

    int count();
    
}
