/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.ClothesSize;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ClothesSizeFacadeLocal {

    void create(ClothesSize clothesSize);

    void edit(ClothesSize clothesSize);

    void remove(ClothesSize clothesSize);

    ClothesSize find(Object id);

    List<ClothesSize> findAll();

    List<ClothesSize> findRange(int[] range);

    int count();
    
}
