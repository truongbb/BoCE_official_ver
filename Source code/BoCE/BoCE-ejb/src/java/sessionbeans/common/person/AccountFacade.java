/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.common.person;

import sessionbeans.AbstractFacade;
import entities.common.person.Account;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Zhang
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account> implements AccountFacadeLocal {

    @PersistenceContext(unitName = "BoCE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountFacade() {
        super(Account.class);
    }

    @Override
    public Account login(String username, String password) {
//        SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "login");
        Query query = getEntityManager().createQuery("select a from Account a where a.username = :p_username and a.password = :p_password");
        query.setParameter("p_username", username);
        query.setParameter("p_password", password);
        return (Account) query.getSingleResult();
    }

}
