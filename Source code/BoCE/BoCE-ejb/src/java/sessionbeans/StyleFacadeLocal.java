/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.Style;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface StyleFacadeLocal {

    void create(Style style);

    void edit(Style style);

    void remove(Style style);

    Style find(Object id);

    List<Style> findAll();

    List<Style> findRange(int[] range);

    int count();
    
}
