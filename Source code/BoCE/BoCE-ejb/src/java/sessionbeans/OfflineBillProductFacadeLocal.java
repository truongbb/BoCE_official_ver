/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.OfflineBillProduct;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface OfflineBillProductFacadeLocal {

    void create(OfflineBillProduct offlineBillProduct);

    void edit(OfflineBillProduct offlineBillProduct);

    void remove(OfflineBillProduct offlineBillProduct);

    OfflineBillProduct find(Object id);

    List<OfflineBillProduct> findAll();

    List<OfflineBillProduct> findRange(int[] range);

    int count();
    
}
