/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.customer;

import entities.customer.CustomerType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sessionbeans.AbstractFacade;

/**
 *
 * @author Zhang
 */
@Stateless
public class CustomerTypeFacade extends AbstractFacade<CustomerType> implements CustomerTypeFacadeLocal {
    @PersistenceContext(unitName = "BoCE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerTypeFacade() {
        super(CustomerType.class);
    }
    
}
