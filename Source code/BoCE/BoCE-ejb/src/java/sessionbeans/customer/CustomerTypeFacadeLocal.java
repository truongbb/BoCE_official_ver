/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.customer;

import entities.customer.CustomerType;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface CustomerTypeFacadeLocal {

    void create(CustomerType customerType);

    void edit(CustomerType customerType);

    void remove(CustomerType customerType);

    CustomerType find(Object id);

    List<CustomerType> findAll();

    List<CustomerType> findRange(int[] range);

    int count();
    
}
