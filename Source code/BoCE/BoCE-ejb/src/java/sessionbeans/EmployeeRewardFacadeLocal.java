/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.EmployeeReward;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface EmployeeRewardFacadeLocal {

    void create(EmployeeReward employeeReward);

    void edit(EmployeeReward employeeReward);

    void remove(EmployeeReward employeeReward);

    EmployeeReward find(Object id);

    List<EmployeeReward> findAll();

    List<EmployeeReward> findRange(int[] range);

    int count();
    
}
