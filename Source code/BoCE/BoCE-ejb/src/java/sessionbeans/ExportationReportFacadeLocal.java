/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.ExportationReport;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ExportationReportFacadeLocal {

    void create(ExportationReport exportationReport);

    void edit(ExportationReport exportationReport);

    void remove(ExportationReport exportationReport);

    ExportationReport find(Object id);

    List<ExportationReport> findAll();

    List<ExportationReport> findRange(int[] range);

    int count();
    
}
