/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.ClothesCategory;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ClothesCategoryFacadeLocal {

    void create(ClothesCategory clothesCategory);

    void edit(ClothesCategory clothesCategory);

    void remove(ClothesCategory clothesCategory);

    ClothesCategory find(Object id);

    List<ClothesCategory> findAll();

    List<ClothesCategory> findRange(int[] range);

    int count();
    
}
