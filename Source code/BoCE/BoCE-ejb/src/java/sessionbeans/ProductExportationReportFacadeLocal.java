/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.ProductExportationReport;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ProductExportationReportFacadeLocal {

    void create(ProductExportationReport productExportationReport);

    void edit(ProductExportationReport productExportationReport);

    void remove(ProductExportationReport productExportationReport);

    ProductExportationReport find(Object id);

    List<ProductExportationReport> findAll();

    List<ProductExportationReport> findRange(int[] range);

    int count();
    
}
