/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.transaction.order;

import entities.transaction.order.OrdersProduct;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface OrdersProductFacadeLocal {

    void create(OrdersProduct ordersProduct);

    void edit(OrdersProduct ordersProduct);

    void remove(OrdersProduct ordersProduct);

    OrdersProduct find(Object id);

    List<OrdersProduct> findAll();

    List<OrdersProduct> findRange(int[] range);

    int count();
    
}
