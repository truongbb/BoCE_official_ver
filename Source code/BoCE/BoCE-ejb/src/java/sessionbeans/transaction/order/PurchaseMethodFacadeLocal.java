/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.transaction.order;

import entities.transaction.order.PurchaseMethod;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface PurchaseMethodFacadeLocal {

    void create(PurchaseMethod purchaseMethod);

    void edit(PurchaseMethod purchaseMethod);

    void remove(PurchaseMethod purchaseMethod);

    PurchaseMethod find(Object id);

    List<PurchaseMethod> findAll();

    List<PurchaseMethod> findRange(int[] range);

    int count();
    
}
