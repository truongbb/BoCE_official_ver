/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.ProductImportationReport;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ProductImportationReportFacadeLocal {

    void create(ProductImportationReport productImportationReport);

    void edit(ProductImportationReport productImportationReport);

    void remove(ProductImportationReport productImportationReport);

    ProductImportationReport find(Object id);

    List<ProductImportationReport> findAll();

    List<ProductImportationReport> findRange(int[] range);

    int count();
    
}
