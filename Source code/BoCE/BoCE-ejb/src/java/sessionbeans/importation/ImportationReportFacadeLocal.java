/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans.importation;

import entities.importation.ImportationReport;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface ImportationReportFacadeLocal {

    void create(ImportationReport importationReport);

    void edit(ImportationReport importationReport);

    void remove(ImportationReport importationReport);

    ImportationReport find(Object id);

    List<ImportationReport> findAll();

    List<ImportationReport> findRange(int[] range);

    int count();
    
}
