/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.Discipline;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface DisciplineFacadeLocal {

    void create(Discipline discipline);

    void edit(Discipline discipline);

    void remove(Discipline discipline);

    Discipline find(Object id);

    List<Discipline> findAll();

    List<Discipline> findRange(int[] range);

    int count();
    
}
