/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import entities.OnlineBill;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Zhang
 */
@Local
public interface OnlineBillFacadeLocal {

    void create(OnlineBill onlineBill);

    void edit(OnlineBill onlineBill);

    void remove(OnlineBill onlineBill);

    OnlineBill find(Object id);

    List<OnlineBill> findAll();

    List<OnlineBill> findRange(int[] range);

    int count();
    
}
