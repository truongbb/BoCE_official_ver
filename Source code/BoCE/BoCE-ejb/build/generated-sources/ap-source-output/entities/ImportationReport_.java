package entities;

import entities.Employee;
import entities.ProductImportationReport;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(ImportationReport.class)
public class ImportationReport_ { 

    public static volatile SingularAttribute<ImportationReport, String> note;
    public static volatile SingularAttribute<ImportationReport, Date> createdDate;
    public static volatile ListAttribute<ImportationReport, ProductImportationReport> productImportationReportList;
    public static volatile SingularAttribute<ImportationReport, Employee> employeeId;
    public static volatile SingularAttribute<ImportationReport, BigDecimal> id;

}