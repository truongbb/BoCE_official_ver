package entities;

import entities.Book;
import entities.Clothes;
import entities.Discount;
import entities.Electronics;
import entities.OfflineBillProduct;
import entities.OrdersProduct;
import entities.ProductExportationReport;
import entities.ProductImportationReport;
import entities.ProductType;
import entities.Warehouse;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Product.class)
public class Product_ { 

    public static volatile SingularAttribute<Product, Double> unitPrice;
    public static volatile SingularAttribute<Product, Electronics> electronics;
    public static volatile SingularAttribute<Product, BigInteger> quantity;
    public static volatile SingularAttribute<Product, BigDecimal> productId;
    public static volatile SingularAttribute<Product, Book> book;
    public static volatile SingularAttribute<Product, String> description;
    public static volatile ListAttribute<Product, OfflineBillProduct> offlineBillProductList;
    public static volatile SingularAttribute<Product, ProductType> productTypeId;
    public static volatile SingularAttribute<Product, Clothes> clothes;
    public static volatile SingularAttribute<Product, String> pictureThumb;
    public static volatile SingularAttribute<Product, Warehouse> warehouseId;
    public static volatile ListAttribute<Product, ProductImportationReport> productImportationReportList;
    public static volatile SingularAttribute<Product, Discount> discountId;
    public static volatile ListAttribute<Product, OrdersProduct> ordersProductList;
    public static volatile ListAttribute<Product, ProductExportationReport> productExportationReportList;

}