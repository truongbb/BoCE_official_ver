package entities;

import entities.Employee;
import entities.Reward;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(EmployeeReward.class)
public class EmployeeReward_ { 

    public static volatile SingularAttribute<EmployeeReward, Reward> rewardId;
    public static volatile SingularAttribute<EmployeeReward, Double> bonus;
    public static volatile SingularAttribute<EmployeeReward, String> description;
    public static volatile SingularAttribute<EmployeeReward, Employee> employeeId;
    public static volatile SingularAttribute<EmployeeReward, BigDecimal> id;
    public static volatile SingularAttribute<EmployeeReward, Date> rewardDate;

}