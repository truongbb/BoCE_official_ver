package entities;

import entities.Clothes;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(ClothesCategory.class)
public class ClothesCategory_ { 

    public static volatile SingularAttribute<ClothesCategory, String> name;
    public static volatile SingularAttribute<ClothesCategory, BigDecimal> id;
    public static volatile ListAttribute<ClothesCategory, Clothes> clothesList;

}