package entities;

import entities.Discipline;
import entities.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(EmployeeDiscipline.class)
public class EmployeeDiscipline_ { 

    public static volatile SingularAttribute<EmployeeDiscipline, Double> forfeit;
    public static volatile SingularAttribute<EmployeeDiscipline, String> description;
    public static volatile SingularAttribute<EmployeeDiscipline, Discipline> disciplineId;
    public static volatile SingularAttribute<EmployeeDiscipline, Employee> employeeId;
    public static volatile SingularAttribute<EmployeeDiscipline, BigDecimal> id;
    public static volatile SingularAttribute<EmployeeDiscipline, Date> disciplineDate;

}