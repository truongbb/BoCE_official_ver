package entities;

import entities.Employee;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Ethnic.class)
public class Ethnic_ { 

    public static volatile ListAttribute<Ethnic, Employee> employeeList;
    public static volatile SingularAttribute<Ethnic, BigDecimal> id;
    public static volatile SingularAttribute<Ethnic, String> value;

}