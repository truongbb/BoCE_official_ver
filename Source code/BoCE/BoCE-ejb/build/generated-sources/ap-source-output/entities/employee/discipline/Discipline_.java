package entities.employee.discipline;

import entities.employee.discipline.DisciplineType;
import entities.employee.discipline.EmployeeDiscipline;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Discipline.class)
public class Discipline_ { 

    public static volatile SingularAttribute<Discipline, String> name;
    public static volatile ListAttribute<Discipline, EmployeeDiscipline> employeeDisciplineList;
    public static volatile SingularAttribute<Discipline, BigDecimal> id;
    public static volatile SingularAttribute<Discipline, DisciplineType> disciplineTypeId;

}