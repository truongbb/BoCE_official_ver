package entities.employee;

import entities.employee.Employee;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Literacy.class)
public class Literacy_ { 

    public static volatile ListAttribute<Literacy, Employee> employeeList;
    public static volatile SingularAttribute<Literacy, BigDecimal> id;
    public static volatile SingularAttribute<Literacy, String> value;

}