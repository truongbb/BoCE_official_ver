package entities.employee.reward;

import entities.employee.Employee;
import entities.employee.reward.Reward;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(EmployeeReward.class)
public class EmployeeReward_ { 

    public static volatile SingularAttribute<EmployeeReward, Reward> rewardId;
    public static volatile SingularAttribute<EmployeeReward, Double> bonus;
    public static volatile SingularAttribute<EmployeeReward, String> description;
    public static volatile SingularAttribute<EmployeeReward, Employee> employeeId;
    public static volatile SingularAttribute<EmployeeReward, BigDecimal> id;
    public static volatile SingularAttribute<EmployeeReward, Date> rewardDate;

}