package entities.employee.discipline;

import entities.employee.discipline.Discipline;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(DisciplineType.class)
public class DisciplineType_ { 

    public static volatile ListAttribute<DisciplineType, Discipline> disciplineList;
    public static volatile SingularAttribute<DisciplineType, BigDecimal> id;
    public static volatile SingularAttribute<DisciplineType, String> type;

}