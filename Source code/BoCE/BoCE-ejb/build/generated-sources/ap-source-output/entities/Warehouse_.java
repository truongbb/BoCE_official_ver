package entities;

import entities.Product;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Warehouse.class)
public class Warehouse_ { 

    public static volatile SingularAttribute<Warehouse, String> code;
    public static volatile SingularAttribute<Warehouse, String> phone;
    public static volatile SingularAttribute<Warehouse, String> name;
    public static volatile SingularAttribute<Warehouse, String> location;
    public static volatile SingularAttribute<Warehouse, BigDecimal> id;
    public static volatile ListAttribute<Warehouse, Product> productList;

}