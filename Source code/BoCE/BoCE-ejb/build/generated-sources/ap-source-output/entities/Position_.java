package entities;

import entities.Employee;
import entities.Salary;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Position.class)
public class Position_ { 

    public static volatile ListAttribute<Position, Employee> employeeList;
    public static volatile SingularAttribute<Position, String> name;
    public static volatile SingularAttribute<Position, Salary> salaryLevel;
    public static volatile SingularAttribute<Position, BigDecimal> id;

}