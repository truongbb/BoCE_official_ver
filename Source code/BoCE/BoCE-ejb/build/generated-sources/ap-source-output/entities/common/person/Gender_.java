package entities.common.person;

import entities.common.person.Person;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Gender.class)
public class Gender_ { 

    public static volatile ListAttribute<Gender, Person> personList;
    public static volatile SingularAttribute<Gender, String> gender;
    public static volatile SingularAttribute<Gender, BigDecimal> id;

}