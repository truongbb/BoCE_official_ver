package entities.common;

import entities.exportation.ProductExportationReport;
import entities.importation.ProductImportationReport;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Unit.class)
public class Unit_ { 

    public static volatile ListAttribute<Unit, ProductImportationReport> productImportationReportList;
    public static volatile SingularAttribute<Unit, BigDecimal> id;
    public static volatile SingularAttribute<Unit, String> value;
    public static volatile ListAttribute<Unit, ProductExportationReport> productExportationReportList;

}