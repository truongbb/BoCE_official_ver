package entities.common;

import entities.employee.Employee;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Ethnic.class)
public class Ethnic_ { 

    public static volatile ListAttribute<Ethnic, Employee> employeeList;
    public static volatile SingularAttribute<Ethnic, BigDecimal> id;
    public static volatile SingularAttribute<Ethnic, String> value;

}