package entities;

import entities.CustomerType;
import entities.Orders;
import entities.Person;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, CustomerType> customerTypeId;
    public static volatile SingularAttribute<Customer, Person> personId;
    public static volatile SingularAttribute<Customer, BigDecimal> id;
    public static volatile ListAttribute<Customer, Orders> ordersList;

}