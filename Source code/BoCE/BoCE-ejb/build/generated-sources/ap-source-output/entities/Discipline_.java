package entities;

import entities.DisciplineType;
import entities.EmployeeDiscipline;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Discipline.class)
public class Discipline_ { 

    public static volatile SingularAttribute<Discipline, String> name;
    public static volatile ListAttribute<Discipline, EmployeeDiscipline> employeeDisciplineList;
    public static volatile SingularAttribute<Discipline, BigDecimal> id;
    public static volatile SingularAttribute<Discipline, DisciplineType> disciplineTypeId;

}