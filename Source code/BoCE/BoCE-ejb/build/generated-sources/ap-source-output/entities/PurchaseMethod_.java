package entities;

import entities.Orders;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(PurchaseMethod.class)
public class PurchaseMethod_ { 

    public static volatile SingularAttribute<PurchaseMethod, String> purchaseMethod;
    public static volatile SingularAttribute<PurchaseMethod, BigDecimal> id;
    public static volatile ListAttribute<PurchaseMethod, Orders> ordersList;

}