package entities;

import entities.Product;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Discount.class)
public class Discount_ { 

    public static volatile SingularAttribute<Discount, Date> beginDate;
    public static volatile SingularAttribute<Discount, Double> rate;
    public static volatile SingularAttribute<Discount, Date> endDate;
    public static volatile SingularAttribute<Discount, BigDecimal> id;
    public static volatile ListAttribute<Discount, Product> productList;

}