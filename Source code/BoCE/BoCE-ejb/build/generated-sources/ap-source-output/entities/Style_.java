package entities;

import entities.Clothes;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Style.class)
public class Style_ { 

    public static volatile SingularAttribute<Style, String> style;
    public static volatile SingularAttribute<Style, BigDecimal> id;
    public static volatile ListAttribute<Style, Clothes> clothesList;

}