package entities;

import entities.Customer;
import entities.OnlineBill;
import entities.OrdersProduct;
import entities.PurchaseMethod;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Orders.class)
public class Orders_ { 

    public static volatile SingularAttribute<Orders, Date> expextedReceivingDate;
    public static volatile SingularAttribute<Orders, String> receivingAddress;
    public static volatile SingularAttribute<Orders, Customer> customerId;
    public static volatile ListAttribute<Orders, OnlineBill> onlineBillList;
    public static volatile SingularAttribute<Orders, PurchaseMethod> purchaseMethodId;
    public static volatile SingularAttribute<Orders, BigDecimal> id;
    public static volatile SingularAttribute<Orders, Double> additionalCharge;
    public static volatile SingularAttribute<Orders, Double> cash;
    public static volatile ListAttribute<Orders, OrdersProduct> ordersProductList;

}