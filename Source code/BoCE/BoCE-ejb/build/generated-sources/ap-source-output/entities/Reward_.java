package entities;

import entities.EmployeeReward;
import entities.RewardType;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Reward.class)
public class Reward_ { 

    public static volatile ListAttribute<Reward, EmployeeReward> employeeRewardList;
    public static volatile SingularAttribute<Reward, RewardType> rewardTypeId;
    public static volatile SingularAttribute<Reward, String> name;
    public static volatile SingularAttribute<Reward, BigDecimal> id;

}