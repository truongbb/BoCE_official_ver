package entities;

import entities.Discipline;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(DisciplineType.class)
public class DisciplineType_ { 

    public static volatile ListAttribute<DisciplineType, Discipline> disciplineList;
    public static volatile SingularAttribute<DisciplineType, BigDecimal> id;
    public static volatile SingularAttribute<DisciplineType, String> type;

}