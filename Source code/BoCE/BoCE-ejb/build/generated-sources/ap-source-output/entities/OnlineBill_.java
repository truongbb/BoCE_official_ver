package entities;

import entities.Bill;
import entities.Orders;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(OnlineBill.class)
public class OnlineBill_ { 

    public static volatile SingularAttribute<OnlineBill, Orders> orderId;
    public static volatile SingularAttribute<OnlineBill, Bill> billId;
    public static volatile SingularAttribute<OnlineBill, BigDecimal> onlineBillId;
    public static volatile SingularAttribute<OnlineBill, Date> receivedDate;

}