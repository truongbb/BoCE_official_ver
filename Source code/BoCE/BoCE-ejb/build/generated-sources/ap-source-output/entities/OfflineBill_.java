package entities;

import entities.Bill;
import entities.OfflineBillProduct;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(OfflineBill.class)
public class OfflineBill_ { 

    public static volatile SingularAttribute<OfflineBill, BigDecimal> offlineBillId;
    public static volatile SingularAttribute<OfflineBill, Bill> billId;
    public static volatile ListAttribute<OfflineBill, OfflineBillProduct> offlineBillProductList;
    public static volatile SingularAttribute<OfflineBill, Double> additionalCharge;
    public static volatile SingularAttribute<OfflineBill, Double> cash;

}