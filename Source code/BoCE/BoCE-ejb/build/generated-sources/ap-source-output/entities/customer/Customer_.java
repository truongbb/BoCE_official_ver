package entities.customer;

import entities.common.person.Person;
import entities.customer.CustomerType;
import entities.transaction.order.Orders;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, CustomerType> customerTypeId;
    public static volatile SingularAttribute<Customer, Person> personId;
    public static volatile SingularAttribute<Customer, BigDecimal> id;
    public static volatile ListAttribute<Customer, Orders> ordersList;

}