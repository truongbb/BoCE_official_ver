package entities;

import entities.Color;
import entities.ElectronicsCategory;
import entities.Product;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Electronics.class)
public class Electronics_ { 

    public static volatile SingularAttribute<Electronics, Product> productId;
    public static volatile SingularAttribute<Electronics, ElectronicsCategory> electronicsCategoryId;
    public static volatile SingularAttribute<Electronics, Color> colorId;
    public static volatile SingularAttribute<Electronics, String> name;
    public static volatile SingularAttribute<Electronics, Double> weight;
    public static volatile SingularAttribute<Electronics, BigDecimal> electronicsId;

}