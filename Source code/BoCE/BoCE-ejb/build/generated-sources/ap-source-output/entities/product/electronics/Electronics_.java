package entities.product.electronics;

import entities.common.Color;
import entities.product.Product;
import entities.product.electronics.ElectronicsCategory;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Electronics.class)
public class Electronics_ { 

    public static volatile SingularAttribute<Electronics, Product> productId;
    public static volatile SingularAttribute<Electronics, ElectronicsCategory> electronicsCategoryId;
    public static volatile SingularAttribute<Electronics, Color> colorId;
    public static volatile SingularAttribute<Electronics, String> name;
    public static volatile SingularAttribute<Electronics, Double> weight;
    public static volatile SingularAttribute<Electronics, BigDecimal> electronicsId;

}