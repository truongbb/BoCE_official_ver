package entities.product.clothes;

import entities.product.clothes.Clothes;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(ClothesSize.class)
public class ClothesSize_ { 

    public static volatile SingularAttribute<ClothesSize, String> clothesSize;
    public static volatile SingularAttribute<ClothesSize, BigDecimal> id;
    public static volatile ListAttribute<ClothesSize, Clothes> clothesList;

}