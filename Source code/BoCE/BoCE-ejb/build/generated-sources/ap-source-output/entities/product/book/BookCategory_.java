package entities.product.book;

import entities.product.book.Book;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(BookCategory.class)
public class BookCategory_ { 

    public static volatile SingularAttribute<BookCategory, String> name;
    public static volatile SingularAttribute<BookCategory, BigDecimal> id;
    public static volatile ListAttribute<BookCategory, Book> bookList;

}