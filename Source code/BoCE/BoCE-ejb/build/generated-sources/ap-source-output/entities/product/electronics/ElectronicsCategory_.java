package entities.product.electronics;

import entities.product.electronics.Electronics;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(ElectronicsCategory.class)
public class ElectronicsCategory_ { 

    public static volatile ListAttribute<ElectronicsCategory, Electronics> electronicsList;
    public static volatile SingularAttribute<ElectronicsCategory, String> name;
    public static volatile SingularAttribute<ElectronicsCategory, BigDecimal> id;

}