package entities.product.book;

import entities.product.Product;
import entities.product.book.Author;
import entities.product.book.BookCategory;
import entities.product.book.Publisher;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, Publisher> publisherId;
    public static volatile SingularAttribute<Book, Product> productId;
    public static volatile SingularAttribute<Book, BigInteger> totalPage;
    public static volatile SingularAttribute<Book, String> name;
    public static volatile SingularAttribute<Book, BigInteger> publishedYear;
    public static volatile SingularAttribute<Book, Author> authorId;
    public static volatile SingularAttribute<Book, BigDecimal> bookId;
    public static volatile SingularAttribute<Book, BookCategory> bookCategoryId;

}