package entities.product;

import entities.exportation.ProductExportationReport;
import entities.importation.ProductImportationReport;
import entities.product.Discount;
import entities.product.ProductType;
import entities.product.Warehouse;
import entities.product.book.Book;
import entities.product.clothes.Clothes;
import entities.product.electronics.Electronics;
import entities.transaction.bill.OfflineBillProduct;
import entities.transaction.order.OrdersProduct;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Product.class)
public class Product_ { 

    public static volatile SingularAttribute<Product, Double> unitPrice;
    public static volatile SingularAttribute<Product, Electronics> electronics;
    public static volatile SingularAttribute<Product, BigInteger> quantity;
    public static volatile SingularAttribute<Product, BigDecimal> productId;
    public static volatile SingularAttribute<Product, Book> book;
    public static volatile SingularAttribute<Product, String> description;
    public static volatile ListAttribute<Product, OfflineBillProduct> offlineBillProductList;
    public static volatile SingularAttribute<Product, ProductType> productTypeId;
    public static volatile SingularAttribute<Product, Clothes> clothes;
    public static volatile SingularAttribute<Product, String> pictureThumb;
    public static volatile SingularAttribute<Product, Warehouse> warehouseId;
    public static volatile ListAttribute<Product, ProductImportationReport> productImportationReportList;
    public static volatile SingularAttribute<Product, Discount> discountId;
    public static volatile ListAttribute<Product, OrdersProduct> ordersProductList;
    public static volatile ListAttribute<Product, ProductExportationReport> productExportationReportList;

}