package entities.product.clothes;

import entities.common.Color;
import entities.product.Product;
import entities.product.clothes.ClothesCategory;
import entities.product.clothes.ClothesSize;
import entities.product.clothes.Style;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Clothes.class)
public class Clothes_ { 

    public static volatile SingularAttribute<Clothes, ClothesCategory> clothesCategoryId;
    public static volatile SingularAttribute<Clothes, Product> productId;
    public static volatile SingularAttribute<Clothes, ClothesSize> clothesSizeId;
    public static volatile SingularAttribute<Clothes, Color> colorId;
    public static volatile SingularAttribute<Clothes, Style> styleId;
    public static volatile SingularAttribute<Clothes, BigDecimal> clothesId;
    public static volatile SingularAttribute<Clothes, String> name;
    public static volatile SingularAttribute<Clothes, String> description;

}