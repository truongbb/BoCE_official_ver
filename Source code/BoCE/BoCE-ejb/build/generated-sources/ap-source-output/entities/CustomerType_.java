package entities;

import entities.Customer;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(CustomerType.class)
public class CustomerType_ { 

    public static volatile ListAttribute<CustomerType, Customer> customerList;
    public static volatile SingularAttribute<CustomerType, BigDecimal> id;
    public static volatile SingularAttribute<CustomerType, String> type;

}