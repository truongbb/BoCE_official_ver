package entities.importation;

import entities.common.Unit;
import entities.importation.ImportationReport;
import entities.importation.Supplier;
import entities.product.Product;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(ProductImportationReport.class)
public class ProductImportationReport_ { 

    public static volatile SingularAttribute<ProductImportationReport, Double> unitPrice;
    public static volatile SingularAttribute<ProductImportationReport, BigInteger> importedQuantity;
    public static volatile SingularAttribute<ProductImportationReport, ImportationReport> importationReportId;
    public static volatile SingularAttribute<ProductImportationReport, Supplier> supplierId;
    public static volatile SingularAttribute<ProductImportationReport, Product> productId;
    public static volatile SingularAttribute<ProductImportationReport, Unit> unitId;
    public static volatile SingularAttribute<ProductImportationReport, BigDecimal> id;

}