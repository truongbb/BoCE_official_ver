package entities.importation;

import entities.importation.ProductImportationReport;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Supplier.class)
public class Supplier_ { 

    public static volatile SingularAttribute<Supplier, String> address;
    public static volatile SingularAttribute<Supplier, String> phone;
    public static volatile SingularAttribute<Supplier, String> name;
    public static volatile ListAttribute<Supplier, ProductImportationReport> productImportationReportList;
    public static volatile SingularAttribute<Supplier, BigDecimal> id;
    public static volatile SingularAttribute<Supplier, String> email;

}