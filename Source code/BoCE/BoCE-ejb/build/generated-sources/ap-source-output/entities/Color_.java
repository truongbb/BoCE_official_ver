package entities;

import entities.Clothes;
import entities.Electronics;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Color.class)
public class Color_ { 

    public static volatile SingularAttribute<Color, String> code;
    public static volatile SingularAttribute<Color, String> color;
    public static volatile ListAttribute<Color, Electronics> electronicsList;
    public static volatile SingularAttribute<Color, BigDecimal> id;
    public static volatile ListAttribute<Color, Clothes> clothesList;

}