package entities;

import entities.Position;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Salary.class)
public class Salary_ { 

    public static volatile ListAttribute<Salary, Position> positionList;
    public static volatile SingularAttribute<Salary, Double> salaryRate;
    public static volatile SingularAttribute<Salary, Double> basicSalary;
    public static volatile SingularAttribute<Salary, Double> allowanceRate;
    public static volatile SingularAttribute<Salary, BigDecimal> salaryLevel;

}