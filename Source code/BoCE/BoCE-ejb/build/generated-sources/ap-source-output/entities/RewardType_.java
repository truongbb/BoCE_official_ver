package entities;

import entities.Reward;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(RewardType.class)
public class RewardType_ { 

    public static volatile ListAttribute<RewardType, Reward> rewardList;
    public static volatile SingularAttribute<RewardType, BigDecimal> id;
    public static volatile SingularAttribute<RewardType, String> type;

}