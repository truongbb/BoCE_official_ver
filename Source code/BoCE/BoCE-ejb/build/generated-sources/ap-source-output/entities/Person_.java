package entities;

import entities.Account;
import entities.Customer;
import entities.Employee;
import entities.Gender;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Person.class)
public class Person_ { 

    public static volatile SingularAttribute<Person, Date> birthday;
    public static volatile SingularAttribute<Person, String> lastName;
    public static volatile SingularAttribute<Person, String> firstName;
    public static volatile SingularAttribute<Person, String> address;
    public static volatile SingularAttribute<Person, String> phone;
    public static volatile ListAttribute<Person, Account> accountList;
    public static volatile SingularAttribute<Person, Gender> genderId;
    public static volatile SingularAttribute<Person, String> midName;
    public static volatile SingularAttribute<Person, BigDecimal> id;
    public static volatile SingularAttribute<Person, Employee> employee;
    public static volatile SingularAttribute<Person, String> email;
    public static volatile SingularAttribute<Person, Customer> customer;

}