package entities;

import entities.Bill;
import entities.Department;
import entities.EmployeeDiscipline;
import entities.EmployeeReward;
import entities.Ethnic;
import entities.ExportationReport;
import entities.ImportationReport;
import entities.Literacy;
import entities.Person;
import entities.Position;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Employee.class)
public class Employee_ { 

    public static volatile ListAttribute<Employee, EmployeeReward> employeeRewardList;
    public static volatile ListAttribute<Employee, ImportationReport> importationReportList;
    public static volatile SingularAttribute<Employee, Position> positionId;
    public static volatile ListAttribute<Employee, ExportationReport> exportationReportList;
    public static volatile SingularAttribute<Employee, Date> startWorkingDate;
    public static volatile ListAttribute<Employee, Bill> billList;
    public static volatile SingularAttribute<Employee, Department> departmentId;
    public static volatile SingularAttribute<Employee, Literacy> literacyId;
    public static volatile ListAttribute<Employee, EmployeeDiscipline> employeeDisciplineList;
    public static volatile SingularAttribute<Employee, Ethnic> ethnicId;
    public static volatile SingularAttribute<Employee, Person> personId;
    public static volatile SingularAttribute<Employee, BigDecimal> id;

}