package entities.transaction.order;

import entities.product.Product;
import entities.transaction.order.Orders;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(OrdersProduct.class)
public class OrdersProduct_ { 

    public static volatile SingularAttribute<OrdersProduct, Orders> ordersId;
    public static volatile SingularAttribute<OrdersProduct, BigInteger> quantity;
    public static volatile SingularAttribute<OrdersProduct, Product> productId;
    public static volatile SingularAttribute<OrdersProduct, BigDecimal> id;

}