package entities.transaction.bill;

import entities.transaction.bill.Bill;
import entities.transaction.order.Orders;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(OnlineBill.class)
public class OnlineBill_ { 

    public static volatile SingularAttribute<OnlineBill, Orders> orderId;
    public static volatile SingularAttribute<OnlineBill, Bill> billId;
    public static volatile SingularAttribute<OnlineBill, BigDecimal> onlineBillId;
    public static volatile SingularAttribute<OnlineBill, Date> receivedDate;

}