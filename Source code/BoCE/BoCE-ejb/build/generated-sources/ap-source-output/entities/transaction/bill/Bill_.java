package entities.transaction.bill;

import entities.employee.Employee;
import entities.transaction.bill.OfflineBill;
import entities.transaction.bill.OnlineBill;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(Bill.class)
public class Bill_ { 

    public static volatile ListAttribute<Bill, OfflineBill> offlineBillList;
    public static volatile ListAttribute<Bill, OnlineBill> onlineBillList;
    public static volatile SingularAttribute<Bill, Employee> employeeId;
    public static volatile SingularAttribute<Bill, BigDecimal> id;
    public static volatile SingularAttribute<Bill, Date> transactionDate;

}