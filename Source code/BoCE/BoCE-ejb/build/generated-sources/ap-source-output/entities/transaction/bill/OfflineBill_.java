package entities.transaction.bill;

import entities.transaction.bill.Bill;
import entities.transaction.bill.OfflineBillProduct;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(OfflineBill.class)
public class OfflineBill_ { 

    public static volatile SingularAttribute<OfflineBill, BigDecimal> offlineBillId;
    public static volatile SingularAttribute<OfflineBill, Bill> billId;
    public static volatile ListAttribute<OfflineBill, OfflineBillProduct> offlineBillProductList;
    public static volatile SingularAttribute<OfflineBill, Double> additionalCharge;
    public static volatile SingularAttribute<OfflineBill, Double> cash;

}