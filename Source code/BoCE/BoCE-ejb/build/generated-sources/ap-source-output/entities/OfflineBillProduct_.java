package entities;

import entities.OfflineBill;
import entities.Product;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(OfflineBillProduct.class)
public class OfflineBillProduct_ { 

    public static volatile SingularAttribute<OfflineBillProduct, OfflineBill> offlineBillId;
    public static volatile SingularAttribute<OfflineBillProduct, BigInteger> quantity;
    public static volatile SingularAttribute<OfflineBillProduct, Product> productId;
    public static volatile SingularAttribute<OfflineBillProduct, BigDecimal> id;

}