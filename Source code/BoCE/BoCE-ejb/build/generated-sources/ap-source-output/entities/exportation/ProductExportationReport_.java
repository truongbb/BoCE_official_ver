package entities.exportation;

import entities.common.Unit;
import entities.exportation.ExportationReport;
import entities.product.Product;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(ProductExportationReport.class)
public class ProductExportationReport_ { 

    public static volatile SingularAttribute<ProductExportationReport, Double> unitPrice;
    public static volatile SingularAttribute<ProductExportationReport, ExportationReport> exportedReportId;
    public static volatile SingularAttribute<ProductExportationReport, Product> productId;
    public static volatile SingularAttribute<ProductExportationReport, Unit> unitId;
    public static volatile SingularAttribute<ProductExportationReport, BigDecimal> id;
    public static volatile SingularAttribute<ProductExportationReport, BigInteger> exportedQuantity;

}