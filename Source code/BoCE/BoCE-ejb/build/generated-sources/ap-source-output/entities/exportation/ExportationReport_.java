package entities.exportation;

import entities.employee.Employee;
import entities.exportation.ProductExportationReport;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-12T09:18:11")
@StaticMetamodel(ExportationReport.class)
public class ExportationReport_ { 

    public static volatile SingularAttribute<ExportationReport, String> note;
    public static volatile SingularAttribute<ExportationReport, Date> createdDate;
    public static volatile SingularAttribute<ExportationReport, Employee> employeeId;
    public static volatile SingularAttribute<ExportationReport, BigDecimal> id;
    public static volatile ListAttribute<ExportationReport, ProductExportationReport> productExportationReportList;

}