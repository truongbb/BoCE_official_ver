package entities;

import entities.ClothesCategory;
import entities.ClothesSize;
import entities.Color;
import entities.Product;
import entities.Style;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-07T22:39:59")
@StaticMetamodel(Clothes.class)
public class Clothes_ { 

    public static volatile SingularAttribute<Clothes, ClothesCategory> clothesCategoryId;
    public static volatile SingularAttribute<Clothes, Product> productId;
    public static volatile SingularAttribute<Clothes, ClothesSize> clothesSizeId;
    public static volatile SingularAttribute<Clothes, Color> colorId;
    public static volatile SingularAttribute<Clothes, Style> styleId;
    public static volatile SingularAttribute<Clothes, BigDecimal> clothesId;
    public static volatile SingularAttribute<Clothes, String> name;
    public static volatile SingularAttribute<Clothes, String> description;

}