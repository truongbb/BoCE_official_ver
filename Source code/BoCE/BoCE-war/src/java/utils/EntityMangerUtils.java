package utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author truongbb
 */
public class EntityMangerUtils {

    public static EntityManager getEntityManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BoCE_webPU");
        EntityManager em = emf.createEntityManager();
        return em;
    }
}
