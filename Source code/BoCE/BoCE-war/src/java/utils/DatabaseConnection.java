package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author truongbb
 */
public interface DatabaseConnection {

    // driver
    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    // url
    public static final String ORACLE_URL = "jdbc:oracle:thin:@localhost:1521:xe";
    public static final String SQL_SERVER_URL = "jdbc:sqlserver://localhost:1433;databaseName=boce";
    public static final String MYSQL_URL = "jdbc:mysql://localhost:3306/boce";

    // username
    public static final String ORACLE_USERNAME = "boce";
    public static final String SQL_SERVER_USERNAME = "";
    public static final String MYSQL_USERNAME = "root";

    // pass
    public static final String ORACLE_PASSWORD = "admin";
    public static final String SQL_SERVER_PASSWORD = "";
    public static final String MYSQL_PASSWORD = "";

    Connection openConnection(String username, String password);

    default void closeConnection(ResultSet rs, PreparedStatement ps, Connection cn) {
        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (ps != null && !ps.isClosed()) {
                ps.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (cn != null && !cn.isClosed()) {
                cn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
