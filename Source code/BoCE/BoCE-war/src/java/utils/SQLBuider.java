package utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author truongbb
 */
public class SQLBuider {

    // -------------------------MODULES--------------------------
    // common
    public static final String SQL_MODULE_COMMON_ACCOUNT = "common/account";
    public static final String SQL_MODULE_COMMON_UNIT = "common/unit";
    public static final String SQL_MODULE_COMMON_COLOR = "common/color";
    public static final String SQL_MODULE_COMMON_GENDER = "common/gender";

    // product
    public static final String SQL_MODULE_PRODUCT = "product";
    public static final String SQL_MODULE_PRODUCT_DISCOUNT = "product/discount";
    public static final String SQL_MODULE_PRODUCT_WAREHOUSE = "product/warehouse";
    public static final String SQL_MODULE_PRODUCT_PRODUCTTYPE = "product/productType";

    // customer
    public static final String SQL_MODULE_CUSTOMER = "customer";
    public static final String SQL_MODULE_CUSTOMER_CUSTOMER_TYPE = "customer/customer_type";

    // employee
    public static final String SQL_MODULE_EMPLOYEE = "employee";
    public static final String SQL_MODULE_EMPLOYEE_DEPARTMENT = "employee/department";
    public static final String SQL_MODULE_EMPLOYEE_DISCIPLINE_TYPE = "employee/discipline_type";
    public static final String SQL_MODULE_EMPLOYEE_DISPLINE = "employee/displine";
    public static final String SQL_MODULE_EMPLOYEE_REWARD_TYPE = "employee/reward_type";
    public static final String SQL_MODULE_EMPLOYEE_REWARD = "employee/reward";
    public static final String SQL_MODULE_EMPLOYEE_LITERACY = "employee/literacy";
    public static final String SQL_MODULE_EMPLOYEE_POSITION = "employee/position";
    public static final String SQL_MODULE_EMPLOYEE_SALARY = "employee/salary";

    // import/export
    public static final String SQL_MODULE_EXPORTATION = "exportation";
    public static final String SQL_MODULE_EXPORTATION_PRODUCT = "exportation_product";
    public static final String SQL_MODULE_IMPORTATION = "importation";
    public static final String SQL_MODULE_IMPORTATION_PRODUCT = "importation_product";

    // book
    public static final String SQL_MODULE_BOOK = "product/book";
    public static final String SQL_MODULE_BOOK_CATEGORY = "product/book/category";
    public static final String SQL_MODULE_BOOK_AUTHOR = "product/book/author";
    public static final String SQL_MODULE_BOOK_PUBLISHER = "product/book/publisher";

    // electronics
    public static final String SQL_MODULE_ELECTRONICS = "product/electronics";
    public static final String SQL_MODULE_ELECTRONICS_CATEGORY = "product/electronics/category";

    // clothes
    public static final String SQL_MODULE_CLOTHES = "product/clothes";
    public static final String SQL_MODULE_CLOTHES_CATEGORY = "product/clothes/category";
    public static final String SQL_MODULE_CLOTHES_STYLE = "product/clothes/style";

    // transasction
    public static final String SQL_MODULE_TRANSACTION_PURCHASE_METHOD = "transaction/purchase_method";
    public static final String SQL_MODULE_TRANSACTION_ORDER = "transaction/order";
    public static final String SQL_MODULE_TRANSACTION_BILL = "transaction/bill";

    public static String getSqlQueryById(String module, String queryId) {
        try {
            StringBuilder sql = new StringBuilder("");
            BufferedInputStream bis = new BufferedInputStream(SQLBuider.class.getResourceAsStream("/sql/" + module + "/" + queryId + ".sql"));
            int i = 0;
            while ((i = bis.read()) != -1) {
                sql.append((char) i);
            }
            return sql.toString();
        } catch (IOException ex) {
            Logger.getLogger(SQLBuider.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
