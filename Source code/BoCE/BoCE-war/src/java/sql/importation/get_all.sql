select im_re.id, im_re.warehouse_manager_id, im_re.created_date, im_re.note, 
    po.product_id, po.product_type_id, po_ty.type, po.unit_price, po.discount_id, dis.rate, dis.begin_date, dis.end_date, po.description, 
    po.quantity, po.picture_thumb, po.warehouse_id, wah.name, wah.code, wah.location, wah.phone, wah.email,
    su.id, su.name, su.address, su.email, su.phone,
    u.id, u.value, im_re_po.imported_quantity, im_re_po.unit_price
from importation_report im_re
join importation_report_product im_re_po on im_re_po.importation_id = im_re.id
join unit u on u.id = im_re_po.unit_id
join supplier su on su.id = im_re_po.supplier_id
join employee e on e.id = im_re.warehouse_manager_id
join product po on po.product_id = im_re_po.product_id
join product_type po_ty on po_ty.id = po.product_type_id
join warehouse wah on wah.id = po.warehouse_id
left join discount dis on dis.id = po.discount_id
order by im_re.id