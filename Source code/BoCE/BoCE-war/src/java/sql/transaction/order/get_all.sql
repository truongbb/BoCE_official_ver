select o.id, o.customer_id, c.first_name, c.mid_name, c.last_name, c.address, c.gender_id, g.value, c.birthday, c.email, c.phone,  c.customer_type_id, 
    ct.type, a.id, a.username, a.password, o.receiving_address, o.additional_charge, o.expired_date, o.expected_receiving_date, o.cash, o.purchase_method_id, 
    pm.type, pr.product_id, pr.product_type_id, prt.type, pr.unit_price, pr.discount_id, dis.rate, dis.begin_date, dis.end_date, pr.description,
    pr.quantity, pr.picture_thumb, pr.warehouse_id, wah.name, wah.code, wah.location, wah.phone, wah.email, op.buying_quantity
from orders o
join purchase_method pm on pm.id = o.purchase_method_id
join customer c on c.id = o.customer_id
join gender g on g.id = c.gender_id
join customer_type ct on ct.id = c.customer_type_id
join account a on a.id = c.account_id
join orders_product op on op.order_id = o.id
join product pr on pr.product_id = op.product_id
join product_type prt on prt.id = pr.product_type_id
join warehouse wah on wah.id = pr.warehouse_id
left join discount dis on dis.id = pr.discount_id
order by o.id