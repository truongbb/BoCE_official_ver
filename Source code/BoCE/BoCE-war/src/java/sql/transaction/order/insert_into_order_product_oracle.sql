-- insert into orders_product (id, orders_id, product_id, quantity)
-- with temp_data as(
--     select id, additional_charge, cash, receiving_address, expexted_receiving_date, purchase_method_id, customer_id
--     from orders
--     where additional_charge= :p_addtional_charge
--     and cash = :p_cash
--     and receiving_address = :p_receiving_address
--     and to_char(expexted_receiving_date, 'YYYYMMDD') = :p_expected_receiving_date
--     and purchase_method_id = :p_purchase_method_id
--     and customer_id = :p_customer_id
-- )
-- select orders_product_seq.nextval, t.id, :p_product_id, :p_quantity
-- from temp_data t
insert into orders_product (id, orders_id, product_id, quantity)
with temp_data as(
    select id, additional_charge, cash, receiving_address, expexted_receiving_date, purchase_method_id, customer_id
    from orders
    where additional_charge= ?
    and cash = ?
    and receiving_address = ?
    and to_char(expexted_receiving_date, 'YYYYMMDD') = ?
    and purchase_method_id = ?
    and customer_id = ?
)
select orders_product_seq.nextval, t.id, ?, ?
from temp_data t