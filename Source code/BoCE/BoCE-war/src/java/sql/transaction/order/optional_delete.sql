delete from orders
where id in (
    select id, additional_charge, cash, receiving_address, expexted_receiving_date, purchase_method_id, customer_id
    from orders
    where additional_charge = ?
    and cash = ?
    and receiving_address = ?
    and to_char(expexted_receiving_date, 'YYYYMMDD') = ?
    and purchase_method_id = ?
    and customer_id = ?
)