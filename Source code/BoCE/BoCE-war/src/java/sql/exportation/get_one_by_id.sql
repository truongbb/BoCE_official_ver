select ex_re.id, ex_re.warehouse_manager_id, ex_re.created_date, ex_re.note, 
    po.product_id, po_type.id, po_type.type, po.unit_price, dis.id, dis.rate, dis.begin_date, dis.end_date, po.description, po.quantity, po.picture_thumb,
    wah.id, wah.name, wah.code, wah.location, wah.phone, wah.email,
    ex_re_po.unit_id, u.value, ex_re_po.exported_quantity, ex_re_po.unit_price
from exportation_report ex_re
join exportation_report_product ex_re_po on ex_re.id = ex_re_po.exportation_id
join unit u on u.id = ex_re_po.unit_id
join product po on po.product_id = ex_re_po.product_id
join product_type po_type on po_type.id = po.product_type_id
join warehouse wah on wah.id = po.warehouse_id
left join discount dis on dis.id = po.discount_id
where ex_re.id = ?
order by ex_re.id