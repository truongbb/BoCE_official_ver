select cus.id, cus.first_name, cus.mid_name, cus.last_name, cus.address, gen.id, gen.value, cus.birthday, cus.phone,
        cus.email, custype.id, custype.type, acc.id, acc.username, acc.password
from customer cus
join gender gen on cus.gender_id = gen.id
join customer_type custype on custype.id = cus.customer_type_id
join account acc on acc.id = cus.account_id
order by cus.id