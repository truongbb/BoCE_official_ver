select dis.id, dis.name, dis.discipline_type_id, distype.type
from discipline dis
join discipline_type distype on distype.id = dis.discipline_type_id
where dis.id = ?