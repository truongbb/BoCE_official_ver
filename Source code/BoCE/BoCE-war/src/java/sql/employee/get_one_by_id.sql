select emp.id, emp.first_name, emp.mid_name, emp.last_name, emp.address, gend.id, gend.value, emp.birthday, emp.phone, 
    eth.id, eth.value, emp.start_working_date, lit.id, lit.value, pos.id, pos.name, sal.salary_level, sal.basic_salary, 
    sal.salary_rate, sal.allowance_rate, dept.id, dept.name, dept.phone, acc.id, acc.username, acc.password, 
    dis.id, dis.name, dis_type.id, dis_type.type, dis_emp.discipline_date, dis_emp.forfeit, dis_emp.description,
    rew.id, rew.name, rew_type.id, rew_type.type, rew_emp.reward_date, rew_emp.bonus, rew_emp.description
from employee emp
join department dept on emp.department_id = dept.id
join position pos on pos.id = emp.position_id
join salary sal on sal.salary_level = pos.salary_level
join ethnic eth on eth.id = emp.ethnic_id
join literacy lit on lit.id = emp.literacy_id
join gender gend on gend.id = emp.gender_id
join account acc on acc.id = emp.account_id
left join discipline_employee dis_emp on emp.id = dis_emp.employee_id
left join discipline dis on dis.id = dis_emp.discipline_id
left join discipline_type dis_type on dis_type.id = dis.discipline_type_id
left join reward_employee rew_emp on rew_emp.employee_id = emp.id
left join reward rew on rew.id = rew_emp.reward_id
left join reward_type rew_type on rew.reward_type_id = rew_type.id
where emp.id = ?