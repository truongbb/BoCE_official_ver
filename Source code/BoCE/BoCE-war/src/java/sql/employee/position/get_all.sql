select pos.id, pos.name, pos.salary_level, sal.basic_salary, sal.salary_rate, sal.allowance_rate
from position pos
join salary sal on sal.salary_level = pos.salary_level
order by pos.id