select b.book_id, b.product_id, b.name, b.author_id, a.name, a.description, b.book_category_id, bc.name, b.published_year, b.publisher_id,
    pu.name, pu.phone, pu.email, pu.address, b.total_page,
    pr.product_type_id, pr_ty.type, pr.unit_price, pr.discount_id, dis.rate, dis.begin_date, dis.end_date,pr.description, pr.quantity, 
    pr.picture_thumb, pr.warehouse_id, wah.name, wah.code, wah.location, wah.phone, wah.email
from book b
join book_category bc on bc.id = b.book_category_id
join author a on a.id = b.author_id
join publisher pu on pu.id = b.publisher_id
join product pr on pr.product_id = b.product_id
join product_type pr_ty on pr_ty.id = pr.product_type_id
join warehouse wah on wah.id = pr.warehouse_id
left join discount dis on dis.id = pr.discount_id
where b.book_id = ?
order by b.book_id