select cl.id, cl.product_id, cl.name, cl.clothes_category_id, clca.name, cl.color_id, c.color, c.code, cl.description, cl.style_id, s.value,
    cl.size_id, clsz.clothes_size, pr.product_type_id, pr_ty.type, pr.unit_price, pr.discount_id, dis.rate, dis.begin_date, dis.end_date,
    pr.description, pr.quantity, pr.picture_thumb, pr. warehouse_id, wah.name, wah.code, wah.location, wah. phone, wah.email
from clothes cl
join clothes_category clca on clca.id = cl.clothes_category_id
join style s on s.id = cl.style_id
join clothes_size clsz on clsz.id = cl.size_id
join color c on c.id = cl.color_id
join product pr on pr.product_id = cl.product_id
join product_type pr_ty on pr.product_type_id = pr_ty.id
join warehouse wah on wah.id = pr.warehouse_id
left join discount dis on dis.id = pr.discount_id
order by cl.id