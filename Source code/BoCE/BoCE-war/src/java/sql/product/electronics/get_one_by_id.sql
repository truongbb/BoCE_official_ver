select e.sku, e.product_id, e.name, e. electronic_category_id, ec.name, e.color_id, c.color, c.code, e.weight,
    pr.product_type_id, pr_ty.type, pr.unit_price, pr.discount_id, dis.rate, dis.begin_date, dis.end_date, pr.description, pr.quantity, 
    pr.picture_thumb, pr.warehouse_id, wah.name, wah.code, wah.location, wah.phone, wah.email
from electronics e
join electronic_category ec on e.electronic_category_id = ec.id
join color c on c.id = e.color_id
join product pr on pr.product_id = e.product_id
join product_type pr_ty on pr_ty.id = pr.product_type_id
join warehouse wah on wah.id = pr.warehouse_id
left join discount dis on dis.id = pr.discount_id
where sku = ?
order by e.sku