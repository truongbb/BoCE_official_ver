select pr.product_id, pr.product_type_id, pr_ty.type, pr.unit_price, pr.discount_id, dis.rate, dis.begin_date, dis.end_date, pr.description, 
    pr.quantity, pr.picture_thumb, pr.warehouse_id, wah.name, wah.code, wah.location, wah.phone
from product pr
join product_type pr_ty on pr.product_type_id = pr_ty.id
join warehouse wah on wah.id = pr.warehouse_id
left join discount dis on dis.id = pr.discount_id
where pr.product_id = ?
order by pr.product_id