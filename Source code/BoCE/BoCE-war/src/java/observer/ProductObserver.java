package observer;

import bo.product.Product;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class ProductObserver implements Observable {

    private Product product;
    private boolean isNew;
    private List<Observer> warehouseManagerObservers; // list này lấy từ db ra (khi thủ kho nào click follow 1 product nhất định, rồi truyền vào bằng constructor)

    public ProductObserver() {
    }

    public ProductObserver(Product product, boolean isNew, List<Observer> warehouseManagerObservers) {
        this.product = product;
        this.isNew = isNew;
        this.warehouseManagerObservers = warehouseManagerObservers;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
        notifyObserver();//một sản phẩm được đánh dấu là mới thì sẽ thông báo tới toàn bộ thủ kho đã bấm follow sản phẩm này
    }

    public List<Observer> getWarehouseManagerObservers() {
        return warehouseManagerObservers;
    }

    public void setWarehouseManagerObservers(List<Observer> warehouseManagerObservers) {
        this.warehouseManagerObservers = warehouseManagerObservers;
    }

    @Override
    public void addObserver(Observer observer) {// thêm người theo dõi
        this.warehouseManagerObservers.add(observer);//add rồi ghi lại vào db
    }

    @Override
    public void removeObserver(Observer observer) {// xóa bỏ người theo dõi
        this.warehouseManagerObservers.remove(observer);//remove rồi ghi lại vào db
    }

    @Override
    public void notifyObserver() {// thông báo tới toàn bộ người theo dõi
        this.warehouseManagerObservers.stream().forEach(Observer::update);
    }
}
