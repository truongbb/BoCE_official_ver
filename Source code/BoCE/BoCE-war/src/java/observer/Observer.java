package observer;

/**
 *
 * @author truongbb
 * @source https://dzone.com/articles/observer-pattern-java
 */
public interface Observer {

    void update();

}
