package observer;

import bo.employee.Employee;

/**
 *
 * @author Zhang
 */
public class WarehouseManagerObserver extends Employee implements Observer {

    private Observable observable;

    public WarehouseManagerObserver() {
    }

    public WarehouseManagerObserver(Observable observable) {
        this.observable = observable;
    }

    public Observable getObservable() {
        return observable;
    }

    public void setObservable(Observable observable) {
        this.observable = observable;
    }

    @Override
    public void update() {// thông báo vào trong db
        System.out.println("Có sản phẩm mới rồi");
        unSubcrible();//thông báo xong rồi coi như đã có sp mới, bỏ theo dõi
    }

    public void unSubcrible() {
        this.observable.removeObserver(this);
    }
}
