package dao.product;

import bo.product.Product;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface ProductDAO extends CommonDAO<Product> {

    boolean insertIntoFullTable(Product product, boolean isInsertIntoDiscount);
}
