package dao.product.electronics.category;

import bo.product.electronics.ElectronicCategory;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface ElectronicCategoryDAO extends CommonDAO<ElectronicCategory> {
    
}
