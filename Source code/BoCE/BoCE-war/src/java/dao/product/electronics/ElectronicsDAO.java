package dao.product.electronics;

import bo.product.electronics.Electronics;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface ElectronicsDAO extends CommonDAO<Electronics> {

}
