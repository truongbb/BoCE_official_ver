package dao.product.electronics;

import bo.common.person.Address;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;
import bo.common.Color;
import bo.product.electronics.ElectronicCategory;
import bo.product.electronics.Electronics;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class OracleElectronicsDAOImpl implements ElectronicsDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Electronics> getAll() {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_ELECTRONICS, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Electronics> electronics = new ArrayList<>();
                while (rs.next()) {
                    int sku = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String name = rs.getString(3);
                    int electronicCategoryId = rs.getInt(4);
                    String electronicCategory = rs.getString(5);
                    int colorId = rs.getInt(6);
                    String color = rs.getString(7);
                    String colorCode = rs.getString(8);
                    float weight = rs.getFloat(9);
                    int productTypeId = rs.getInt(10);
                    String productType = rs.getString(11);
                    float unitPrice = rs.getFloat(12);
                    int discountId = rs.getInt(13);
                    float discountRate = rs.getFloat(14);
                    java.util.Date beginDate = rs.getDate(15);
                    java.util.Date endDate = rs.getDate(16);
                    String description = rs.getString(17);
                    int quantity = rs.getInt(18);
                    String pictureThumb = rs.getString(19);
                    int warehouseId = rs.getInt(20);
                    String warehouseName = rs.getString(21);
                    String warehouseCode = rs.getString(22);
                    String warehouseLocation = rs.getString(23);
                    String warehousePhone = rs.getString(24);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    electronics.add(new Electronics(sku, name, new ElectronicCategory(electronicCategoryId,
                            electronicCategory), new Color(colorId, color, colorCode), weight, productId,
                            new ProductType(productTypeId, productType), unitPrice, discount, description,
                            quantity, pictureThumb, new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone)));
                }
                return electronics;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerElectronicsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Electronics getOneById(int id) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_ELECTRONICS, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int sku = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String name = rs.getString(3);
                    int electronicCategoryId = rs.getInt(4);
                    String electronicCategory = rs.getString(5);
                    int colorId = rs.getInt(6);
                    String color = rs.getString(7);
                    String colorCode = rs.getString(8);
                    float weight = rs.getFloat(9);
                    int productTypeId = rs.getInt(10);
                    String productType = rs.getString(11);
                    float unitPrice = rs.getFloat(12);
                    int discountId = rs.getInt(13);
                    float discountRate = rs.getFloat(14);
                    java.util.Date beginDate = rs.getDate(15);
                    java.util.Date endDate = rs.getDate(16);
                    String description = rs.getString(17);
                    int quantity = rs.getInt(18);
                    String pictureThumb = rs.getString(19);
                    int warehouseId = rs.getInt(20);
                    String warehouseName = rs.getString(21);
                    String warehouseCode = rs.getString(22);
                    String warehouseLocation = rs.getString(23);
                    String warehousePhone = rs.getString(24);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    return new Electronics(sku, name, new ElectronicCategory(electronicCategoryId,
                            electronicCategory), new Color(colorId, color, colorCode), weight, productId,
                            new ProductType(productTypeId, productType), unitPrice, discount, description,
                            quantity, pictureThumb, new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone));
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerElectronicsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Electronics electronics) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_ELECTRONICS, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, electronics.getId());
                ps.setString(2, electronics.getName());
                ps.setInt(3, electronics.getElectronicCategory().getId());
                ps.setInt(4, electronics.getColor().getId());
                ps.setFloat(5, electronics.getWeigh());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerElectronicsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Electronics electronics) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_ELECTRONICS, "update");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, electronics.getId());
                ps.setString(2, electronics.getName());
                ps.setInt(3, electronics.getElectronicCategory().getId());
                ps.setInt(4, electronics.getColor().getId());
                ps.setFloat(5, electronics.getWeigh());
                ps.setInt(6, electronics.getElectronicSku());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerElectronicsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Electronics electronics) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_ELECTRONICS, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, electronics.getElectronicSku());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerElectronicsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Electronics> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
