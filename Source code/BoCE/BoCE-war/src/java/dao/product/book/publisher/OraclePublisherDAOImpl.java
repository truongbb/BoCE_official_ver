package dao.product.book.publisher;

import bo.common.person.Address;
import bo.product.book.Publisher;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class OraclePublisherDAOImpl implements PublisherDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Publisher> getAll() {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_PUBLISHER, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Publisher> publishers = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String phone = rs.getString(3);
                    String email = rs.getString(4);
                    String address = rs.getString(5);
                    publishers.add(new Publisher(id, name, phone, email, new Address(address.split("#")[0], address.split("#")[1], address.split("#")[2], address.split("#")[3])));
                }
                return publishers;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerPublisherDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Publisher getOneById(int id) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_PUBLISHER, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String name = rs.getString(2);
                    String phone = rs.getString(3);
                    String email = rs.getString(4);
                    String address = rs.getString(5);
                    return new Publisher(id, name, phone, email, new Address(address.split("#")[0], address.split("#")[1], address.split("#")[2], address.split("#")[3]));
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerPublisherDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Publisher publisher) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_PUBLISHER, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, publisher.getName());
                ps.setString(2, publisher.getPhone());
                ps.setString(3, publisher.getEmail());
                ps.setString(4, publisher.getAddress().toString());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerPublisherDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Publisher publisher) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_PUBLISHER, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, publisher.getName());
                ps.setString(2, publisher.getPhone());
                ps.setString(3, publisher.getEmail());
                ps.setString(4, publisher.getAddress().toString());
                ps.setInt(5, publisher.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerPublisherDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Publisher publisher) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_PUBLISHER, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, publisher.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerPublisherDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Publisher> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
