package dao.product.book;

import bo.common.person.Address;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;
import bo.product.book.Author;
import bo.product.book.Book;
import bo.product.book.BookCategory;
import bo.product.book.Publisher;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class MysqlBookDAOImpl implements BookDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<Book> getAll() {
        Connection connection = mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Book> books = new ArrayList<>();
                while (rs.next()) {
                    int bookId = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String bookName = rs.getString(3);
                    int authorId = rs.getInt(4);
                    String authorName = rs.getString(5);
                    String authorDescription = rs.getString(6);
                    int bookCategoryId = rs.getInt(7);
                    String bookCategoryName = rs.getString(8);
                    int publishedYear = rs.getInt(9);
                    int publisherId = rs.getInt(10);
                    String publisherName = rs.getString(11);
                    String publisherPhone = rs.getString(12);
                    String publisherEmail = rs.getString(13);
                    String publisherAddress = rs.getString(14);
                    int totalPage = rs.getInt(15);
                    int productTypeId = rs.getInt(16);
                    String productType = rs.getString(17);
                    float unitPrice = rs.getFloat(18);
                    int discountId = rs.getInt(19);
                    float discountRate = rs.getFloat(20);
                    java.util.Date beginDate = rs.getDate(21);
                    java.util.Date endDate = rs.getDate(22);
                    String productDescription = rs.getString(23);
                    int productQuantity = rs.getInt(24);
                    String pictureThumb = rs.getString(25);
                    int warehouseId = rs.getInt(26);
                    String warehouseName = rs.getString(27);
                    String warehouseCode = rs.getString(28);
                    String warehouseLocation = rs.getString(29);
                    String warehousePhone = rs.getString(30);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    books.add(new Book(bookId, bookName, new Author(authorId, authorName, authorDescription),
                            new Publisher(publisherId, publisherName, publisherPhone, publisherEmail,
                                    new Address(publisherAddress.split("#")[0], publisherAddress.split("#")[1],
                                            publisherAddress.split("#")[2], publisherAddress.split("#")[3])),
                            new BookCategory(bookCategoryId, bookCategoryName), publishedYear, totalPage, productId,
                            new ProductType(productTypeId, productType), unitPrice, discount, productDescription,
                            productQuantity, pictureThumb, new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone)));

                }
                return books;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Book getOneById(int id) {
        Connection connection = mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int bookId = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String bookName = rs.getString(3);
                    int authorId = rs.getInt(4);
                    String authorName = rs.getString(5);
                    String authorDescription = rs.getString(6);
                    int bookCategoryId = rs.getInt(7);
                    String bookCategoryName = rs.getString(8);
                    int publishedYear = rs.getInt(9);
                    int publisherId = rs.getInt(10);
                    String publisherName = rs.getString(11);
                    String publisherPhone = rs.getString(12);
                    String publisherEmail = rs.getString(13);
                    String publisherAddress = rs.getString(14);
                    int totalPage = rs.getInt(15);
                    int productTypeId = rs.getInt(16);
                    String productType = rs.getString(17);
                    float unitPrice = rs.getFloat(18);
                    int discountId = rs.getInt(19);
                    float discountRate = rs.getFloat(20);
                    java.util.Date beginDate = rs.getDate(21);
                    java.util.Date endDate = rs.getDate(22);
                    String productDescription = rs.getString(23);
                    int productQuantity = rs.getInt(24);
                    String pictureThumb = rs.getString(25);
                    int warehouseId = rs.getInt(26);
                    String warehouseName = rs.getString(27);
                    String warehouseCode = rs.getString(28);
                    String warehouseLocation = rs.getString(29);
                    String warehousePhone = rs.getString(30);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    return new Book(bookId, bookName, new Author(authorId, authorName, authorDescription),
                            new Publisher(publisherId, publisherName, publisherPhone, publisherEmail,
                                    new Address(publisherAddress.split("#")[0], publisherAddress.split("#")[1],
                                            publisherAddress.split("#")[2], publisherAddress.split("#")[3])),
                            new BookCategory(bookCategoryId, bookCategoryName), publishedYear, totalPage, productId,
                            new ProductType(productTypeId, productType), unitPrice, discount, productDescription,
                            productQuantity, pictureThumb, new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone));

                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Book book) {
        Connection connection = mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, book.getName());
                ps.setInt(2, book.getAuthor().getId());
                ps.setInt(3, book.getBookCategory().getId());
                ps.setInt(4, book.getPublishedYear());
                ps.setInt(5, book.getPublisher().getId());
                ps.setInt(6, book.getTotalPage());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Book book) {
        Connection connection = mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, book.getName());
                ps.setInt(2, book.getAuthor().getId());
                ps.setInt(3, book.getBookCategory().getId());
                ps.setInt(4, book.getPublishedYear());
                ps.setInt(5, book.getPublisher().getId());
                ps.setInt(6, book.getTotalPage());
                ps.setInt(7, book.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Book book) {
        Connection connection = mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, book.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Book> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
