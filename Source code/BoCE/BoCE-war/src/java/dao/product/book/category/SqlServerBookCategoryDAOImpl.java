package dao.product.book.category;

import bo.product.book.BookCategory;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class SqlServerBookCategoryDAOImpl implements BookCategoryDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<BookCategory> getAll() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_CATEGORY, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<BookCategory> bookCategories = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    bookCategories.add(new BookCategory(id, name));
                }
                return bookCategories;
            } catch (SQLException ex) {
                Logger.getLogger(OracleBookCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public BookCategory getOneById(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_CATEGORY, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String name = rs.getString(2);
                    return new BookCategory(gottenId, name);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleBookCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(BookCategory bookCategory) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_CATEGORY, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, bookCategory.getName());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleBookCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(BookCategory bookCategory) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_CATEGORY, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, bookCategory.getName());
                ps.setInt(2, bookCategory.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleBookCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(BookCategory bookCategory) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_BOOK_CATEGORY, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, bookCategory.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleBookCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<BookCategory> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
