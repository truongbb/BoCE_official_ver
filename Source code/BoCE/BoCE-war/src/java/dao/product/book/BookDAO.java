package dao.product.book;

import bo.product.book.Book;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface BookDAO extends CommonDAO<Book>{
    
}
