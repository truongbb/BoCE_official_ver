package dao.product.book.author;

import bo.product.book.Author;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface AuthorDAO extends CommonDAO<Author> {

}
