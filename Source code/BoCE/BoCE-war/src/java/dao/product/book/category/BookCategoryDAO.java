package dao.product.book.category;

import bo.product.book.BookCategory;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface BookCategoryDAO extends CommonDAO<BookCategory> {

}
