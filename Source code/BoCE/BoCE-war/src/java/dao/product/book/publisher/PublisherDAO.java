package dao.product.book.publisher;

import bo.product.book.Publisher;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface PublisherDAO extends CommonDAO<Publisher> {

}
