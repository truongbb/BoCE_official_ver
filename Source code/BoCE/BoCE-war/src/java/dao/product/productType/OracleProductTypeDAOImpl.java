package dao.product.productType;

import bo.product.ProductType;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class OracleProductTypeDAOImpl implements ProductTypeDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<ProductType> getAll() {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_PRODUCTTYPE, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<ProductType> productTypes = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    productTypes.add(new ProductType(id, name));
                }
                return productTypes;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public ProductType getOneById(int id) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                ProductType productType = null;
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_PRODUCTTYPE, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String type = rs.getString(2);
                    return new ProductType(gottenId, type);
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ProductType productType) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_PRODUCTTYPE, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, productType.getName());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(ProductType pt) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_PRODUCTTYPE, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, pt.getName());
                ps.setInt(2, pt.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(ProductType pt) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_PRODUCTTYPE, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, pt.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<ProductType> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
