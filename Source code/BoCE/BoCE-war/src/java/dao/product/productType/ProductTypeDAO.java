package dao.product.productType;

import bo.product.ProductType;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface ProductTypeDAO extends CommonDAO<ProductType> {

}
