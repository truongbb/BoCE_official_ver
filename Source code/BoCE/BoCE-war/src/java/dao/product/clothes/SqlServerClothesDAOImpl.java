package dao.product.clothes;

import bo.common.Color;
import bo.common.person.Address;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;
import bo.product.clothes.Clothes;
import bo.product.clothes.ClothesCategory;
import bo.product.clothes.ClothesSize;
import bo.product.clothes.Style;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class SqlServerClothesDAOImpl implements ClothesDAO {

    SqlServerDAOFactory sQLServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Clothes> getAll() {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Clothes> clothes = new ArrayList<>();
                while (rs.next()) {
                    int clothesId = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String name = rs.getString(3);
                    int clothesCategoryId = rs.getInt(4);
                    String clothesCategory = rs.getString(5);
                    int colorId = rs.getInt(6);
                    String colorValue = rs.getString(7);
                    String colorCode = rs.getString(8);
                    String clothesDescription = rs.getString(9);
                    int styleId = rs.getInt(10);
                    String styleValue = rs.getString(11);
                    int sizeId = rs.getInt(12);
                    String sizeValue = rs.getString(13);
                    int productTypeId = rs.getInt(14);
                    String productType = rs.getString(15);
                    float unitPrice = rs.getFloat(16);
                    int discountId = rs.getInt(17);
                    float discountRate = rs.getFloat(18);
                    java.util.Date beginDate = rs.getDate(19);
                    java.util.Date endDate = rs.getDate(20);
                    String productDescription = rs.getString(21);
                    int productQuantity = rs.getInt(22);
                    String pictureThumb = rs.getString(23);
                    int warehouseId = rs.getInt(24);
                    String warehouseName = rs.getString(25);
                    String warehouseCode = rs.getString(26);
                    String warehouseLocation = rs.getString(27);
                    String warehousePhone = rs.getString(28);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    clothes.add(new Clothes(clothesId, name, new ClothesCategory(clothesCategoryId, clothesCategory),
                            new Color(colorId, colorValue, colorCode), clothesDescription,
                            new Style(styleId, styleValue), new ClothesSize(sizeId, sizeValue),
                            productId, new ProductType(productTypeId, productType), unitPrice, discount,
                            productDescription, productQuantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone)));
                }
                return clothes;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerClothesDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Clothes getOneById(int id) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int clothesId = rs.getInt(1);
                    int productId = rs.getInt(2);
                    String name = rs.getString(3);
                    int clothesCategoryId = rs.getInt(4);
                    String clothesCategory = rs.getString(5);
                    int colorId = rs.getInt(6);
                    String colorValue = rs.getString(7);
                    String colorCode = rs.getString(8);
                    String clothesDescription = rs.getString(9);
                    int styleId = rs.getInt(10);
                    String styleValue = rs.getString(11);
                    int sizeId = rs.getInt(12);
                    String sizeValue = rs.getString(13);
                    int productTypeId = rs.getInt(14);
                    String productType = rs.getString(15);
                    float unitPrice = rs.getFloat(16);
                    int discountId = rs.getInt(17);
                    float discountRate = rs.getFloat(18);
                    java.util.Date beginDate = rs.getDate(19);
                    java.util.Date endDate = rs.getDate(20);
                    String productDescription = rs.getString(21);
                    int productQuantity = rs.getInt(22);
                    String pictureThumb = rs.getString(23);
                    int warehouseId = rs.getInt(24);
                    String warehouseName = rs.getString(25);
                    String warehouseCode = rs.getString(26);
                    String warehouseLocation = rs.getString(27);
                    String warehousePhone = rs.getString(28);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    return new Clothes(clothesId, name, new ClothesCategory(clothesCategoryId, clothesCategory),
                            new Color(colorId, colorValue, colorCode), clothesDescription,
                            new Style(styleId, styleValue), new ClothesSize(sizeId, sizeValue),
                            productId, new ProductType(productTypeId, productType), unitPrice, discount,
                            productDescription, productQuantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone));
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerClothesDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Clothes clothes) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, clothes.getId());
                ps.setString(2, clothes.getName());
                ps.setInt(3, clothes.getClothesCategory().getId());
                ps.setInt(4, clothes.getMainColor().getId());
                ps.setString(5, clothes.getClothesDescription());
                ps.setInt(6, clothes.getStyle().getId());
                ps.setInt(7, clothes.getSize().getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerClothesDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Clothes clothes) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES, "update");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, clothes.getId());
                ps.setString(2, clothes.getName());
                ps.setInt(3, clothes.getClothesCategory().getId());
                ps.setInt(4, clothes.getMainColor().getId());
                ps.setString(5, clothes.getClothesDescription());
                ps.setInt(6, clothes.getStyle().getId());
                ps.setInt(7, clothes.getSize().getId());
                ps.setInt(8, clothes.getClothesId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerClothesDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Clothes clothes) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, clothes.getClothesId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerClothesDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Clothes> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
