package dao.product.clothes.category;

import bo.product.clothes.ClothesCategory;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class MysqlClothesCategoryDAOImpl implements ClothesCategoryDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<ClothesCategory> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES_CATEGORY, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<ClothesCategory> clothesCategories = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    clothesCategories.add(new ClothesCategory(id, name));
                }
                return clothesCategories;
            } catch (SQLException ex) {
                Logger.getLogger(OracleClothesCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }

        }
        return null;
    }

    @Override
    public ClothesCategory getOneById(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES_CATEGORY, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String name = rs.getString(2);
                    return new ClothesCategory(gottenId, name);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleClothesCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ClothesCategory clothesCategory) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES_CATEGORY, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, clothesCategory.getName());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleClothesCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(ClothesCategory clothesCategory) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES_CATEGORY, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, clothesCategory.getName());
                ps.setInt(2, clothesCategory.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleClothesCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(ClothesCategory clothesCategory) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CLOTHES_CATEGORY, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, clothesCategory.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleClothesCategoryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<ClothesCategory> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
