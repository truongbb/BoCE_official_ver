package dao.product.clothes.style;

import bo.product.clothes.Style;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface StyleDAO extends CommonDAO<Style> {

}
