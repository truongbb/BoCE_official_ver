package dao.product.clothes.category;

import bo.product.clothes.ClothesCategory;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface ClothesCategoryDAO extends CommonDAO<ClothesCategory> {
    
}
