package dao.product.clothes;

import bo.product.clothes.Clothes;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface ClothesDAO extends CommonDAO<Clothes> {

}
