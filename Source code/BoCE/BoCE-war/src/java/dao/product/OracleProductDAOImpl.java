package dao.product;

import bo.common.person.Address;
import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class OracleProductDAOImpl implements ProductDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Product> getAll() {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Product> products = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    //productType
                    int productTypeId = rs.getInt(2);
                    String productTypeName = rs.getString(3);
                    float unitPrice = rs.getFloat(4);
                    //Discount
                    int discountId = rs.getInt(5);
                    float discountRate = rs.getFloat(6);
                    java.util.Date discountBeginDate = rs.getDate(7);
                    java.util.Date discountEndDate = rs.getDate(8);
                    String description = rs.getString(9);
                    int quantity = rs.getInt(10);
                    String pictureThumb = rs.getString(11);
                    //warehouse
                    int warehouseId = rs.getInt(12);
                    String warehouseName = rs.getString(13);
                    String warehouseCode = rs.getString(14);
                    String warehouseLocation = rs.getString(15);
                    String warehousePhone = rs.getString(16);
                    String warehouseEmail = rs.getString(17);

                    Discount discount = null;
                    if (discountBeginDate != null) {
                        discount = new Discount(discountId, discountRate, discountBeginDate, discountEndDate);
                    }

                    products.add(new Product(id, new ProductType(productTypeId, productTypeName),
                            unitPrice, discount, description, quantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone)));
                }
                return products;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Product getOneById(int id) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    //productType
                    int productTypeId = rs.getInt(2);
                    String productTypeName = rs.getString(3);
                    float unitPrice = rs.getFloat(4);
                    //Discount
                    int discountId = rs.getInt(5);
                    float discountRate = rs.getFloat(6);
                    java.util.Date discountBeginDate = rs.getDate(7);
                    java.util.Date discountEndDate = rs.getDate(8);
                    String description = rs.getString(9);
                    int quantity = rs.getInt(10);
                    String pictureThumb = rs.getString(11);
                    //warehouse
                    int warehouseId = rs.getInt(12);
                    String warehouseName = rs.getString(13);
                    String warehouseCode = rs.getString(14);
                    String warehouseLocation = rs.getString(15);
                    String warehousePhone = rs.getString(16);

                    Discount discount = null;
                    if (discountBeginDate != null) {
                        discount = new Discount(discountId, discountRate, discountBeginDate, discountEndDate);
                    }

                    return new Product(id, new ProductType(productTypeId, productTypeName),
                            unitPrice, discount, description, quantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split(",")[0], warehouseLocation.split(",")[1],
                                            warehouseLocation.split(",")[2], warehouseLocation.split(",")[3]),
                                    warehousePhone));
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Product p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertIntoFullTable(Product product, boolean isInsertIntoDiscount) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);

                // insert into discount
                if (isInsertIntoDiscount) {
                    String discountSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "insert_oracle");
                    ps = connection.prepareStatement(discountSql);
                    ps.setFloat(1, product.getDiscount().getRate());
                    ps.setString(2, product.getDiscount().getBeginDate().getYear() + "" + product.getDiscount().getBeginDate().getMonth() + "" + product.getDiscount().getBeginDate().getDate());
                    ps.setString(3, product.getDiscount().getEndDate().getYear() + "" + product.getDiscount().getEndDate().getMonth() + "" + product.getDiscount().getEndDate().getDate());
                    if (ps.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }
                }

                //insert into product
                String productSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT, "insert_oracle");
                ps = connection.prepareStatement(productSql);
                ps.setInt(1, product.getProductType().getId());
                ps.setFloat(2, product.getUnitPrice());
                if (product.getDiscount() != null) {
                    ps.setInt(3, product.getDiscount().getId());
                } else {
                    ps.setInt(3, 0);
                }
                ps.setString(4, product.getDescription());
                ps.setInt(5, product.getQuantity());
                ps.setString(6, product.getPictureThumb());
                ps.setInt(7, product.getWarehouse().getId());
                if (ps.executeUpdate() > 0) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OracleProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Product product) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT, "update");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, product.getProductType().getId());
                ps.setFloat(2, product.getUnitPrice());
                if (product.getDiscount() != null) {
                    ps.setInt(3, product.getDiscount().getId());
                } else {
                    ps.setInt(3, 0);
                }
                ps.setString(4, product.getDescription());
                ps.setInt(5, product.getQuantity());
                ps.setString(6, product.getPictureThumb());
                ps.setInt(7, product.getWarehouse().getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Product p) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, p.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerProductDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Product> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
