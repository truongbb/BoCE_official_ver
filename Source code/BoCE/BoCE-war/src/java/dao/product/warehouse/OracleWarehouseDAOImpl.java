package dao.product.warehouse;

import bo.common.person.Address;
import bo.product.Warehouse;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class OracleWarehouseDAOImpl implements WarehouseDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Warehouse> getAll() {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_WAREHOUSE, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Warehouse> warehouses = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String code = rs.getString(3);
                    String location = rs.getString(4);
                    String phone = rs.getString(5);
                    warehouses.add(new Warehouse(id, name, code,
                            new Address(location.split("#")[0], location.split("#")[1],
                                    location.split("#")[2], location.split("#")[3]), phone));
                }
                return warehouses;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerWarehouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Warehouse getOneById(int id) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_WAREHOUSE, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String name = rs.getString(2);
                    String code = rs.getString(3);
                    String location = rs.getString(4);
                    String phone = rs.getString(5);
                    return new Warehouse(gottenId, name, code,
                            new Address(location.split("#")[0], location.split("#")[1], location.split("#")[2],
                                    location.split("#")[3]), phone);
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerWarehouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Warehouse warehouse) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_WAREHOUSE, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, warehouse.getName());
                ps.setString(2, warehouse.getCode());
                ps.setString(3, warehouse.getLocation().toString());
                ps.setString(4, warehouse.getPhone());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerWarehouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Warehouse w) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_WAREHOUSE, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, w.getName());
                ps.setString(2, w.getCode());
                ps.setString(3, w.getLocation().toString());
                ps.setString(4, w.getPhone());
                ps.setInt(6, w.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerWarehouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Warehouse w) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_WAREHOUSE, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, w.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerWarehouseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Warehouse> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
