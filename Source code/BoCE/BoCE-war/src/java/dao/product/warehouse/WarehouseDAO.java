package dao.product.warehouse;

import bo.product.Warehouse;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface WarehouseDAO extends CommonDAO<Warehouse>{
    
}
