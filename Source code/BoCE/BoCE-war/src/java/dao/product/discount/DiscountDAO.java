package dao.product.discount;

import bo.product.Discount;
import dao.common.CommonDAO;

/**
 *
 * @author cuongnd
 */
public interface DiscountDAO extends CommonDAO<Discount>{
    
}
