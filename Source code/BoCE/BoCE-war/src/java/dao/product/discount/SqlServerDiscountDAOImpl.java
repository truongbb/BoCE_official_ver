package dao.product.discount;

import bo.product.Discount;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author cuongnd
 */
public class SqlServerDiscountDAOImpl implements DiscountDAO {

    SqlServerDAOFactory sQLServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Discount> getAll() {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Discount> discounts = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    float rate = rs.getFloat(2);
                    java.util.Date beginDate = rs.getDate(3);
                    java.util.Date endDate = rs.getDate(4);
                    Discount discount = new Discount(id, rate, beginDate, endDate);
                    discounts.add(discount);
                }
                return discounts;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerDiscountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Discount getOneById(int id) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    float rate = rs.getFloat(2);
                    java.util.Date beginDate = rs.getDate(3);
                    java.util.Date endDate = rs.getDate(4);
                    return new Discount(gottenId, rate, beginDate, endDate);
                }
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerDiscountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Discount discount) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setFloat(1, discount.getRate());
                String beginDateString = discount.getBeginDate().getYear() + "-" + discount.getBeginDate().getMonth() + "-" + discount.getBeginDate().getDay();
                ps.setDate(2, java.sql.Date.valueOf(beginDateString));
                String endDateString = discount.getEndDate().getYear() + "-" + discount.getEndDate().getMonth() + "-" + discount.getEndDate().getDay();
                ps.setDate(3, java.sql.Date.valueOf(endDateString));
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerDiscountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Discount discount) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "update");
                ps = connection.prepareStatement(sql);
                ps.setFloat(1, discount.getRate());
                ps.setDate(2, java.sql.Date.valueOf(discount.getBeginDate().getYear() + "-" + discount.getBeginDate().getMonth() + "-" + discount.getBeginDate().getDay()));
                ps.setDate(3, java.sql.Date.valueOf(discount.getEndDate().getYear() + "-" + discount.getEndDate().getMonth() + "-" + discount.getEndDate().getDay()));
                ps.setInt(4, discount.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerDiscountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Discount d) {
        Connection connection = sQLServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, d.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(SqlServerDiscountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                sQLServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Discount> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
