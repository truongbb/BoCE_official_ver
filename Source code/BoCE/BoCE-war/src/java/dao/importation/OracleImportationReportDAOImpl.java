package dao.importation;

import bo.common.Pentad;
import bo.common.Unit;
import bo.common.person.Address;
import bo.employee.Employee;
import bo.importation.ImportationReport;
import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Supplier;
import bo.product.Warehouse;
import builder.importation.ImportationReportBuilderImpl;
import builder.importation.ImportationReportDirector;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class OracleImportationReportDAOImpl implements ImportationReportDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<ImportationReport> getAll() {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_IMPORTATION, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<ImportationReport> importationReports = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    int warehouseManagerId = rs.getInt(2);
                    java.util.Date createdDate = rs.getDate(3);
                    String note = rs.getString(4);
                    int productId = rs.getInt(5);
                    int productTypeId = rs.getInt(6);
                    String type = rs.getString(7);
                    float productUnitPrice = rs.getFloat(8);
                    int discountId = rs.getInt(9);
                    float discountRate = rs.getFloat(10);
                    java.util.Date beginDate = rs.getDate(11);
                    java.util.Date endDate = rs.getDate(12);
                    String description = rs.getString(13);
                    int productQuantity = rs.getInt(14);
                    String pictureThumb = rs.getString(15);
                    int warehouseId = rs.getInt(16);
                    String warehouseName = rs.getString(17);
                    String warehouseCode = rs.getString(18);
                    String warehouseLocation = rs.getString(19);
                    String warehousePhone = rs.getString(20);
                    int supplierId = rs.getInt(21);
                    String supplierName = rs.getString(22);
                    String supplierAddress = rs.getString(23);
                    String supplierEmail = rs.getString(24);
                    String supplierPhone = rs.getString(25);
                    int unitId = rs.getInt(26);
                    String unitValue = rs.getString(27);
                    int importedQuantity = rs.getInt(28);
                    float importedUnitPrice = rs.getFloat(29);

                    Employee employee = new Employee();
                    employee.setId(warehouseManagerId);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    // <editor-fold desc="using builder pattern - truongbb">
                    ImportationReportDirector director = new ImportationReportDirector();
                    ImportationReportBuilderImpl builder = new ImportationReportBuilderImpl();
                    director.construct(builder, id, employee, Arrays.asList(new Pentad<>(
                            new Product(productId, new ProductType(productTypeId, type),
                                    productUnitPrice, discount, description, productQuantity, pictureThumb,
                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
                                            new Address(warehouseLocation.split("#")[0],
                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
                                                    warehouseLocation.split("#")[3]), warehousePhone)),
                            new Supplier(supplierId, supplierName, new Address(supplierAddress.split("#")[0],
                                    supplierAddress.split("#")[1], supplierAddress.split("#")[2],
                                    supplierAddress.split("#")[3]), supplierEmail, supplierPhone),
                            new Unit(unitId, unitValue), importedQuantity,
                            importedUnitPrice)).stream().collect(Collectors.toList()), createdDate, note);
                    importationReports.add(builder.getImportationReport());
                    // </editor-fold>

                    // <editor-fold desc="normal way - truongbb">
//                    importationReports.add(new ImportationReport(id, employee, Arrays.asList(new Pentad<>(
//                            new Product(productId, new ProductType(productTypeId, type),
//                                    productUnitPrice, discount, description, productQuantity, pictureThumb,
//                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
//                                            new Address(warehouseLocation.split("#")[0],
//                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
//                                                    warehouseLocation.split("#")[3]), warehousePhone,
//                                            warehouseEmail)),
//                            new Supplier(supplierId, supplierName, new Address(supplierAddress.split("#")[0],
//                                    supplierAddress.split("#")[1], supplierAddress.split("#")[2],
//                                    supplierAddress.split("#")[3]), supplierEmail, supplierPhone),
//                            new Unit(unitId, unitValue), importedQuantity, importedUnitPrice)).stream().collect(Collectors.toList()), createdDate, note));
                    // </editor-fold>
                }
                return importationReports;
            } catch (SQLException ex) {
                Logger.getLogger(OracleImportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public ImportationReport getOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ImportationReport> getOneByIdUnderTheList(int id) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_IMPORTATION, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                List<ImportationReport> importationReports = new ArrayList<>();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    int warehouseManagerId = rs.getInt(2);
                    java.util.Date createdDate = rs.getDate(3);
                    String note = rs.getString(4);
                    int productId = rs.getInt(5);
                    int productTypeId = rs.getInt(6);
                    String type = rs.getString(7);
                    float productUnitPrice = rs.getFloat(8);
                    int discountId = rs.getInt(9);
                    float discountRate = rs.getFloat(10);
                    java.util.Date beginDate = rs.getDate(11);
                    java.util.Date endDate = rs.getDate(12);
                    String description = rs.getString(13);
                    int productQuantity = rs.getInt(14);
                    String pictureThumb = rs.getString(15);
                    int warehouseId = rs.getInt(16);
                    String warehouseName = rs.getString(17);
                    String warehouseCode = rs.getString(18);
                    String warehouseLocation = rs.getString(19);
                    String warehousePhone = rs.getString(20);
                    int supplierId = rs.getInt(21);
                    String supplierName = rs.getString(22);
                    String supplierAddress = rs.getString(23);
                    String supplierEmail = rs.getString(24);
                    String supplierPhone = rs.getString(25);
                    int unitId = rs.getInt(26);
                    String unitValue = rs.getString(27);
                    int importedQuantity = rs.getInt(28);
                    float importedUnitPrice = rs.getFloat(29);

                    Employee employee = new Employee();
                    employee.setId(warehouseManagerId);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    importationReports.add(new ImportationReport(gottenId, employee, Arrays.asList(new Pentad<>(
                            new Product(productId, new ProductType(productTypeId, type),
                                    productUnitPrice, discount, description, productQuantity, pictureThumb,
                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
                                            new Address(warehouseLocation.split("#")[0],
                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
                                                    warehouseLocation.split("#")[3]), warehousePhone)),
                            new Supplier(supplierId, supplierName, new Address(supplierAddress.split("#")[0],
                                    supplierAddress.split("#")[1], supplierAddress.split("#")[2],
                                    supplierAddress.split("#")[3]), supplierEmail, supplierPhone),
                            new Unit(unitId, unitValue), importedQuantity, importedUnitPrice)).stream().collect(Collectors.toList()), createdDate, note));
                }
                return importationReports;
            } catch (SQLException ex) {
                Logger.getLogger(OracleImportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ImportationReport> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
