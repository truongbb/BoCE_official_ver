package dao.importation;

import bo.importation.ImportationReport;
import java.util.List;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface ImportationReportDAO extends CommonDAO<ImportationReport> {

    List<ImportationReport> getOneByIdUnderTheList(int id);

}
