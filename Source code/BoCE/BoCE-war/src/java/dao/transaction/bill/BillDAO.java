package dao.transaction.bill;

import bo.transaction.Bill;
import dao.common.CommonDAO;

/**
 *
 * @author trongbb
 */
public interface BillDAO extends CommonDAO<Bill> {

}
