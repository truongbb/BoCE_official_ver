package dao.transaction.purchase_method;

import bo.transaction.PurchaseMethod;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class MysqlPurchaseMethodDAOImpl implements PurchaseMethodDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<PurchaseMethod> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_PURCHASE_METHOD, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<PurchaseMethod> purchaseMethods = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String type = rs.getString(2);
                    purchaseMethods.add(new PurchaseMethod(id, type));
                }
                return purchaseMethods;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePurchaseMethodDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public PurchaseMethod getOneById(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_PURCHASE_METHOD, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String type = rs.getString(2);
                    return new PurchaseMethod(id, type);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OraclePurchaseMethodDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(PurchaseMethod purchaseMethod) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_PURCHASE_METHOD, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, purchaseMethod.getType());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePurchaseMethodDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(PurchaseMethod purchaseMethod) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_PURCHASE_METHOD, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, purchaseMethod.getType());
                ps.setInt(2, purchaseMethod.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePurchaseMethodDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(PurchaseMethod purchaseMethod) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_PURCHASE_METHOD, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, purchaseMethod.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePurchaseMethodDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<PurchaseMethod> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
