package dao.transaction.purchase_method;

import bo.transaction.PurchaseMethod;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface PurchaseMethodDAO extends CommonDAO<PurchaseMethod> {
    
}
