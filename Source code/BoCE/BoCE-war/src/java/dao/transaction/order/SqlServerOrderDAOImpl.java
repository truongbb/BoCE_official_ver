package dao.transaction.order;

import bo.common.Account;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.customer.Customer;
import bo.customer.CustomerType;
import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;
import bo.transaction.Order;
import bo.transaction.PurchaseMethod;
import dao.common.SqlServerDAOFactory;
import dto.transaction.order.OrderDto;
import entities.transaction.order.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class SqlServerOrderDAOImpl implements OrderDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Order> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<OrderDto> getAllDto() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_ORDER, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<OrderDto> orderDtos = new ArrayList<>();
                while (rs.next()) {
                    int orderId = rs.getInt(1);
                    int customerId = rs.getInt(2);
                    String firstName = rs.getString(3);
                    String midName = rs.getString(4);
                    String lastName = rs.getString(5);
                    String customerAddress = rs.getString(6);
                    int genderId = rs.getInt(7);
                    String genderValue = rs.getString(8);
                    java.util.Date birthday = rs.getDate(9);
                    String customerEmail = rs.getString(10);
                    String customerPhone = rs.getString(11);
                    int customerTypeId = rs.getInt(12);
                    String customerTypeString = rs.getString(13);
                    int accountId = rs.getInt(14);
                    String username = rs.getString(15);
                    String password = rs.getString(16);
                    String receivingAddress = rs.getString(17);
                    float additionalCharge = rs.getFloat(18);
                    java.util.Date expiredDate = rs.getDate(19);
                    java.util.Date expectedReceivingDate = rs.getDate(20);
                    float cash = rs.getFloat(21);
                    int purchaseMethodId = rs.getInt(22);
                    String purchaseMethod = rs.getString(23);
                    int productId = rs.getInt(24);
                    int productTypeId = rs.getInt(25);
                    String productType = rs.getString(26);
                    float unitPrice = rs.getFloat(27);
                    int discountId = rs.getInt(28);
                    float discountRate = rs.getFloat(29);
                    java.util.Date beginDate = rs.getDate(30);
                    java.util.Date endDate = rs.getDate(31);
                    String description = rs.getString(32);
                    int quantity = rs.getInt(33);
                    String pictureThumb = rs.getString(34);
                    int warehouseId = rs.getInt(35);
                    String warehouseName = rs.getString(36);
                    String warehouseCode = rs.getString(37);
                    String warehouseLocation = rs.getString(38);
                    String warehousePhone = rs.getString(39);
                    int buyingQuantity = rs.getInt(40);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(customerTypeId);
                    customerType.setType(customerTypeString);
                    orderDtos.add(new OrderDto(orderId, new Customer(customerId, customerEmail, customerType, new FullName(firstName, midName, lastName),
                            new Address(customerAddress.split("#")[0], customerAddress.split("#")[1],
                                    customerAddress.split("#")[2], customerAddress.split("#")[3]),
                            new Gender(genderId, genderValue), birthday, customerPhone, new Account(accountId, username, password)),
                            new Address(receivingAddress.split("#")[0], receivingAddress.split("#")[1],
                                    receivingAddress.split("#")[2], receivingAddress.split("#")[3]),
                            additionalCharge, expiredDate, expectedReceivingDate, cash,
                            new PurchaseMethod(purchaseMethodId, purchaseMethod),
                            new Product(productId, new ProductType(productTypeId, productType),
                                    unitPrice, discount, description, quantity, pictureThumb,
                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
                                            new Address(warehouseLocation.split("#")[0],
                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
                                                    warehouseLocation.split("#")[3]), warehousePhone)),
                            buyingQuantity));
                    // </editor-fold>
                    //
                    // <editor-fold desc="normal way">
//                    orderDtos.add(new OrderDto(orderId, new Customer(customerId,
//                            new FullName(firstName, midName, lastName),
//                            new Address(customerAddress.split("#")[0], customerAddress.split("#")[1],
//                                    customerAddress.split("#")[2], customerAddress.split("#")[3]),
//                            new Gender(genderId, genderValue), birthday, customerPhone, customerEmail,
//                            new CustomerType(customerTypeId, customerTypeString),
//                            new Account(accountId, username, password)),
//                            new Address(receivingAddress.split("#")[0], receivingAddress.split("#")[1],
//                                    receivingAddress.split("#")[2], receivingAddress.split("#")[3]),
//                            additionalCharge, expiredDate, expectedReceivingDate, cash,
//                            new PurchaseMethod(purchaseMethodId, purchaseMethod),
//                            new Product(productId, new ProductType(productTypeId, productType),
//                                    unitPrice, discount, description, quantity, pictureThumb,
//                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
//                                            new Address(warehouseLocation.split("#")[0],
//                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
//                                                    warehouseLocation.split("#")[3]), warehousePhone)),
//                            buyingQuantity));
                    // </editor-fold>
                }
                return orderDtos;
            } catch (SQLException ex) {
                Logger.getLogger(OracleOrderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Order getOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<OrderDto> getOneDtoByIdUnderTheList(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_TRANSACTION_ORDER, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                List<OrderDto> orderDtos = new ArrayList<>();
                while (rs.next()) {
                    int orderId = rs.getInt(1);
                    int customerId = rs.getInt(2);
                    String firstName = rs.getString(3);
                    String midName = rs.getString(4);
                    String lastName = rs.getString(5);
                    String customerAddress = rs.getString(6);
                    int genderId = rs.getInt(7);
                    String genderValue = rs.getString(8);
                    java.util.Date birthday = rs.getDate(9);
                    String customerEmail = rs.getString(10);
                    String customerPhone = rs.getString(11);
                    int customerTypeId = rs.getInt(12);
                    String customerTypeString = rs.getString(13);
                    int accountId = rs.getInt(14);
                    String username = rs.getString(15);
                    String password = rs.getString(16);
                    String receivingAddress = rs.getString(17);
                    float additionalCharge = rs.getFloat(18);
                    java.util.Date expiredDate = rs.getDate(19);
                    java.util.Date expectedReceivingDate = rs.getDate(20);
                    float cash = rs.getFloat(21);
                    int purchaseMethodId = rs.getInt(22);
                    String purchaseMethod = rs.getString(23);
                    int productId = rs.getInt(24);
                    int productTypeId = rs.getInt(25);
                    String productType = rs.getString(26);
                    float unitPrice = rs.getFloat(27);
                    int discountId = rs.getInt(28);
                    float discountRate = rs.getFloat(29);
                    java.util.Date beginDate = rs.getDate(30);
                    java.util.Date endDate = rs.getDate(31);
                    String description = rs.getString(32);
                    int quantity = rs.getInt(33);
                    String pictureThumb = rs.getString(34);
                    int warehouseId = rs.getInt(35);
                    String warehouseName = rs.getString(36);
                    String warehouseCode = rs.getString(37);
                    String warehouseLocation = rs.getString(38);
                    String warehousePhone = rs.getString(39);
                    int buyingQuantity = rs.getInt(40);

                    Discount discount = null;

                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }

                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(customerTypeId);
                    customerType.setType(customerTypeString);
                    orderDtos.add(new OrderDto(orderId, new Customer(customerId, customerEmail, customerType, new FullName(firstName, midName, lastName),
                            new Address(customerAddress.split("#")[0], customerAddress.split("#")[1],
                                    customerAddress.split("#")[2], customerAddress.split("#")[3]),
                            new Gender(genderId, genderValue), birthday, customerPhone, new Account(accountId, username, password)),
                            new Address(receivingAddress.split("#")[0], receivingAddress.split("#")[1],
                                    receivingAddress.split("#")[2], receivingAddress.split("#")[3]),
                            additionalCharge, expiredDate, expectedReceivingDate, cash,
                            new PurchaseMethod(purchaseMethodId, purchaseMethod),
                            new Product(productId, new ProductType(productTypeId, productType),
                                    unitPrice, discount, description, quantity, pictureThumb,
                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
                                            new Address(warehouseLocation.split("#")[0],
                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
                                                    warehouseLocation.split("#")[3]), warehousePhone)),
                            buyingQuantity));
                    // </editor-fold>
                    //
                    // <editor-fold desc="normal way">
//                    orderDtos.add(new OrderDto(orderId, new Customer(customerId,
//                            new FullName(firstName, midName, lastName),
//                            new Address(customerAddress.split("#")[0], customerAddress.split("#")[1],
//                                    customerAddress.split("#")[2], customerAddress.split("#")[3]),
//                            new Gender(genderId, genderValue), birthday, customerPhone, customerEmail,
//                            new CustomerType(customerTypeId, customerTypeString),
//                            new Account(accountId, username, password)),
//                            new Address(receivingAddress.split("#")[0], receivingAddress.split("#")[1],
//                                    receivingAddress.split("#")[2], receivingAddress.split("#")[3]),
//                            additionalCharge, expiredDate, expectedReceivingDate, cash,
//                            new PurchaseMethod(purchaseMethodId, purchaseMethod),
//                            new Product(productId, new ProductType(productTypeId, productType),
//                                    unitPrice, discount, description, quantity, pictureThumb,
//                                    new Warehouse(warehouseId, warehouseName, warehouseCode,
//                                            new Address(warehouseLocation.split("#")[0],
//                                                    warehouseLocation.split("#")[1], warehouseLocation.split("#")[2],
//                                                    warehouseLocation.split("#")[3]), warehousePhone, warehouseEmail)),
//                            buyingQuantity));
                    // </editor-fold>
                }
                return orderDtos;
            } catch (SQLException ex) {
                Logger.getLogger(OracleOrderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Order> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertIntoFullTable(Orders orders) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
