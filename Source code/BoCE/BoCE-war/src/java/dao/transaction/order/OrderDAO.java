package dao.transaction.order;

import bo.transaction.Order;
import dto.transaction.order.OrderDto;
import java.util.List;
import dao.common.CommonDAO;
import entities.transaction.order.Orders;

/**
 *
 * @author truongbb
 */
public interface OrderDAO extends CommonDAO<Order> {

    List<OrderDto> getAllDto();

    List<OrderDto> getOneDtoByIdUnderTheList(int id);

    boolean insertIntoFullTable(Orders orders);

}
