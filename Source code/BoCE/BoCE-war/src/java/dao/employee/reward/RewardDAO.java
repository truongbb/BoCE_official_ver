package dao.employee.reward;

import bo.employee.reward.Reward;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface RewardDAO extends CommonDAO<Reward> {

    boolean insertIntoFullTable(Reward reward, boolean isInsertIntoRewardType);
}
