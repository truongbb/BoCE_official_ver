package dao.employee.reward;

import bo.employee.reward.Reward;
import bo.employee.reward.RewardType;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class OracleRewardDAOImpl implements RewardDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Reward> getAll() {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Reward> rewards = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    int rewardTypeId = rs.getInt(3);
                    String rewardType = rs.getString(4);
                    rewards.add(new Reward(id, name, new RewardType(rewardTypeId, rewardType)));
                }
                return rewards;
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Reward getOneById(int id) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String name = rs.getString(2);
                    int rewardTypeId = rs.getInt(3);
                    String rewardType = rs.getString(4);
                    return new Reward(id, name, new RewardType(rewardTypeId, rewardType));
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Reward reward) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, reward.getName());
                ps.setInt(2, reward.getType().getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean insertIntoFullTable(Reward reward, boolean isInsertIntoRewardType) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);

                // insert into reward_type
                if (isInsertIntoRewardType) {
                    String rewardTypeSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD_TYPE, "insert_oracle");
                    ps = connection.prepareStatement(rewardTypeSql);
                    ps.setString(1, reward.getType().getType());
                    if (ps.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }
                }

                // insert into reward
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, reward.getName());
                ps.setInt(2, reward.getType().getId());
                if (ps.executeUpdate() > 0) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    try {
                        connection.setAutoCommit(true);
                    } catch (SQLException ex1) {
                        Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    oracleDAOFactory.closeConnection(null, ps, connection);
                }
            }
        }
        return false;
    }

    @Override
    public boolean update(Reward reward) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, reward.getName());
                ps.setInt(2, reward.getType().getId());
                ps.setInt(3, reward.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Reward reward) {
        Connection connection = oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_REWARD, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, reward.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleRewardDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Reward> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
