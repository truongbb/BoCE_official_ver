package dao.employee.position;

import bo.employee.Position;
import bo.employee.Salary;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class OraclePositionDAOImpl implements PositionDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Position> getAll() {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Position> positions = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    int salary_level = rs.getInt(3);
                    float salary_basic = rs.getFloat(4);
                    float salary_rate = rs.getFloat(5);
                    float allowance_rate = rs.getFloat(6);
                    positions.add(new Position(id, name, new Salary(salary_level, salary_basic, salary_rate, allowance_rate)));
                }
                return positions;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Position getOneById(int id) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String name = rs.getString(2);
                    int salary_level = rs.getInt(3);
                    float salary_basic = rs.getFloat(4);
                    float salary_rate = rs.getFloat(5);
                    float allowance_rate = rs.getFloat(6);
                    return new Position(id, name, new Salary(salary_level, salary_basic, salary_rate, allowance_rate));
                }
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Position position) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, position.getName());
                ps.setInt(2, position.getSalary().getSalaryLevel());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    // truongbb - function to insert position and position's salary
    @Override
    public boolean insertIntoFullTable(Position position, boolean isInsertIntoSalary) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);
                //insert into salary if this salary is not exist in salary table before
                if (isInsertIntoSalary) {
                    String salarySql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "insert_oracle");
                    ps = connection.prepareStatement(salarySql);
                    ps.setFloat(1, position.getSalary().getBasicSalary());
                    ps.setFloat(2, position.getSalary().getSalaryRate());
                    ps.setFloat(3, position.getSalary().getAllowanceRate());
                    if (ps.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }
                }

                // insert into position
                String postionSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "insert_oracle");
                ps = connection.prepareStatement(postionSql);
                ps.setString(1, position.getName());
                ps.setInt(2, position.getSalary().getSalaryLevel());
                if (ps.executeUpdate() > 0) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Position position) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, position.getName());
                ps.setInt(2, position.getSalary().getSalaryLevel());
                ps.setInt(3, position.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Position position) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_POSITION, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, position.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OraclePositionDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Position> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
