package dao.employee.position;

import bo.employee.Position;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface PositionDAO extends CommonDAO<Position> {

    // truongbb
    boolean insertIntoFullTable(Position position, boolean isInsertIntoSalary);

}
