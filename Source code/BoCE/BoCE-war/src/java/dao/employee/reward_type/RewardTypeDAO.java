package dao.employee.reward_type;

import bo.employee.reward.RewardType;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface RewardTypeDAO extends CommonDAO<RewardType> {
    
}
