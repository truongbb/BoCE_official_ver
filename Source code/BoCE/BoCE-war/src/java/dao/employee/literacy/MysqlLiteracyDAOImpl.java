package dao.employee.literacy;

import entities.employee.Literacy;
import dao.common.MysqlDAOFactory;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class MysqlLiteracyDAOImpl implements LiteracyDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<Literacy> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_LITERACY, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Literacy> literacies = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String value = rs.getString(2);
                    literacies.add(new Literacy(BigDecimal.valueOf(id), value));
                }
                return literacies;
            } catch (SQLException ex) {
                Logger.getLogger(OracleLiteracyDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Literacy getOneById(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_LITERACY, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String value = rs.getString(2);
                    return new Literacy(BigDecimal.valueOf(id), value);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleLiteracyDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Literacy literacy) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_LITERACY, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, literacy.getValue());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleLiteracyDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Literacy literacy) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_LITERACY, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, literacy.getValue());
                ps.setInt(2, literacy.getId().intValueExact());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleLiteracyDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Literacy literacy) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_LITERACY, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, literacy.getId().intValueExact());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleLiteracyDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Literacy> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
