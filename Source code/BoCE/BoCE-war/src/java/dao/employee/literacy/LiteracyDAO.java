package dao.employee.literacy;

import entities.employee.Literacy;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface LiteracyDAO extends CommonDAO<Literacy> {

}
