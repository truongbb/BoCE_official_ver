package dao.employee.literacy;

import entities.employee.Literacy;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import utils.EntityMangerUtils;

/**
 *
 * @author truongbb
 */
public class LiteracyJPA extends EntityMangerUtils implements LiteracyDAO {

    @Override
    public List<Literacy> getAll() {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Literacy.findAll");
            em.getTransaction().commit();
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public Literacy getOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Literacy t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Literacy t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Literacy t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Literacy> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        new LiteracyJPA().getAll().forEach(System.out::println);
    }

}
