package dao.employee.displine;

import bo.employee.discipline.Discipline;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface DisciplineDAO extends CommonDAO<Discipline> {

    // truongbb
    boolean insertIntoFullTable(Discipline discipline, boolean isInsertIntoDisciplineType);

}
