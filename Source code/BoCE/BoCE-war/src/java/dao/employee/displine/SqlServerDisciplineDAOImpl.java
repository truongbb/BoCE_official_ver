package dao.employee.displine;

import bo.employee.discipline.Discipline;
import bo.employee.discipline.DisciplineType;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class SqlServerDisciplineDAOImpl implements DisciplineDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Discipline> getAll() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Discipline> displines = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    int discipline_type_id = rs.getInt(3);
                    String discipline_type = rs.getString(4);
                    displines.add(new Discipline(id, name, new DisciplineType(discipline_type_id, discipline_type)));
                }
                return displines;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Discipline getOneById(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String name = rs.getString(2);
                    int discipline_type_id = rs.getInt(3);
                    String discipline_type = rs.getString(4);
                    return new Discipline(id, name, new DisciplineType(discipline_type_id, discipline_type));
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Discipline discipline) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, discipline.getName());
                ps.setInt(2, discipline.getDisciplineType().getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean insertIntoFullTable(Discipline discipline, boolean isInsertIntoDisciplineType) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);

                // insert into discipline_type
                if (isInsertIntoDisciplineType) {
                    String disciplineTypeSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISCIPLINE_TYPE, "insert_mysql");
                    ps = connection.prepareStatement(disciplineTypeSql);
                    ps.setString(1, discipline.getDisciplineType().getType());
                    if (ps.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }
                }
                // insert into discipline
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, discipline.getName());
                ps.setInt(2, discipline.getDisciplineType().getId());
                if (ps.executeUpdate() > 0) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Discipline discipline) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, discipline.getName());
                ps.setInt(2, discipline.getDisciplineType().getId());
                ps.setInt(3, discipline.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Discipline discipline) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DISPLINE, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, discipline.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDisciplineDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Discipline> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
