package dao.employee.discipline_type;

import bo.employee.discipline.DisciplineType;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface DisciplineTypeDAO extends CommonDAO<DisciplineType> {

}
