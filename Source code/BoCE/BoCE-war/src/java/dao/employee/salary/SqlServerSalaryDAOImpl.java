package dao.employee.salary;

import bo.employee.Salary;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class SqlServerSalaryDAOImpl implements SalaryDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Salary> getAll() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Salary> salaries = new ArrayList<>();
                while (rs.next()) {
                    int salary_level = rs.getInt(1);
                    float basic_salary = rs.getFloat(2);
                    float salary_rate = rs.getFloat(3);
                    float allowance_rate = rs.getFloat(4);
                    salaries.add(new Salary(salary_level, basic_salary, salary_rate, allowance_rate));
                }
                return salaries;
            } catch (SQLException ex) {
                Logger.getLogger(OracleSalaryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Salary getOneById(int salaryLevel) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, salaryLevel);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int salary_level = rs.getInt(1);
                    float basic_salary = rs.getFloat(2);
                    float salary_rate = rs.getFloat(3);
                    float allowance_rate = rs.getFloat(4);
                    return new Salary(salary_level, basic_salary, salary_rate, allowance_rate);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleSalaryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Salary salary) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setFloat(1, salary.getBasicSalary());
                ps.setFloat(2, salary.getSalaryRate());
                ps.setFloat(3, salary.getAllowanceRate());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleSalaryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Salary salary) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "update");
                ps = connection.prepareStatement(sql);
                ps.setFloat(1, salary.getBasicSalary());
                ps.setFloat(2, salary.getSalaryRate());
                ps.setFloat(3, salary.getAllowanceRate());
                ps.setInt(4, salary.getSalaryLevel());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleSalaryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Salary salary) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_SALARY, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, salary.getSalaryLevel());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleSalaryDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Salary> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
