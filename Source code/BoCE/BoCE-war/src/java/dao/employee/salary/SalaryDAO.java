package dao.employee.salary;

import bo.employee.Salary;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface SalaryDAO extends CommonDAO<Salary> {

}
