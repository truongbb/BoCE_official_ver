package dao.employee;

import bo.common.Account;
import bo.common.Ethnic;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.employee.Department;
import bo.employee.Employee;
import bo.employee.Literacy;
import bo.employee.Position;
import bo.employee.Salary;
import bo.employee.discipline.DisciplineType;
import bo.employee.reward.RewardType;
import dao.common.SqlServerDAOFactory;
import dto.employee.EmployeeDto;
import dto.employee.discipline_employee.DisciplineEmployeeDto;
import dto.employee.reward_employee.RewardEmployeeDto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class SqlServerEmployeeDAOImpl implements EmployeeDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Employee> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EmployeeDto> getAllDto() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<EmployeeDto> employees = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String first_name = rs.getString(2);
                    String mid_name = rs.getString(3);
                    String last_name = rs.getString(4);
                    String address = rs.getString(5);
                    int gender_id = rs.getInt(6);
                    String gender_value = rs.getString(7);
                    Date brithday = (java.util.Date) rs.getDate(8);
                    String phone = rs.getString(9);
                    int ethnic_id = rs.getInt(10);
                    String ethnic_value = rs.getString(11);
                    Date start_working_date = (java.util.Date) rs.getDate(12);
                    int literacy_id = rs.getInt(13);
                    String literacy_value = rs.getString(14);
                    int position_id = rs.getInt(15);
                    String position_name = rs.getString(16);
                    int salary_level = rs.getInt(17);
                    float basic_salary = rs.getFloat(18);
                    float salary_rate = rs.getFloat(19);
                    float allowance_rate = rs.getFloat(20);
                    int department_id = rs.getInt(21);
                    String department_name = rs.getString(22);
                    String department_phone = rs.getString(23);
                    int account_id = rs.getInt(24);
                    String acount_username = rs.getString(25);
                    String acount_password = rs.getString(26);
                    int disciplineId = rs.getInt(27);
                    String disciplineName = rs.getString(28);
                    int disciplineTypeId = rs.getInt(29);
                    String disciplineTypeName = rs.getString(30);
                    java.util.Date disciplineDate = (java.util.Date) rs.getDate(31);
                    float forfeit = rs.getFloat(32);
                    String disciplineDescription = rs.getString(33);
                    int rewardId = rs.getInt(34);
                    String rewardName = rs.getString(35);
                    int rewardTypeId = rs.getInt(36);
                    String rewardTypeName = rs.getString(37);
                    java.util.Date rewardDate = (java.util.Date) rs.getDate(38);
                    float bonus = rs.getFloat(39);
                    String rewardDescription = rs.getString(40);
                    String[] addressStrings = address.split("#");
                    EmployeeDto employeeDto = new EmployeeDto(id, new FullName(first_name, mid_name, last_name),
                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
                            new Gender(gender_id, gender_value), brithday, phone, new Ethnic(ethnic_id, ethnic_value),
                            start_working_date, new Literacy(literacy_id, literacy_value),
                            new Position(position_id, position_name, new Salary(salary_level, basic_salary, salary_rate, allowance_rate)),
                            new Department(department_id, department_phone, department_name),
                            new Account(account_id, acount_username, acount_password),
                            new DisciplineEmployeeDto(disciplineId, disciplineName, new DisciplineType(disciplineTypeId, disciplineTypeName), disciplineDate, forfeit, disciplineDescription),
                            new RewardEmployeeDto(rewardId, rewardName, new RewardType(rewardTypeId, rewardTypeName), rewardDate, bonus, rewardDescription));
                    employees.add(employeeDto);
                }
                return employees;
            } catch (SQLException ex) {
                Logger.getLogger(OracleEmployeeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Employee getOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EmployeeDto> getOneDtoByIdUnderTheList(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                List<EmployeeDto> employees = new ArrayList<>();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String first_name = rs.getString(2);
                    String mid_name = rs.getString(3);
                    String last_name = rs.getString(4);
                    String address = rs.getString(5);
                    int gender_id = rs.getInt(6);
                    String gender_value = rs.getString(7);
                    Date brithday = (java.util.Date) rs.getDate(8);
                    String phone = rs.getString(9);
                    int ethnic_id = rs.getInt(10);
                    String ethnic_value = rs.getString(11);
                    Date start_working_date = (java.util.Date) rs.getDate(12);
                    int literacy_id = rs.getInt(13);
                    String literacy_value = rs.getString(14);
                    int position_id = rs.getInt(15);
                    String position_name = rs.getString(16);
                    int salary_level = rs.getInt(17);
                    float basic_salary = rs.getFloat(18);
                    float salary_rate = rs.getFloat(19);
                    float allowance_rate = rs.getFloat(20);
                    int department_id = rs.getInt(21);
                    String department_name = rs.getString(22);
                    String department_phone = rs.getString(23);
                    int account_id = rs.getInt(24);
                    String acount_username = rs.getString(25);
                    String acount_password = rs.getString(26);
                    int disciplineId = rs.getInt(27);
                    String disciplineName = rs.getString(28);
                    int disciplineTypeId = rs.getInt(29);
                    String disciplineTypeName = rs.getString(30);
                    java.util.Date disciplineDate = (java.util.Date) rs.getDate(31);
                    float forfeit = rs.getFloat(32);
                    String disciplineDescription = rs.getString(33);
                    int rewardId = rs.getInt(34);
                    String rewardName = rs.getString(35);
                    int rewardTypeId = rs.getInt(36);
                    String rewardTypeName = rs.getString(37);
                    java.util.Date rewardDate = (java.util.Date) rs.getDate(38);
                    float bonus = rs.getFloat(39);
                    String rewardDescription = rs.getString(40);
                    String[] addressStrings = address.split("#");
                    EmployeeDto employeeDto = new EmployeeDto(gottenId, new FullName(first_name, mid_name, last_name),
                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
                            new Gender(gender_id, gender_value), brithday, phone, new Ethnic(ethnic_id, ethnic_value),
                            start_working_date, new Literacy(literacy_id, literacy_value),
                            new Position(position_id, position_name, new Salary(salary_level, basic_salary, salary_rate, allowance_rate)),
                            new Department(department_id, department_phone, department_name),
                            new Account(account_id, acount_username, acount_password),
                            new DisciplineEmployeeDto(disciplineId, disciplineName, new DisciplineType(disciplineTypeId, disciplineTypeName), disciplineDate, forfeit, disciplineDescription),
                            new RewardEmployeeDto(rewardId, rewardName, new RewardType(rewardTypeId, rewardTypeName), rewardDate, bonus, rewardDescription));
                    employees.add(employeeDto);
                }
                return employees;
            } catch (SQLException ex) {
                Logger.getLogger(OracleEmployeeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    /**
     *
     * @param employee
     * @return true when success
     * @description: just insert into only employee table
     */
    @Override
    public boolean insert(Employee employee) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, employee.getFullName().getFirstName());
                ps.setString(2, employee.getFullName().getMidName());
                ps.setString(3, employee.getFullName().getLastName());
                ps.setString(4, employee.getAddress().toString());
                ps.setInt(5, employee.getGender().getId());
                ps.setDate(6, java.sql.Date.valueOf(employee.getBirthDay().getYear() + "-" + employee.getBirthDay().getMonth() + "-" + employee.getBirthDay().getDate()));
                ps.setString(7, employee.getPhone());
                ps.setInt(8, employee.getEthnic().getId());
                ps.setDate(9, java.sql.Date.valueOf(employee.getStartWorkingDate().getYear() + "-" + employee.getStartWorkingDate().getMonth() + "-" + employee.getStartWorkingDate().getDate()));
                ps.setInt(10, employee.getLiteracy().getId());
                ps.setInt(11, employee.getPosition().getId());
                ps.setInt(12, employee.getDepartment().getId());
                ps.setInt(13, employee.getAccount().getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleEmployeeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Employee t) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, t.getFullName().getFirstName());
                ps.setString(2, t.getFullName().getMidName());
                ps.setString(3, t.getFullName().getLastName());
                ps.setString(4, t.getAddress().toString());
                ps.setInt(5, t.getGender().getId());
                ps.setDate(6, java.sql.Date.valueOf(t.getBirthDay().getYear() + "-" + t.getBirthDay().getMonth() + "-" + t.getBirthDay().getDate()));
                ps.setString(7, t.getPhone());
                ps.setInt(8, t.getEthnic().getId());
                ps.setDate(9, java.sql.Date.valueOf(t.getStartWorkingDate().getYear() + "-" + t.getStartWorkingDate().getMonth() + "-" + t.getStartWorkingDate().getDate()));
                ps.setInt(10, t.getLiteracy().getId());
                ps.setInt(11, t.getPosition().getId());
                ps.setInt(12, t.getDepartment().getId());
                ps.setInt(13, t.getAccount().getId());
                ps.setInt(14, t.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleEmployeeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Employee t) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, t.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleEmployeeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Employee> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
