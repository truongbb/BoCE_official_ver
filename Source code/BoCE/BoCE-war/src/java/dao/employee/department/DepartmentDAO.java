package dao.employee.department;

import bo.employee.Department;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface DepartmentDAO extends CommonDAO<Department> {

}
