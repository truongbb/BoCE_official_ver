package dao.employee.department;

import bo.employee.Department;
import dao.common.SqlServerDAOFactory;
import dao.common.gender.OracleGenderDAOImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class SqlServerDepartmentDAOImpl implements DepartmentDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Department> getAll() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DEPARTMENT, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Department> departments = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String phone = rs.getString(3);
                    departments.add(new Department(id, name, phone));
                }
                return departments;
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Department getOneById(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DEPARTMENT, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String name = rs.getString(2);
                    String phone = rs.getString(3);
                    return new Department(id, name, phone);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Department department) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DEPARTMENT, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, department.getName());
                ps.setString(2, department.getPhone());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDepartmentDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Department department) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DEPARTMENT, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, department.getName());
                ps.setString(2, department.getPhone());
                ps.setInt(3, department.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDepartmentDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Department department) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EMPLOYEE_DEPARTMENT, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, department.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleDepartmentDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Department> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
