package dao.employee;

import bo.employee.Employee;
import dto.employee.EmployeeDto;
import java.util.List;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface EmployeeDAO extends CommonDAO<Employee> {

    List<EmployeeDto> getAllDto();

    List<EmployeeDto> getOneDtoByIdUnderTheList(int id);

}
