package dao.exportation;

import bo.common.Tetrad;
import bo.common.Unit;
import bo.common.person.Address;
import bo.employee.Employee;
import bo.exportation.ExportationReport;
import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class MysqlExportationReportDAOImpl implements ExportationReportDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<ExportationReport> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EXPORTATION, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<ExportationReport> exportationReports = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    int warehouse_manager_id = rs.getInt(2);
                    java.util.Date createdDate = rs.getDate(3);
                    String note = rs.getString(4);
                    int productId = rs.getInt(5);
                    int productTypeId = rs.getInt(6);
                    String productType = rs.getString(7);
                    float productUnitPrice = rs.getFloat(8);
                    int discountId = rs.getInt(9);
                    float discountRate = rs.getFloat(10);
                    Date beginDate = rs.getDate(11);
                    Date endDate = rs.getDate(12);
                    String description = rs.getString(13);
                    int quantity = rs.getInt(14);
                    String pictureThumb = rs.getString(15);
                    int warehouseId = rs.getInt(16);
                    String warehouseName = rs.getString(17);
                    String warehouseCode = rs.getString(18);
                    String warehouseLocation = rs.getString(19);
                    String warehousePhone = rs.getString(20);
                    int unitId = rs.getInt(21);
                    String unitValue = rs.getString(22);
                    int exportedQuantity = rs.getInt(23);
                    float exportedUnitPrice = rs.getFloat(24);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }
                    Employee employee = new Employee();
                    employee.setId(warehouse_manager_id);
                    Product product = new Product(productId, new ProductType(productTypeId, productType),
                            productUnitPrice, discount, description, quantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone));
                    exportationReports.add(new ExportationReport(id, employee, Arrays.asList(new Tetrad<>(product, new Unit(unitId, unitValue), exportedQuantity, exportedUnitPrice)).stream().collect(Collectors.toList()), createdDate, note));
                }
                return exportationReports;
            } catch (SQLException ex) {
                Logger.getLogger(OracleExportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public ExportationReport getOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ExportationReport> getOneByIdUnderTheList(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EXPORTATION, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                List<ExportationReport> exportationReports = new ArrayList<>();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    int warehouse_manager_id = rs.getInt(2);
                    java.util.Date createdDate = rs.getDate(3);
                    String note = rs.getString(4);
                    int productId = rs.getInt(5);
                    int productTypeId = rs.getInt(6);
                    String productType = rs.getString(7);
                    float productUnitPrice = rs.getFloat(8);
                    int discountId = rs.getInt(9);
                    float discountRate = rs.getFloat(10);
                    Date beginDate = rs.getDate(11);
                    Date endDate = rs.getDate(12);
                    String description = rs.getString(13);
                    int quantity = rs.getInt(14);
                    String pictureThumb = rs.getString(15);
                    int warehouseId = rs.getInt(16);
                    String warehouseName = rs.getString(17);
                    String warehouseCode = rs.getString(18);
                    String warehouseLocation = rs.getString(19);
                    String warehousePhone = rs.getString(20);
                    int unitId = rs.getInt(21);
                    String unitValue = rs.getString(22);
                    int exportedQuantity = rs.getInt(23);
                    float exportedUnitPrice = rs.getFloat(24);

                    Discount discount = null;
                    if (beginDate != null) {
                        discount = new Discount(discountId, discountRate, beginDate, endDate);
                    }
                    Employee employee = new Employee();
                    employee.setId(warehouse_manager_id);
                    Product product = new Product(productId, new ProductType(productTypeId, productType),
                            productUnitPrice, discount, description, quantity, pictureThumb,
                            new Warehouse(warehouseId, warehouseName, warehouseCode,
                                    new Address(warehouseLocation.split("#")[0], warehouseLocation.split("#")[1],
                                            warehouseLocation.split("#")[2], warehouseLocation.split("#")[3]),
                                    warehousePhone));
                    exportationReports.add(new ExportationReport(gottenId, employee, Arrays.asList(new Tetrad<>(product, new Unit(unitId, unitValue), exportedQuantity, exportedUnitPrice)).stream().collect(Collectors.toList()), createdDate, note));
                }
                return exportationReports;
            } catch (SQLException ex) {
                Logger.getLogger(OracleExportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertIntoFullTable(ExportationReport exportationReport) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);

                // insert into exportation_report
                String exportSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EXPORTATION, "insert_mysql");
                ps = connection.prepareStatement(exportSql);
                ps.setInt(1, exportationReport.getEmployee().getId());
                String dateString = exportationReport.getCreatedDate().getYear() + "-" + exportationReport.getCreatedDate().getMonth() + "-" + exportationReport.getCreatedDate().getDate();
                ps.setDate(2, java.sql.Date.valueOf(dateString));
                ps.setString(3, exportationReport.getNote());
                if (ps.executeUpdate() <= 0) {
                    connection.rollback();
                    return false;
                }

                // because this's EXPORT -> we have not to insert into product
                // product_type, warehouse, unit are static --> we have not to insert into its table
                // insert into exportation_report_product
                for (Tetrad<Product, Unit, Integer, Float> exportedProduct : exportationReport.getExportedProducts()) {
                    // insert into discount
                    String discountSQL = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_PRODUCT_DISCOUNT, "insert_mysql");
                    PreparedStatement ps1 = connection.prepareStatement(discountSQL);
                    ps1.setFloat(1, exportedProduct.getT().getDiscount().getRate());
                    ps1.setDate(2, java.sql.Date.valueOf(exportedProduct.getT().getDiscount().getBeginDate().getYear() + "-" + exportedProduct.getT().getDiscount().getBeginDate().getMonth() + "-" + exportedProduct.getT().getDiscount().getBeginDate().getDay()));
                    ps1.setDate(3, java.sql.Date.valueOf(exportedProduct.getT().getDiscount().getEndDate().getYear() + "-" + exportedProduct.getT().getDiscount().getEndDate().getMonth() + "-" + exportedProduct.getT().getDiscount().getEndDate().getDay()));
                    if (ps1.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }

                    String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_EXPORTATION_PRODUCT, "insert_mysql");
                    ps = connection.prepareStatement(sql);
                    ps.setInt(1, exportationReport.getId());
                    ps.setInt(2, exportedProduct.getT().getId());
                    ps.setInt(3, exportedProduct.getE().getId());
                    ps.setInt(4, exportedProduct.getS());
                    ps.setFloat(5, exportedProduct.getU());
                    if (ps.executeUpdate() <= 0) {
                        connection.rollback();
                        return false;
                    }
                }
                connection.commit();
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(OracleExportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleExportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OracleExportationReportDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ExportationReport> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
