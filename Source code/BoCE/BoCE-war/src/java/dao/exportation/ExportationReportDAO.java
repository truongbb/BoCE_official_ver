package dao.exportation;

import bo.exportation.ExportationReport;
import java.util.List;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface ExportationReportDAO extends CommonDAO<ExportationReport> {

    // truongbb
    boolean insertIntoFullTable(ExportationReport exportationReport);

    // truongbb
    List<ExportationReport> getOneByIdUnderTheList(int id);
}
