package dao.common;

import dao.employee.EmployeeDAO;
import dao.employee.MysqlEmployeeDAOImpl;
import dao.employee.department.DepartmentDAO;
import dao.employee.department.MysqlDepartmentDAOImpl;
import dao.employee.discipline_type.DisciplineTypeDAO;
import dao.employee.discipline_type.MysqlDisciplineTypeDAOImpl;
import dao.employee.literacy.LiteracyDAO;
import dao.employee.literacy.MysqlLiteracyDAOImpl;
import dao.common.account.AccountDAO;
import dao.common.account.MysqlAccountDAOImpl;
import dao.common.color.ColorDAO;
import dao.common.color.MysqlColorDAOImpl;
import dao.common.unit.MysqlUnitDAOImpl;
import dao.common.unit.UnitDAO;
import dao.customer.CustomerDAO;
import dao.customer.MysqlCustomerDAOImpl;
import dao.customer.customer_type.CustomerTypeDAO;
import dao.customer.customer_type.MysqlCustomerTypeDAOImpl;
import dao.employee.position.MysqlPositionDAOImpl;
import dao.employee.position.PositionDAO;
import dao.employee.salary.MysqlSalaryDAOImpl;
import dao.employee.salary.SalaryDAO;
import dao.exportation.ExportationReportDAO;
import dao.exportation.MysqlExportationReportDAOImpl;
import dao.importation.ImportationReportDAO;
import dao.importation.MysqlImportationReportDAOImpl;
import dao.common.gender.GenderDAO;
import dao.common.gender.MysqlGenderDAOImpl;
import dao.product.MysqlProductDAOImpl;
import dao.product.ProductDAO;
import dao.product.discount.DiscountDAO;
import dao.product.discount.MysqlDiscountDAOImpl;
import dao.product.productType.MysqlProductTypeDAOImpl;
import dao.product.productType.ProductTypeDAO;
import dao.product.warehouse.MysqlWarehouseDAOImpl;
import dao.product.warehouse.WarehouseDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import dao.employee.displine.DisciplineDAO;
import dao.employee.displine.MysqlDisciplineDAOImpl;
import dao.employee.reward.MysqlRewardDAOImpl;
import dao.employee.reward.RewardDAO;
import dao.employee.reward_type.MysqlRewardTypeDAOImpl;
import dao.employee.reward_type.RewardTypeDAO;
import dao.product.book.BookDAO;
import dao.product.book.MysqlBookDAOImpl;
import dao.product.book.author.AuthorDAO;
import dao.product.book.author.MysqlAuthorDAOImpl;
import dao.product.book.category.BookCategoryDAO;
import dao.product.book.category.MysqlBookCategoryDAOImpl;
import dao.product.book.publisher.MysqlPublisherDAOImpl;
import dao.product.book.publisher.PublisherDAO;
import dao.product.electronics.ElectronicsDAO;
import dao.product.clothes.ClothesDAO;
import dao.product.clothes.MysqlClothesDAOImpl;
import dao.product.clothes.category.ClothesCategoryDAO;
import dao.product.clothes.category.MysqlClothesCategoryDAOImpl;
import dao.product.clothes.style.MysqlStyleDAOImpl;
import dao.product.clothes.style.StyleDAO;
import dao.product.electronics.MysqlElectronicsDAOImpl;
import dao.product.electronics.category.ElectronicCategoryDAO;
import dao.product.electronics.category.MysqlElectronicCategoryDAOImpl;
import dao.transaction.bill.BillDAO;
import dao.transaction.bill.MySqlBillDAOImpl;
import dao.transaction.order.MySqlOrderDAOImpl;
import dao.transaction.order.OrderDAO;
import dao.transaction.purchase_method.MysqlPurchaseMethodDAOImpl;
import dao.transaction.purchase_method.PurchaseMethodDAO;

/**
 *
 * @author truongbb
 */
public class MysqlDAOFactory extends DAOFactory implements DatabaseConnection {

    @Override
    public Connection openConnection(String username, String password) {
        try {
            Class.forName(DatabaseConnection.MYSQL_DRIVER);
            return DriverManager.getConnection(DatabaseConnection.MYSQL_URL, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(MysqlDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void closeConnection(ResultSet rs, PreparedStatement ps, Connection cn) {
        DatabaseConnection.super.closeConnection(rs, ps, cn);
    }

    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new MysqlEmployeeDAOImpl();
    }

    @Override
    public ProductDAO getProductDAO() {
        return new MysqlProductDAOImpl();
    }

    @Override
    public ProductTypeDAO getProductTypeDAO() {
        return new MysqlProductTypeDAOImpl();
    }

    @Override
    public DiscountDAO getDiscountDAO() {
        return new MysqlDiscountDAOImpl();
    }

    @Override
    public WarehouseDAO getWarehouseDAO() {
        return new MysqlWarehouseDAOImpl();
    }

    @Override
    public GenderDAO getGenderDAO() {
        return new MysqlGenderDAOImpl();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new MysqlDepartmentDAOImpl();
    }

    @Override
    public DisciplineTypeDAO getDisciplineTypeDAO() {
        return new MysqlDisciplineTypeDAOImpl();
    }

    @Override
    public LiteracyDAO getLiteracyDAO() {
        return new MysqlLiteracyDAOImpl();
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new MysqlAccountDAOImpl();
    }

    @Override
    public UnitDAO getUnitDAO() {
        return new MysqlUnitDAOImpl();
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new MysqlCustomerDAOImpl();
    }

    @Override
    public CustomerTypeDAO getCustomerTypeDAO() {
        return new MysqlCustomerTypeDAOImpl();
    }

    @Override
    public PositionDAO getPositionDAO() {
        return new MysqlPositionDAOImpl();
    }

    @Override
    public SalaryDAO getSalaryDAO() {
        return new MysqlSalaryDAOImpl();
    }

    @Override
    public ExportationReportDAO getExportationReportDAO() {
        return new MysqlExportationReportDAOImpl();
    }

    @Override
    public ImportationReportDAO getImportationReportDAO() {
        return new MysqlImportationReportDAOImpl();
    }

    @Override
    public ColorDAO getColorDAO() {
        return new MysqlColorDAOImpl();
    }

    @Override
    public DisciplineDAO getDisciplineDAO() {
        return new MysqlDisciplineDAOImpl();
    }

    @Override
    public RewardDAO getRewardDAO() {
        return new MysqlRewardDAOImpl();
    }

    @Override
    public RewardTypeDAO getRewardTypeDAO() {
        return new MysqlRewardTypeDAOImpl();
    }

    @Override
    public BookDAO getBookDAO() {
        return new MysqlBookDAOImpl();
    }

    @Override
    public AuthorDAO getAuthorDAO() {
        return new MysqlAuthorDAOImpl();
    }

    @Override
    public BookCategoryDAO getBookCategoryDAO() {
        return new MysqlBookCategoryDAOImpl();
    }

    @Override
    public PublisherDAO getPublisherDAO() {
        return new MysqlPublisherDAOImpl();
    }

    @Override
    public ClothesDAO getClothesDAO() {
        return new MysqlClothesDAOImpl();
    }

    @Override
    public ClothesCategoryDAO getClothesCategoryDAO() {
        return new MysqlClothesCategoryDAOImpl();
    }

    @Override
    public ElectronicsDAO getElectronicsDAO() {
        return new MysqlElectronicsDAOImpl();
    }

    @Override
    public ElectronicCategoryDAO getElectronicsCategoryDAO() {
        return new MysqlElectronicCategoryDAOImpl();
    }

    @Override
    public BillDAO getBillDAO() {
        return new MySqlBillDAOImpl();
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new MySqlOrderDAOImpl();
    }

    @Override
    public PurchaseMethodDAO getPurchaseMethodDAO() {
        return new MysqlPurchaseMethodDAOImpl();
    }

    @Override
    public StyleDAO getStyleDAO() {
        return new MysqlStyleDAOImpl();
    }
}
