package dao.common;

import dao.employee.EmployeeDAO;
import dao.employee.department.DepartmentDAO;
import dao.employee.discipline_type.DisciplineTypeDAO;
import dao.employee.literacy.LiteracyDAO;
import dao.common.account.AccountDAO;
import dao.common.color.ColorDAO;
import dao.common.unit.UnitDAO;
import dao.customer.CustomerDAO;
import dao.customer.customer_type.CustomerTypeDAO;
import dao.employee.position.PositionDAO;
import dao.employee.salary.SalaryDAO;
import dao.exportation.ExportationReportDAO;
import dao.importation.ImportationReportDAO;
import dao.common.gender.GenderDAO;
import dao.product.ProductDAO;
import dao.product.discount.DiscountDAO;
import dao.product.productType.ProductTypeDAO;
import dao.product.warehouse.WarehouseDAO;
import dao.employee.displine.DisciplineDAO;
import dao.employee.reward.RewardDAO;
import dao.employee.reward_type.RewardTypeDAO;
import dao.product.book.BookDAO;
import dao.product.book.author.AuthorDAO;
import dao.product.book.category.BookCategoryDAO;
import dao.product.book.publisher.PublisherDAO;
import dao.product.electronics.ElectronicsDAO;
import dao.product.clothes.ClothesDAO;
import dao.product.clothes.category.ClothesCategoryDAO;
import dao.product.clothes.style.StyleDAO;
import dao.product.electronics.category.ElectronicCategoryDAO;
import dao.transaction.bill.BillDAO;
import dao.transaction.order.OrderDAO;
import dao.transaction.purchase_method.PurchaseMethodDAO;

/**
 *
 * @author truongbb
 */
public abstract class DAOFactory {

    public abstract AccountDAO getAccountDAO();

    public abstract ColorDAO getColorDAO();

    public abstract UnitDAO getUnitDAO();

    public abstract CustomerDAO getCustomerDAO();

    public abstract CustomerTypeDAO getCustomerTypeDAO();

    public abstract EmployeeDAO getEmployeeDAO();

    public abstract DepartmentDAO getDepartmentDAO();

    public abstract DisciplineTypeDAO getDisciplineTypeDAO();

    public abstract DisciplineDAO getDisciplineDAO();

    public abstract LiteracyDAO getLiteracyDAO();

    public abstract PositionDAO getPositionDAO();

    public abstract RewardDAO getRewardDAO();

    public abstract RewardTypeDAO getRewardTypeDAO();

    public abstract SalaryDAO getSalaryDAO();

    public abstract ExportationReportDAO getExportationReportDAO();

    public abstract ImportationReportDAO getImportationReportDAO();

    public abstract GenderDAO getGenderDAO();

    public abstract ProductDAO getProductDAO();

    public abstract BookDAO getBookDAO();

    public abstract AuthorDAO getAuthorDAO();

    public abstract BookCategoryDAO getBookCategoryDAO();

    public abstract PublisherDAO getPublisherDAO();

    public abstract ClothesDAO getClothesDAO();

    public abstract ClothesCategoryDAO getClothesCategoryDAO();

    public abstract DiscountDAO getDiscountDAO();

    public abstract ElectronicsDAO getElectronicsDAO();

    public abstract ElectronicCategoryDAO getElectronicsCategoryDAO();

    public abstract ProductTypeDAO getProductTypeDAO();

    public abstract WarehouseDAO getWarehouseDAO();

    public abstract BillDAO getBillDAO();

    public abstract OrderDAO getOrderDAO();

    public abstract PurchaseMethodDAO getPurchaseMethodDAO();

    public abstract StyleDAO getStyleDAO();

}
