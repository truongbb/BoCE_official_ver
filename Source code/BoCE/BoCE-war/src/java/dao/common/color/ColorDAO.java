package dao.common.color;

import bo.common.Color;
import dao.common.CommonDAO;

/**
 *
 * @author truongbb
 */
public interface ColorDAO extends CommonDAO<Color> {
    
}
