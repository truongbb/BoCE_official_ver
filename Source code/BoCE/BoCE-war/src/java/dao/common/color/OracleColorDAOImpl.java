package dao.common.color;

import bo.common.Color;
import dao.common.OracleDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author truongbb
 */
public class OracleColorDAOImpl implements ColorDAO {

    private OracleDAOFactory oracleDAOFactory = new OracleDAOFactory();

    @Override
    public List<Color> getAll() {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_COLOR, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Color> colors = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String color = rs.getString(2);
                    String code = rs.getString(3);
                    colors.add(new Color(id, color, code));
                }
                return colors;
            } catch (SQLException ex) {
                Logger.getLogger(OracleColorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Color getOneById(int id) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_COLOR, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String color = rs.getString(2);
                    String code = rs.getString(3);
                    return new Color(gottenId, color, code);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleColorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Color color) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_COLOR, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, color.getColor());
                ps.setString(2, color.getCode());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleColorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Color color) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_COLOR, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, color.getColor());
                ps.setString(2, color.getCode());
                ps.setInt(3, color.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleColorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Color color) {
        Connection connection = this.oracleDAOFactory.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_COLOR, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, color.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleColorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.oracleDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Color> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
