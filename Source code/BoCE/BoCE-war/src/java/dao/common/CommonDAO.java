package dao.common;

import java.util.List;

/**
 *
 * @author truongbb
 * @param <T>
 *
 */
public interface CommonDAO<T> {

    List<T> getAll();

    T getOneById(int id);

    boolean insert(T t);

    boolean update(T t);

    boolean delete(T t);

    List<T> doSearch(Object object);

}
