package dao.common;

/**
 *
 * @author truongbb
 */
public class DAOFactoryProducer {

    // DB
    public static final String ORACLE = "ORACLE";
    public static final String SQLSERVER = "SQLSERVER";
    public static final String MYSQL = "MYSQL";

    public static DAOFactory getDAOFactory(String DAOFactory) {

        switch (DAOFactory) {
            case ORACLE:
                return new OracleDAOFactory();
            case SQLSERVER:
                return new SqlServerDAOFactory();
            case MYSQL:
                return new MysqlDAOFactory();
        }
        return null;
    }
}
