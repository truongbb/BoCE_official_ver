package dao.common.unit;

import bo.common.Unit;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface UnitDAO extends CommonDAO<Unit>{
    
}
