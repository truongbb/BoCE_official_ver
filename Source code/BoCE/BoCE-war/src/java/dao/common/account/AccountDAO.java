package dao.common.account;

import bo.common.Account;
import utils.DatabaseConnection;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 * @editor truongbb on 3/3/2018: apply bridge pattern
 * 
 */
public abstract class AccountDAO implements CommonDAO<Account> {

    protected DatabaseConnection dbConnection;

    public AccountDAO() {
    }

    public AccountDAO(DatabaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public DatabaseConnection getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(DatabaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }
    
    public abstract Account login(String username, String password);

}
