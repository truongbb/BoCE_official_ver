package dao.common.account;

import bo.common.Account;
import bo.common.Pair;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 * @editor truongbb on 3/3/2018: apply bridge pattern
 */
public class OracleAccountDAOImpl extends AccountDAO {

    //<editor-fold desc="bridge pattern">
    public OracleAccountDAOImpl() {
    }

    public OracleAccountDAOImpl(DatabaseConnection dbConnection) {
        super(dbConnection);
    }
    //</editor-fold>

    @Override
    public Account login(String username, String password) {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "login");
                ps = connection.prepareStatement(sql);
                ps.setString(1, username);
                ps.setString(2, password);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String gottenUsername = rs.getString(2);
                    String gottenPassword = rs.getString(3);
                    return new Account(gottenId, gottenUsername, gottenPassword);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public List<Account> getAll() {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Account> accounts = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String username = rs.getString(2);
                    String password = rs.getString(3);
                    accounts.add(new Account(id, username, password));
                }
                return accounts;
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Account getOneById(int id) {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String username = rs.getString(2);
                    String password = rs.getString(3);
                    return new Account(gottenId, username, password);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Account account) {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "insert_oracle");
                ps = connection.prepareStatement(sql);
                ps.setString(1, account.getUsername());
                ps.setString(2, account.getPassword());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Account account) {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, account.getUsername());
                ps.setString(2, account.getPassword());
                ps.setInt(3, account.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Account account) {
        Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, account.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.dbConnection.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Account> doSearch(Object objectAccount) {
        if (objectAccount instanceof Account) {
            Account account = (Account) objectAccount;
            List<Pair<String, Object>> criterias = new ArrayList<>();

            Connection connection = this.dbConnection.openConnection(DatabaseConnection.ORACLE_USERNAME, DatabaseConnection.ORACLE_PASSWORD);
            PreparedStatement ps = null;
            ResultSet rs = null;
            if (connection != null) {
                try {
                    String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "do_search");
                    if (account.getUsername() != null && !account.getUsername().trim().equals("")) {
                        sql += "\n and username like '%' || ? || '%'";
                        criterias.add(new Pair<>("username", account.getUsername()));
                    }
                    if (account.getPassword() != null && !account.getPassword().trim().equals("")) {
                        sql += "\n and password like '%' || ? || '%'";
                        criterias.add(new Pair<>("password", account.getPassword()));
                    }
                    sql += "\n order by id";
                    ps = connection.prepareStatement(sql);
                    if (account.getUsername() != null && !account.getUsername().trim().equals("")) {
                        ps.setString(criterias.indexOf(criterias.stream().filter(t -> t.getT().equals("username")).findFirst().get()) + 1, account.getUsername());
                    }
                    if (account.getPassword() != null && !account.getPassword().trim().equals("")) {
                        ps.setString(criterias.indexOf(criterias.stream().filter(t -> t.getT().equals("password")).findFirst().get()) + 1, account.getPassword());
                    }
                    rs = ps.executeQuery();
                    List<Account> accounts = new ArrayList<>();
                    while (rs.next()) {
                        int id = rs.getInt(1);
                        String username = rs.getString(2);
                        String password = rs.getString(3);
                        accounts.add(new Account(id, username, password));
                    }
                    return accounts;
                } catch (SQLException ex) {
                    Logger.getLogger(OracleAccountDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    this.dbConnection.closeConnection(null, ps, connection);
                }
            }
        }
        return null;
    }

}
