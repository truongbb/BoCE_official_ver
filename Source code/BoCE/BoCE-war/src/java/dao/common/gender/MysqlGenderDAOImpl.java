package dao.common.gender;

import bo.common.person.Gender;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class MysqlGenderDAOImpl implements GenderDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<Gender> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_GENDER, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Gender> genders = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String value = rs.getString(2);
                    genders.add(new Gender(id, value));
                }
                return genders;
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Gender getOneById(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_GENDER, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String value = rs.getString(2);
                    return new Gender(id, value);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Gender gender) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_GENDER, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, gender.getGender());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Gender gender) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_GENDER, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, gender.getGender());
                ps.setInt(2, gender.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Gender gender) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_GENDER, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, gender.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleGenderDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Gender> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
