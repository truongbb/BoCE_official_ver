package dao.common.gender;

import bo.common.person.Gender;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface GenderDAO extends CommonDAO<Gender> {

}
