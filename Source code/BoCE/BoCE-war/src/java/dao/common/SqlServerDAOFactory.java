package dao.common;

import dao.employee.EmployeeDAO;
import dao.employee.SqlServerEmployeeDAOImpl;
import dao.employee.department.DepartmentDAO;
import dao.employee.department.SqlServerDepartmentDAOImpl;
import dao.employee.discipline_type.DisciplineTypeDAO;
import dao.employee.discipline_type.SqlServerDisciplineTypeDAOImpl;
import dao.employee.displine.SqlServerDisciplineDAOImpl;
import dao.employee.literacy.LiteracyDAO;
import dao.employee.literacy.SqlServerLiteracyDAOImpl;
import dao.common.account.AccountDAO;
import dao.common.account.SqlServerAccountDAOImpl;
import dao.common.color.ColorDAO;
import dao.common.color.SqlServerColorDAOImpl;
import dao.common.unit.SqlServerUnitDAOImpl;
import dao.common.unit.UnitDAO;
import dao.customer.CustomerDAO;
import dao.customer.SqlServerCustomerDAOImpl;
import dao.customer.customer_type.CustomerTypeDAO;
import dao.customer.customer_type.SqlServerCustomerTypeDAOImpl;
import dao.employee.position.PositionDAO;
import dao.employee.position.SqlServerPositionDAOImpl;
import dao.employee.salary.SqlServerSalaryDAOImpl;
import dao.employee.salary.SalaryDAO;
import dao.exportation.ExportationReportDAO;
import dao.exportation.SqlServerExportationReportDAOImpl;
import dao.importation.ImportationReportDAO;
import dao.importation.SqlServerImportationReportDAOImpl;
import dao.common.gender.GenderDAO;
import dao.common.gender.SqlServerGenderDAOImpl;
import dao.product.ProductDAO;
import dao.product.SqlServerProductDAOImpl;
import dao.product.clothes.SqlServerClothesDAOImpl;
import dao.product.discount.DiscountDAO;
import dao.product.electronics.SqlServerElectronicsDAOImpl;
import dao.product.productType.ProductTypeDAO;
import dao.product.productType.SqlServerProductTypeDAOImpl;
import dao.product.warehouse.SqlServerWarehouseDAOImpl;
import dao.product.warehouse.WarehouseDAO;
import dao.product.book.SqlServerBookDAOImpl;
import dao.product.book.author.SqlServerAuthorDAOImpl;
import dao.product.book.publisher.SqlServerPublisherDAOImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import dao.employee.displine.DisciplineDAO;
import dao.employee.reward.RewardDAO;
import dao.employee.reward.SqlServerRewardDAOImpl;
import dao.employee.reward_type.RewardTypeDAO;
import dao.employee.reward_type.SqlServerRewardTypeDAOImpl;
import dao.product.book.BookDAO;
import dao.product.book.author.AuthorDAO;
import dao.product.book.category.BookCategoryDAO;
import dao.product.book.category.SqlServerBookCategoryDAOImpl;
import dao.product.book.publisher.PublisherDAO;
import dao.product.electronics.ElectronicsDAO;
import dao.product.clothes.ClothesDAO;
import dao.product.clothes.category.ClothesCategoryDAO;
import dao.product.clothes.category.SqlServerClothesCategoryDAOImpl;
import dao.product.clothes.style.SqlServerStyleDAOImpl;
import dao.product.clothes.style.StyleDAO;
import dao.product.discount.SqlServerDiscountDAOImpl;
import dao.product.electronics.category.ElectronicCategoryDAO;
import dao.product.electronics.category.SqlServerElectronicCategoryDAOImpl;
import dao.transaction.bill.BillDAO;
import dao.transaction.bill.SqlServerBillDAOImpl;
import dao.transaction.order.OrderDAO;
import dao.transaction.order.SqlServerOrderDAOImpl;
import dao.transaction.purchase_method.PurchaseMethodDAO;
import dao.transaction.purchase_method.SqlServerPurchaseMethodDAOImpl;

/**
 *
 * @author truongbb
 */
public class SqlServerDAOFactory extends DAOFactory implements DatabaseConnection {

    @Override
    public Connection openConnection(String username, String password) {
        try {
            Class.forName(DatabaseConnection.SQL_SERVER_DRIVER);
            return DriverManager.getConnection(DatabaseConnection.SQL_SERVER_URL, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(SqlServerDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void closeConnection(ResultSet rs, PreparedStatement ps, Connection cn) {
        DatabaseConnection.super.closeConnection(rs, ps, cn);
    }

    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new SqlServerEmployeeDAOImpl();
    }

    @Override
    public ProductDAO getProductDAO() {
        return new SqlServerProductDAOImpl();
    }

    @Override
    public ProductTypeDAO getProductTypeDAO() {
        return new SqlServerProductTypeDAOImpl();
    }

    @Override
    public DiscountDAO getDiscountDAO() {
        return new SqlServerDiscountDAOImpl();
    }

    @Override
    public WarehouseDAO getWarehouseDAO() {
        return new SqlServerWarehouseDAOImpl();
    }

    @Override
    public GenderDAO getGenderDAO() {
        return new SqlServerGenderDAOImpl();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new SqlServerDepartmentDAOImpl();
    }

    @Override
    public DisciplineTypeDAO getDisciplineTypeDAO() {
        return new SqlServerDisciplineTypeDAOImpl();
    }

    @Override
    public LiteracyDAO getLiteracyDAO() {
        return new SqlServerLiteracyDAOImpl();
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new SqlServerAccountDAOImpl();
    }

    @Override
    public UnitDAO getUnitDAO() {
        return new SqlServerUnitDAOImpl();
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new SqlServerCustomerDAOImpl();
    }

    @Override
    public CustomerTypeDAO getCustomerTypeDAO() {
        return new SqlServerCustomerTypeDAOImpl();
    }

    @Override
    public PositionDAO getPositionDAO() {
        return new SqlServerPositionDAOImpl();
    }

    @Override
    public SalaryDAO getSalaryDAO() {
        return new SqlServerSalaryDAOImpl();
    }

    @Override
    public ExportationReportDAO getExportationReportDAO() {
        return new SqlServerExportationReportDAOImpl();
    }

    @Override
    public ImportationReportDAO getImportationReportDAO() {
        return new SqlServerImportationReportDAOImpl();
    }

    @Override
    public ColorDAO getColorDAO() {
        return new SqlServerColorDAOImpl();
    }

    @Override
    public DisciplineDAO getDisciplineDAO() {
        return new SqlServerDisciplineDAOImpl();
    }

    @Override
    public RewardDAO getRewardDAO() {
        return new SqlServerRewardDAOImpl();
    }

    @Override
    public RewardTypeDAO getRewardTypeDAO() {
        return new SqlServerRewardTypeDAOImpl();
    }

    @Override
    public BookDAO getBookDAO() {
        return new SqlServerBookDAOImpl();
    }

    @Override
    public AuthorDAO getAuthorDAO() {
        return new SqlServerAuthorDAOImpl();
    }

    @Override
    public BookCategoryDAO getBookCategoryDAO() {
        return new SqlServerBookCategoryDAOImpl();
    }

    @Override
    public PublisherDAO getPublisherDAO() {
        return new SqlServerPublisherDAOImpl();
    }

    @Override
    public ClothesDAO getClothesDAO() {
        return new SqlServerClothesDAOImpl();
    }

    @Override
    public ClothesCategoryDAO getClothesCategoryDAO() {
        return new SqlServerClothesCategoryDAOImpl();
    }

    @Override
    public ElectronicsDAO getElectronicsDAO() {
        return new SqlServerElectronicsDAOImpl();
    }

    @Override
    public ElectronicCategoryDAO getElectronicsCategoryDAO() {
        return new SqlServerElectronicCategoryDAOImpl();
    }

    @Override
    public BillDAO getBillDAO() {
        return new SqlServerBillDAOImpl();
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new SqlServerOrderDAOImpl();
    }

    @Override
    public PurchaseMethodDAO getPurchaseMethodDAO() {
        return new SqlServerPurchaseMethodDAOImpl();
    }

    @Override
    public StyleDAO getStyleDAO() {
        return new SqlServerStyleDAOImpl();
    }

}
