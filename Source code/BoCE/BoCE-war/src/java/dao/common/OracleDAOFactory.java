package dao.common;

import dao.employee.EmployeeDAO;
import dao.employee.OracleEmployeeDAOImpl;
import dao.employee.department.DepartmentDAO;
import dao.employee.department.OracleDepartmentDAOImpl;
import dao.employee.discipline_type.DisciplineTypeDAO;
import dao.employee.discipline_type.OracleDisciplineTypeDAOImpl;
import dao.employee.displine.OracleDisciplineDAOImpl;
import dao.employee.literacy.LiteracyDAO;
import dao.employee.literacy.OracleLiteracyDAOImpl;
import dao.common.account.AccountDAO;
import dao.common.account.OracleAccountDAOImpl;
import dao.common.color.ColorDAO;
import dao.common.color.OracleColorDAOImpl;
import dao.common.unit.OracleUnitDAOImpl;
import dao.common.unit.UnitDAO;
import dao.customer.CustomerDAO;
import dao.customer.OracleCustomerDAOImpl;
import dao.customer.customer_type.CustomerTypeDAO;
import dao.customer.customer_type.OracleCustomerTypeDAOImpl;
import dao.employee.position.OraclePositionDAOImpl;
import dao.employee.position.PositionDAO;
import dao.employee.salary.OracleSalaryDAOImpl;
import dao.employee.salary.SalaryDAO;
import dao.exportation.ExportationReportDAO;
import dao.exportation.OracleExportationReportDAOImpl;
import dao.importation.ImportationReportDAO;
import dao.common.gender.GenderDAO;
import dao.common.gender.OracleGenderDAOImpl;
import dao.product.OracleProductDAOImpl;
import dao.product.ProductDAO;
import dao.product.clothes.OracleClothesDAOImpl;
import dao.product.discount.DiscountDAO;
import dao.product.discount.OracleDiscountDAOImpl;
import dao.product.electronics.OracleElectronicsDAOImpl;
import dao.product.productType.OracleProductTypeDAOImpl;
import dao.product.productType.ProductTypeDAO;
import dao.product.warehouse.OracleWarehouseDAOImpl;
import dao.product.warehouse.WarehouseDAO;
import dao.product.book.OracleBookDAOImpl;
import dao.product.book.author.OracleAuthorDAOImpl;
import dao.product.book.publisher.OraclePublisherDAOImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import dao.employee.displine.DisciplineDAO;
import dao.employee.reward.OracleRewardDAOImpl;
import dao.employee.reward.RewardDAO;
import dao.employee.reward_type.OracleRewardTypeDAOImpl;
import dao.employee.reward_type.RewardTypeDAO;
import dao.importation.OracleImportationReportDAOImpl;
import dao.product.book.BookDAO;
import dao.product.book.author.AuthorDAO;
import dao.product.book.category.BookCategoryDAO;
import dao.product.book.category.OracleBookCategoryDAOImpl;
import dao.product.book.publisher.PublisherDAO;
import dao.product.electronics.ElectronicsDAO;
import dao.product.clothes.ClothesDAO;
import dao.product.clothes.category.ClothesCategoryDAO;
import dao.product.clothes.category.OracleClothesCategoryDAOImpl;
import dao.product.clothes.style.OracleStyleDAOImpl;
import dao.product.clothes.style.StyleDAO;
import dao.product.electronics.category.ElectronicCategoryDAO;
import dao.product.electronics.category.OracleElectronicCategoryDAOImpl;
import dao.transaction.bill.BillDAO;
import dao.transaction.bill.OracleBillDAOImpl;
import dao.transaction.order.OracleOrderDAOImpl;
import dao.transaction.order.OrderDAO;
import dao.transaction.purchase_method.OraclePurchaseMethodDAOImpl;
import dao.transaction.purchase_method.PurchaseMethodDAO;

/**
 *
 * @author truongbb
 */
public class OracleDAOFactory extends DAOFactory implements DatabaseConnection {

    @Override
    public Connection openConnection(String username, String password) {
        try {
            Class.forName(DatabaseConnection.ORACLE_DRIVER);
            return DriverManager.getConnection(DatabaseConnection.ORACLE_URL, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(OracleDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void closeConnection(ResultSet rs, PreparedStatement ps, Connection cn) {
        DatabaseConnection.super.closeConnection(rs, ps, cn);
    }

    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new OracleEmployeeDAOImpl();
    }

    @Override
    public ProductDAO getProductDAO() {
        return new OracleProductDAOImpl();
    }

    @Override
    public ProductTypeDAO getProductTypeDAO() {
        return new OracleProductTypeDAOImpl();
    }

    @Override
    public DiscountDAO getDiscountDAO() {
        return new OracleDiscountDAOImpl();
    }

    @Override
    public WarehouseDAO getWarehouseDAO() {
        return new OracleWarehouseDAOImpl();
    }

    @Override
    public GenderDAO getGenderDAO() {
        return new OracleGenderDAOImpl();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new OracleDepartmentDAOImpl();
    }

    @Override
    public DisciplineTypeDAO getDisciplineTypeDAO() {
        return new OracleDisciplineTypeDAOImpl();
    }

    @Override
    public LiteracyDAO getLiteracyDAO() {
        return new OracleLiteracyDAOImpl();
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new OracleAccountDAOImpl();
    }

    @Override
    public UnitDAO getUnitDAO() {
        return new OracleUnitDAOImpl();
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new OracleCustomerDAOImpl();
    }

    @Override
    public CustomerTypeDAO getCustomerTypeDAO() {
        return new OracleCustomerTypeDAOImpl();
    }

    @Override
    public PositionDAO getPositionDAO() {
        return new OraclePositionDAOImpl();
    }

    @Override
    public SalaryDAO getSalaryDAO() {
        return new OracleSalaryDAOImpl();
    }

    @Override
    public ExportationReportDAO getExportationReportDAO() {
        return new OracleExportationReportDAOImpl();
    }

    @Override
    public ImportationReportDAO getImportationReportDAO() {
        return new OracleImportationReportDAOImpl();
    }

    @Override
    public ColorDAO getColorDAO() {
        return new OracleColorDAOImpl();
    }

    @Override
    public DisciplineDAO getDisciplineDAO() {
        return new OracleDisciplineDAOImpl();
    }

    @Override
    public RewardDAO getRewardDAO() {
        return new OracleRewardDAOImpl();
    }

    @Override
    public RewardTypeDAO getRewardTypeDAO() {
        return new OracleRewardTypeDAOImpl();
    }

    @Override
    public BookDAO getBookDAO() {
        return new OracleBookDAOImpl();
    }

    @Override
    public AuthorDAO getAuthorDAO() {
        return new OracleAuthorDAOImpl();
    }

    @Override
    public BookCategoryDAO getBookCategoryDAO() {
        return new OracleBookCategoryDAOImpl();
    }

    @Override
    public PublisherDAO getPublisherDAO() {
        return new OraclePublisherDAOImpl();
    }

    @Override
    public ClothesDAO getClothesDAO() {
        return new OracleClothesDAOImpl();
    }

    @Override
    public ClothesCategoryDAO getClothesCategoryDAO() {
        return new OracleClothesCategoryDAOImpl();
    }

    @Override
    public ElectronicsDAO getElectronicsDAO() {
        return new OracleElectronicsDAOImpl();
    }

    @Override
    public ElectronicCategoryDAO getElectronicsCategoryDAO() {
        return new OracleElectronicCategoryDAOImpl();
    }

    @Override
    public BillDAO getBillDAO() {
        return new OracleBillDAOImpl();
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new OracleOrderDAOImpl();
    }

    @Override
    public PurchaseMethodDAO getPurchaseMethodDAO() {
        return new OraclePurchaseMethodDAOImpl();
    }

    @Override
    public StyleDAO getStyleDAO() {
        return new OracleStyleDAOImpl();
    }
}
