package dao.customer;

import bo.common.Account;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.customer.Customer;
import bo.customer.CustomerType;
import dao.common.SqlServerDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class SqlServerCustomerDAOImpl implements CustomerDAO {

    private SqlServerDAOFactory sqlServerDAOFactory = new SqlServerDAOFactory();

    @Override
    public List<Customer> getAll() {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<Customer> customers = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String first_name = rs.getString(2);
                    String mid_name = rs.getString(3);
                    String last_name = rs.getString(4);
                    String address = rs.getString(5);
                    int gender_id = rs.getInt(6);
                    String gender_value = rs.getString(7);
                    Date birthday = rs.getDate(8);
                    String phone = rs.getString(9);
                    String email = rs.getString(10);
                    int customer_type_id = rs.getInt(11);
                    String customer_type = rs.getString(12);
                    int account_id = rs.getInt(13);
                    String username = rs.getString(14);
                    String password = rs.getString(15);
                    String[] addressStrings = address.split("#");
                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(customer_type_id);
                    customerType.setType(customer_type);
                    customers.add(new Customer(id, email, customerType,
                            new FullName(first_name, mid_name, last_name),
                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
                            new Gender(gender_id, gender_value), birthday, phone, new Account(account_id, username, password)));
                    // </editor-fold>
//
                    // <editor-fold desc="normal way">
//                    customers.add(new Customer(id, new FullName(first_name, mid_name, last_name),
//                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
//                            new Gender(gender_id, gender_value), birthday, phone, email,
//                            new CustomerType(customer_type_id, customer_type),
//                            new Account(account_id, username, password)));
                    // </editor-fold>
                }
                return customers;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public Customer getOneById(int id) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    int gottenId = rs.getInt(1);
                    String first_name = rs.getString(2);
                    String mid_name = rs.getString(3);
                    String last_name = rs.getString(4);
                    String address = rs.getString(5);
                    int gender_id = rs.getInt(6);
                    String gender_value = rs.getString(7);
                    Date birthday = rs.getDate(8);
                    String phone = rs.getString(9);
                    String email = rs.getString(10);
                    int customer_type_id = rs.getInt(11);
                    String customer_type = rs.getString(12);
                    int account_id = rs.getInt(13);
                    String username = rs.getString(14);
                    String password = rs.getString(15);
                    String[] addressStrings = address.split("#");
                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(customer_type_id);
                    customerType.setType(customer_type);
                    return new Customer(id, email, customerType,
                            new FullName(first_name, mid_name, last_name),
                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
                            new Gender(gender_id, gender_value), birthday, phone, new Account(account_id, username, password));
                    // </editor-fold>

                    // <editor-fold desc="normal way">
//                    return new Customer(gottenId, new FullName(first_name, mid_name, last_name),
//                            new Address(addressStrings[0], addressStrings[1], addressStrings[2], addressStrings[3]),
//                            new Gender(gender_id, gender_value), birthday, phone, email,
//                            new CustomerType(customer_type_id, customer_type),
//                            new Account(account_id, username, password));
                    // </editor-fold>
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertIntoFullTable(Customer customer, boolean isInsertType) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null && customer != null) {
            try {
                connection.setAutoCommit(false); // shutdown auto commit

                // insert into account
                String sqlAccount = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "insert_mysql");
                connection.prepareStatement(sqlAccount);
                ps.setString(1, customer.getAccount().getUsername());
                ps.setString(2, customer.getAccount().getPassword());
                int affectedRowsAccount = ps.executeUpdate();
                if (affectedRowsAccount <= 0) {
                    connection.rollback();
                    return false;
                }

                if (isInsertType) {// if customer type is not exist in customer_type table --> we need insert
                    // insert into customer type
                    String sqlType = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "insert_mysql");
                    connection.prepareStatement(sqlType);
                    ps.setString(1, customer.getCustomerType().getType());
                    int affectedRowsCustomerType = ps.executeUpdate();
                    if (affectedRowsCustomerType <= 0) {
                        connection.rollback();
                        return false;
                    }
                }

                // insert into customer
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, customer.getId());
                ps.setString(2, customer.getFullName().getFirstName());
                ps.setString(3, customer.getFullName().getMidName());
                ps.setString(4, customer.getFullName().getLastName());
                ps.setString(5, customer.getAddress().toString());
                ps.setInt(6, customer.getGender().getId());
                ps.setDate(7, java.sql.Date.valueOf(customer.getBirthDay().getYear() + "-" + customer.getBirthDay().getMonth() + "-" + customer.getBirthDay().getDate()));
                ps.setString(8, customer.getEmail());
                ps.setString(9, customer.getPhone());
                ps.setInt(10, customer.getCustomerType().getId());
                ps.setInt(11, customer.getAccount().getId());
                if (ps.executeUpdate() > 0) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } catch (SQLException ex) {
                try {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                    connection.rollback();// roll back if there's any exception occurs
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(Customer customer) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, customer.getFullName().getFirstName());
                ps.setString(2, customer.getFullName().getMidName());
                ps.setString(3, customer.getFullName().getLastName());
                ps.setString(4, customer.getAddress().toString());
                ps.setInt(5, customer.getGender().getId());
                ps.setDate(6, java.sql.Date.valueOf(customer.getBirthDay().getYear() + "-" + customer.getBirthDay().getMonth() + "-" + customer.getBirthDay().getDate()));
                ps.setString(7, customer.getEmail());
                ps.setString(8, customer.getPhone());
                ps.setInt(9, customer.getCustomerType().getId());
                ps.setInt(10, customer.getAccount().getId());
                ps.setInt(11, customer.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(Customer customer) {
        Connection connection = this.sqlServerDAOFactory.openConnection(DatabaseConnection.SQL_SERVER_USERNAME, DatabaseConnection.SQL_SERVER_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                connection.setAutoCommit(false);

                // delete customer
                String customerSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER, "delete");
                ps = connection.prepareStatement(customerSql);
                ps.setInt(1, customer.getId());
                if (ps.executeUpdate() <= 0) {
                    connection.rollback();
                    return false;
                }

                // delete account
                String accountSql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_COMMON_ACCOUNT, "delete");
                ps = connection.prepareStatement(accountSql);
                ps.setInt(1, customer.getAccount().getId());
                if (ps.executeUpdate() <= 0) {
                    connection.rollback();
                    return false;
                } else {
                    connection.commit();
                    return true;
                }
            } catch (SQLException ex) {
                try {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } finally {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(OracleCustomerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.sqlServerDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<Customer> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
