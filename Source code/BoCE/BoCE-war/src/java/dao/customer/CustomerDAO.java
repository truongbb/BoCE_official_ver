package dao.customer;

import bo.customer.Customer;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface CustomerDAO extends CommonDAO<Customer> {

    // truongbb
    boolean insertIntoFullTable(Customer customer, boolean isInsertType);
}
