package dao.customer.customer_type;

import bo.customer.CustomerType;
import dao.common.CommonDAO;

/**
 *
 * @author thucpn
 */
public interface CustomerTypeDAO extends CommonDAO<CustomerType> {

}
