package dao.customer.customer_type;

import bo.customer.CustomerType;
import dao.common.MysqlDAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatabaseConnection;
import utils.SQLBuider;

/**
 *
 * @author thucpn
 */
public class MysqlCustomerTypeDAOImpl implements CustomerTypeDAO {

    private MysqlDAOFactory mysqlDAOFactory = new MysqlDAOFactory();

    @Override
    public List<CustomerType> getAll() {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "get_all");
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                List<CustomerType> customerTypes = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String type = rs.getString(2);
                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(id);
                    customerType.setType(type);
                    customerTypes.add(customerType);
                    // </editor-fold>
                    //
                    // <editor-fold desc="normal way">
                    //  customerTypes.add(new CustomerType(id, type));
                    // </editor-fold>
                }
                return customerTypes;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public CustomerType getOneById(int id) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "get_one_by_id");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    id = rs.getInt(1);
                    String type = rs.getString(2);
                    // <editor-fold desc="singleton pattern">
                    CustomerType customerType = CustomerType.getInstance();
                    customerType.setId(id);
                    customerType.setType(type);
                    return customerType;
                    // </editor-fold>
                    //
                    // <editor-fold desc="normal way">
                    // return new CustomerType(id, type);
                    // </editor-fold>
                }
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(rs, ps, connection);
            }
        }
        return null;
    }

    @Override
    public boolean insert(CustomerType customerType) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "insert_mysql");
                ps = connection.prepareStatement(sql);
                ps.setString(1, customerType.getType());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean update(CustomerType customerType) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "update");
                ps = connection.prepareStatement(sql);
                ps.setString(1, customerType.getType());
                ps.setInt(2, customerType.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public boolean delete(CustomerType customerType) {
        Connection connection = this.mysqlDAOFactory.openConnection(DatabaseConnection.MYSQL_USERNAME, DatabaseConnection.MYSQL_PASSWORD);
        PreparedStatement ps = null;
        if (connection != null) {
            try {
                String sql = SQLBuider.getSqlQueryById(SQLBuider.SQL_MODULE_CUSTOMER_CUSTOMER_TYPE, "delete");
                ps = connection.prepareStatement(sql);
                ps.setInt(1, customerType.getId());
                return ps.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(OracleCustomerTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.mysqlDAOFactory.closeConnection(null, ps, connection);
            }
        }
        return false;
    }

    @Override
    public List<CustomerType> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
