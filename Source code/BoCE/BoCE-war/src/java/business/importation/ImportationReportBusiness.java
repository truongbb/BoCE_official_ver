package business.importation;

import bo.importation.ImportationReport;
import business.common.CommonBusiness;

/**
 *
 * @author truongbb
 */
public interface ImportationReportBusiness extends CommonBusiness<ImportationReport> {

}
