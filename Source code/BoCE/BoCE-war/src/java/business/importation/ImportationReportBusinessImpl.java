package business.importation;

import bo.importation.ImportationReport;
import business.employee.EmployeeBusiness;
import dao.common.CommonDAO;
import dao.importation.ImportationReportDAO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class ImportationReportBusinessImpl implements ImportationReportBusiness {

    private CommonDAO importationReportDAO;

    private EmployeeBusiness employeeBusiness;

    public ImportationReportBusinessImpl() {
    }

    public ImportationReportBusinessImpl(CommonDAO importationReportDAO) {
        this.importationReportDAO = importationReportDAO;
    }

    public ImportationReportBusinessImpl(CommonDAO importationReportDAO, EmployeeBusiness employeeBusiness) {
        this.importationReportDAO = importationReportDAO;
        this.employeeBusiness = employeeBusiness;
    }

    public CommonDAO getImportationReportDAO() {
        return importationReportDAO;
    }

    public void setImportationReportDAO(CommonDAO importationReportDAO) {
        this.importationReportDAO = importationReportDAO;
    }

    public EmployeeBusiness getEmployeeBusiness() {
        return employeeBusiness;
    }

    public void setEmployeeBusiness(EmployeeBusiness employeeBusiness) {
        this.employeeBusiness = employeeBusiness;
    }

    @Override
    public List<ImportationReport> getAll() {
        List<ImportationReport> gottenImportationReports = importationReportDAO.getAll();
        return convertList(gottenImportationReports);
    }

    @Override
    public ImportationReport getOneById(int id) {
        if (importationReportDAO instanceof ImportationReportDAO) {
            ImportationReportDAO dao = (ImportationReportDAO) importationReportDAO;
            List<ImportationReport> importationReports = convertList(dao.getOneByIdUnderTheList(id));
            if (importationReports != null && !importationReports.isEmpty()) {
                return importationReports.get(0);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ImportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ImportationReport> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ImportationReport> convertList(List<ImportationReport> gottenImportationReports) {
        if (gottenImportationReports != null && !gottenImportationReports.isEmpty()) {
            List<ImportationReport> importationReports = new ArrayList<>();
            int id = -1;
            for (ImportationReport gottenImportationReport : gottenImportationReports) {
                gottenImportationReport.setEmployee(employeeBusiness.getOneById(gottenImportationReport.getEmployee().getId()));
                if (id == gottenImportationReport.getId()) {
                    gottenImportationReports.get(gottenImportationReports.indexOf(gottenImportationReport) - 1).
                            getImportedProducts().add(gottenImportationReport.getImportedProducts().get(0));
                } else {
                    importationReports.add(gottenImportationReport);
                    id = gottenImportationReport.getId();
                }
            }
            return importationReports;
        }
        return null;
    }

}
