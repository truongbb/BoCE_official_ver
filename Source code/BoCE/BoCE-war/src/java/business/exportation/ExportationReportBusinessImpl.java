package business.exportation;

import bo.exportation.ExportationReport;
import business.employee.EmployeeBusiness;
import dao.common.CommonDAO;
import dao.exportation.ExportationReportDAO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class ExportationReportBusinessImpl implements ExportationReportBusiness {

    private CommonDAO exportationReportDAO;

    private EmployeeBusiness employeeBusiness;

    public ExportationReportBusinessImpl() {
    }

    public ExportationReportBusinessImpl(CommonDAO exportationReportDAO) {
        this.exportationReportDAO = exportationReportDAO;
    }

    public ExportationReportBusinessImpl(CommonDAO exportationReportDAO, EmployeeBusiness employeeBusiness) {
        this.exportationReportDAO = exportationReportDAO;
        this.employeeBusiness = employeeBusiness;
    }

    public CommonDAO getExportationReportDAO() {
        return exportationReportDAO;
    }

    public void setExportationReportDAO(ExportationReportDAO exportationReportDAO) {
        this.exportationReportDAO = exportationReportDAO;
    }

    public EmployeeBusiness getEmployeeBusiness() {
        return employeeBusiness;
    }

    public void setEmployeeBusiness(EmployeeBusiness employeeBusiness) {
        this.employeeBusiness = employeeBusiness;
    }

    @Override
    public List<ExportationReport> getAll() {
        List<ExportationReport> gottenExportationReports = exportationReportDAO.getAll();
        return convertList(gottenExportationReports);
    }

    @Override
    public ExportationReport getOneById(int id) {
        if (exportationReportDAO instanceof ExportationReportDAO) {
            ExportationReportDAO dao = (ExportationReportDAO) exportationReportDAO;
            List<ExportationReport> gottenExportationReports = dao.getOneByIdUnderTheList(id);
            List<ExportationReport> exportationReports = convertList(gottenExportationReports);
            if (exportationReports != null && !exportationReports.isEmpty()) {
                return exportationReports.get(0);
            }
        }
        return null;
    }

    @Override
    public boolean insert(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ExportationReport t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ExportationReport> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ExportationReport> convertList(List<ExportationReport> gottenExportationReports) {
        if (gottenExportationReports != null && !gottenExportationReports.isEmpty()) {
            List<ExportationReport> exportationReports = new ArrayList<>();
            int id = -1;
            for (ExportationReport gottenExportationReport : gottenExportationReports) {
                gottenExportationReport.setEmployee(employeeBusiness.getOneById(gottenExportationReport.getEmployee().getId()));
                if (id == gottenExportationReport.getId()) {
                    gottenExportationReports.get(gottenExportationReports.indexOf(gottenExportationReport) - 1).
                            getExportedProducts().add(gottenExportationReport.getExportedProducts().get(0));
                } else {
                    exportationReports.add(gottenExportationReport);
                    id = gottenExportationReport.getId();
                }
            }
            return exportationReports;
        }
        return null;
    }

}
