package business.exportation;

import bo.exportation.ExportationReport;
import business.common.CommonBusiness;

/**
 *
 * @author truongbb
 */
public interface ExportationReportBusiness extends CommonBusiness<ExportationReport> {

}
