package business.transaction.order;

import bo.transaction.Order;
import business.common.CommonBusiness;

/**
 *
 * @author truongbb
 */
public interface OrderBusiness extends CommonBusiness<Order> {

}
