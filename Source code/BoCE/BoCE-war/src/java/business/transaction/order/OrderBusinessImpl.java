package business.transaction.order;

import bo.common.Pair;
import bo.product.Product;
import bo.transaction.Cart;
import bo.transaction.Order;
import dao.common.CommonDAO;
import dao.transaction.order.OrderDAO;
import dto.transaction.order.OrderDto;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author truongbb
 */
public class OrderBusinessImpl implements OrderBusiness {

    private CommonDAO orderDAO;

    public OrderBusinessImpl() {
    }

    public OrderBusinessImpl(CommonDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public CommonDAO getOrderDAO() {
        return orderDAO;
    }

    public void setOrderDAO(CommonDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Override
    public List<Order> getAll() {
        if (orderDAO instanceof OrderDAO) {
            OrderDAO dao = (OrderDAO) orderDAO;
            return sortingById(convertDtoList(dao.getAllDto()));
        }
        return null;
    }

    @Override
    public Order getOneById(int id) {
        if (orderDAO instanceof OrderDAO) {
            OrderDAO dao = (OrderDAO) orderDAO;
            List<Order> orders = convertDtoList(dao.getOneDtoByIdUnderTheList(id));
            if (orders != null && !orders.isEmpty()) {
                return orders.get(0);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Order t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Order> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Order> convertDtoList(List<OrderDto> orderDtos) {
        List<Order> orders = null;
        if (orderDtos != null && !orderDtos.isEmpty()) {
            orders = new ArrayList<>();
            int orderId = -1;
            Order order = null;
            List<Pair<Product, Integer>> productList = null;
            for (OrderDto orderDto : orderDtos) {
                if (orderId == orderDto.getId()) {
                    productList.add(new Pair<>(orderDto.getProduct(), orderDto.getBuyingQuantity()));
                } else {
                    if (order != null) {
                        order.setCart(new Cart(null, productList));
                        orders.add(order);
                    }
                    orderId = orderDto.getId();
                    order = new Order();

                    order.setId(orderDto.getId());
                    order.setCustomer(orderDto.getCustomer());
                    order.setExpectedReceivingDate(orderDto.getExpectedReceivingDate());
                    order.setCash(orderDto.getCash());
                    order.setAdditionalCharge(orderDto.getAdditionalCharge());
                    order.setExpiredDate(orderDto.getExpiredDate());
                    order.setPurchaseMethod(orderDto.getPurchaseMethod());
                    order.setReceivingAddress(orderDto.getReceivingAddress());

                    productList = new ArrayList<>();
                    productList.add(new Pair<>(orderDto.getProduct(), orderDto.getBuyingQuantity()));
                }
                if (orderDtos.indexOf(orderDto) + 1 == orderDtos.size()) {
                    order.setCart(new Cart(null, productList));
                    orders.add(order);
                }
            }
        }
        return orders;
    }

    public List<Order> sortingById(List<Order> orders) {
        return orders.stream().sorted(Comparator.comparing(Order::getId)).collect(Collectors.toList());
    }

}
