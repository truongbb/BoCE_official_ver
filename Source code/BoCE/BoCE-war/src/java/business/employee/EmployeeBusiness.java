package business.employee;

import bo.employee.Employee;
import business.common.CommonBusiness;

/**
 *
 * @author truongbb
 */
public interface EmployeeBusiness extends CommonBusiness<Employee> {

}
