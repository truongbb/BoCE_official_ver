package business.employee;

import bo.common.Tetrad;
import bo.employee.Employee;
import bo.employee.discipline.Discipline;
import bo.employee.reward.Reward;
import dao.common.CommonDAO;
import dao.employee.EmployeeDAO;
import dto.employee.EmployeeDto;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author truongbb
 */
public class EmployeeBusinessImpl implements EmployeeBusiness {

    private CommonDAO employeeDAO;

    public EmployeeBusinessImpl() {
    }

    public EmployeeBusinessImpl(CommonDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    public CommonDAO getEmployeeDAO() {
        return employeeDAO;
    }

    public void setEmployeeDAO(CommonDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    @Override
    public List<Employee> getAll() {
        if (employeeDAO instanceof EmployeeDAO) {
            EmployeeDAO dao = (EmployeeDAO) employeeDAO;
            List<EmployeeDto> employeeDtoList = dao.getAllDto();
            return sortingById(convertDtoListToBoList(employeeDtoList));
        }
        return null;
    }

    @Override
    public Employee getOneById(int id) {
        if (employeeDAO instanceof EmployeeDAO) {
            EmployeeDAO dao = (EmployeeDAO) employeeDAO;
            List<EmployeeDto> employeeDtos = dao.getOneDtoByIdUnderTheList(id);
            List<Employee> employees = convertDtoListToBoList(employeeDtos);
            if (employees != null && !employees.isEmpty()) {
                return employees.get(0);
            }
        }
        return null;
    }

    @Override
    public boolean insert(Employee t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Employee t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Employee t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Employee> doSearch(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Employee> convertDtoListToBoList(List<EmployeeDto> employeeDtoList) {
        if (employeeDtoList != null && !employeeDtoList.isEmpty()) {
            List<Employee> employees = new ArrayList<>();
            List<Tetrad<Discipline, String, Date, Float>> disciplineList = null;
            List<Tetrad<Reward, String, Date, Float>> rewardList = null;

            int tempId = -1;
            Employee tempEmployee = null;

            for (EmployeeDto employeeDto : employeeDtoList) {
                if (tempId == employeeDto.getId()) {

                    if (employeeDto.getDisciplineEmployeeDto().getName() != null) {
                        disciplineList.add(new Tetrad<>(
                                new Discipline(employeeDto.getDisciplineEmployeeDto().getId(),
                                        employeeDto.getDisciplineEmployeeDto().getName(),
                                        employeeDto.getDisciplineEmployeeDto().getDisciplineType()),
                                employeeDto.getDisciplineEmployeeDto().getDescription(), employeeDto.getDisciplineEmployeeDto().getDisciplineDate(), employeeDto.getDisciplineEmployeeDto().getForfeit()));
                    }
                    if (employeeDto.getRewardEmployeeDto().getName() != null) {
                        rewardList.add(new Tetrad<>(
                                new Reward(employeeDto.getRewardEmployeeDto().getId(),
                                        employeeDto.getRewardEmployeeDto().getName(),
                                        employeeDto.getRewardEmployeeDto().getRewardType()),
                                employeeDto.getRewardEmployeeDto().getDescription(), employeeDto.getRewardEmployeeDto().getRewardDate(), employeeDto.getRewardEmployeeDto().getBonus()));
                    }
                } else {

                    if (tempEmployee != null) {
                        tempEmployee.setRewardList(rewardList);
                        tempEmployee.setDisciplineList(disciplineList);
                        employees.add(tempEmployee);
                    }

                    tempEmployee = new Employee();
                    tempEmployee.setId(employeeDto.getId());
                    tempEmployee.setFullName(employeeDto.getFullName());
                    tempEmployee.setAccount(employeeDto.getAccount());
                    tempEmployee.setAddress(employeeDto.getAddress());
                    tempEmployee.setBirthDay(employeeDto.getBirthday());
                    tempEmployee.setStartWorkingDate(employeeDto.getStartWorkingDate());
                    tempEmployee.setDepartment(employeeDto.getDepartment());
                    tempEmployee.setEthnic(employeeDto.getEthnic());
                    tempEmployee.setGender(employeeDto.getGender());
                    tempEmployee.setLiteracy(employeeDto.getLiteracy());
                    tempEmployee.setPhone(employeeDto.getPhone());
                    tempEmployee.setPosition(employeeDto.getPosition());

                    tempId = employeeDto.getId();
                    disciplineList = new ArrayList<>();
                    rewardList = new ArrayList<>();

                    if (employeeDto.getDisciplineEmployeeDto().getName() != null) {
                        disciplineList.add(new Tetrad<>(
                                new Discipline(employeeDto.getDisciplineEmployeeDto().getId(),
                                        employeeDto.getDisciplineEmployeeDto().getName(),
                                        employeeDto.getDisciplineEmployeeDto().getDisciplineType()),
                                employeeDto.getDisciplineEmployeeDto().getDescription(), employeeDto.getDisciplineEmployeeDto().getDisciplineDate(), employeeDto.getDisciplineEmployeeDto().getForfeit()));
                    }
                    if (employeeDto.getRewardEmployeeDto().getName() != null) {
                        rewardList.add(new Tetrad<>(
                                new Reward(employeeDto.getRewardEmployeeDto().getId(),
                                        employeeDto.getRewardEmployeeDto().getName(),
                                        employeeDto.getRewardEmployeeDto().getRewardType()),
                                employeeDto.getRewardEmployeeDto().getDescription(), employeeDto.getRewardEmployeeDto().getRewardDate(), employeeDto.getRewardEmployeeDto().getBonus()));
                    }
                }
                if (employeeDtoList.indexOf(employeeDto) + 1 == employeeDtoList.size()) {
                    tempEmployee.setDisciplineList(disciplineList);
                    tempEmployee.setRewardList(rewardList);
                    employees.add(tempEmployee);
                }
            }
            return employees;
        }
        return null;
    }

    public List<Employee> sortingById(List<Employee> employees) {
        return employees.stream().sorted(Comparator.comparing(Employee::getId)).collect(Collectors.toList());
    }
}
