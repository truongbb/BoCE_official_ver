package business.account;

import bo.common.Account;

/**
 *
 * @author truongbb
 */
public interface AccountBusiness {

    Account login(String username, String password);
}
