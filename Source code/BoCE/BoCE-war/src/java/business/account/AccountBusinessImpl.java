package business.account;

import bo.common.Account;
import dao.common.account.AccountDAO;

/**
 *
 * @author truongbb
 */
public class AccountBusinessImpl implements AccountBusiness {

    private AccountDAO accountDAO;

    public AccountBusinessImpl() {
    }

    public AccountBusinessImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    public AccountDAO getAccountDAO() {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public Account login(String username, String password) {
        return this.accountDAO.login(username, password);
    }
}
