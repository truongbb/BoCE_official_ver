package business.common;

import bo.employee.Employee;
import bo.exportation.ExportationReport;
import bo.importation.ImportationReport;
import bo.transaction.Order;
import business.employee.EmployeeBusinessImpl;
import business.exportation.ExportationReportBusinessImpl;
import business.importation.ImportationReportBusinessImpl;
import business.transaction.order.OrderBusinessImpl;
import dao.common.CommonDAO;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class BusinessMaker {

    private CommonBusiness empoyeeBusiness;
    private CommonBusiness exportationReportBusiness;
    private CommonBusiness importationReportBusiness;
    private CommonBusiness orderBusiness;

    public BusinessMaker() {
        empoyeeBusiness = new EmployeeBusinessImpl();
        exportationReportBusiness = new ExportationReportBusinessImpl();
        importationReportBusiness = new ImportationReportBusinessImpl();
        orderBusiness = new OrderBusinessImpl();
    }

    public BusinessMaker(CommonDAO dao) {
        empoyeeBusiness = new EmployeeBusinessImpl(dao);
        exportationReportBusiness = new ExportationReportBusinessImpl(dao);
        importationReportBusiness = new ImportationReportBusinessImpl(dao);
        orderBusiness = new OrderBusinessImpl(dao);
    }

    //<editor-fold desc="employee bussiness" defaultstate="collapsed">
    public List<Employee> getAllEmployee() {
        return empoyeeBusiness.getAll();
    }

    public Employee getOneEmployeeById(int id) {
        return (Employee) empoyeeBusiness.getOneById(id);
    }
    //</editor-fold>

    //<editor-fold desc="exportation report bussiness" defaultstate="collapsed">
    public List<ExportationReport> getAllExportationReport() {
        return exportationReportBusiness.getAll();
    }

    public ExportationReport getOneExportationReportById(int id) {
        return (ExportationReport) exportationReportBusiness.getOneById(id);
    }
    //</editor-fold>

    //<editor-fold desc="exportation report bussiness" defaultstate="collapsed">
    public List<ImportationReport> getAllImportationReport() {
        return importationReportBusiness.getAll();
    }

    public ImportationReport getOneImportationReportById(int id) {
        return (ImportationReport) importationReportBusiness.getOneById(id);
    }
    //</editor-fold>

    //<editor-fold desc="exportation report bussiness" defaultstate="collapsed">
    public List<Order> getAllOrder() {
        return orderBusiness.getAll();
    }

    public Order getOneOrderById(int id) {
        return (Order) orderBusiness.getOneById(id);
    }
    //</editor-fold>
}
