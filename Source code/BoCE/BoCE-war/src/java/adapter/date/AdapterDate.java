package adapter.date;

import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author truongbb
 */
public class AdapterDate implements DateFormartTarget {

    @Override
    public Date getDateFormart(String dateStr) {
        Date date = null;

        try {
            date = new AdapteeDate().formatDate(dateStr);
        } catch (ParseException ex) {
            Logger.getLogger(AdapterDate.class.getName()).log(Level.SEVERE, null, ex);
        }

        return date;
    }

}
