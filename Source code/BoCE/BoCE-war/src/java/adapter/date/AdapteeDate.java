package adapter.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author truongbb
 */
public class AdapteeDate {

    public Date formatDate(String dateStr) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
    }
}
