package adapter.date;

import java.util.Date;

/**
 *
 * @author truongbb
 */
public interface DateFormartTarget {

    Date getDateFormart(String dateStr);
}
