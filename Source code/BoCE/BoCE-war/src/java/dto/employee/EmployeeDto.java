package dto.employee;

import bo.common.Account;
import bo.common.Ethnic;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.employee.Department;
import bo.employee.Literacy;
import bo.employee.Position;
import dto.employee.discipline_employee.DisciplineEmployeeDto;
import dto.employee.reward_employee.RewardEmployeeDto;
import java.util.Date;

/**
 *
 * @author truongbb
 */
public class EmployeeDto {

    private int id;
    private FullName fullName;
    private Address address;
    private Gender gender;
    private java.util.Date birthday;
    private String phone;
    private Ethnic ethnic;
    private java.util.Date startWorkingDate;
    private Literacy literacy;
    private Position position;
    private Department department;
    private Account account;
    private DisciplineEmployeeDto disciplineEmployeeDto;
    private RewardEmployeeDto rewardEmployeeDto;

    public EmployeeDto() {
    }

    public EmployeeDto(int id, FullName fullName, Address address, Gender gender, Date birthday, String phone, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, Account account, DisciplineEmployeeDto disciplineEmployeeDto, RewardEmployeeDto rewardEmployeeDto) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.gender = gender;
        this.birthday = birthday;
        this.phone = phone;
        this.ethnic = ethnic;
        this.startWorkingDate = startWorkingDate;
        this.literacy = literacy;
        this.position = position;
        this.department = department;
        this.account = account;
        this.disciplineEmployeeDto = disciplineEmployeeDto;
        this.rewardEmployeeDto = rewardEmployeeDto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FullName getFullName() {
        return fullName;
    }

    public void setFullName(FullName fullName) {
        this.fullName = fullName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Ethnic getEthnic() {
        return ethnic;
    }

    public void setEthnic(Ethnic ethnic) {
        this.ethnic = ethnic;
    }

    public Date getStartWorkingDate() {
        return startWorkingDate;
    }

    public void setStartWorkingDate(Date startWorkingDate) {
        this.startWorkingDate = startWorkingDate;
    }

    public Literacy getLiteracy() {
        return literacy;
    }

    public void setLiteracy(Literacy literacy) {
        this.literacy = literacy;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public DisciplineEmployeeDto getDisciplineEmployeeDto() {
        return disciplineEmployeeDto;
    }

    public void setDisciplineEmployeeDto(DisciplineEmployeeDto disciplineEmployeeDto) {
        this.disciplineEmployeeDto = disciplineEmployeeDto;
    }

    public RewardEmployeeDto getRewardEmployeeDto() {
        return rewardEmployeeDto;
    }

    public void setRewardEmployeeDto(RewardEmployeeDto rewardEmployeeDto) {
        this.rewardEmployeeDto = rewardEmployeeDto;
    }

}
