package dto.employee.reward_employee;

import bo.employee.reward.RewardType;
import java.util.Date;

/**
 *
 * @author truongbb
 */
public class RewardEmployeeDto {

    private int id;
    private String name;
    private RewardType rewardType;
    private java.util.Date rewardDate;
    private float bonus;
    private String description;

    public RewardEmployeeDto() {
    }

    public RewardEmployeeDto(int id, String name, RewardType rewardType, Date rewardDate, float bonus, String description) {
        this.id = id;
        this.name = name;
        this.rewardType = rewardType;
        this.rewardDate = rewardDate;
        this.bonus = bonus;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RewardType getRewardType() {
        return rewardType;
    }

    public void setRewardType(RewardType rewardType) {
        this.rewardType = rewardType;
    }

    public Date getRewardDate() {
        return rewardDate;
    }

    public void setRewardDate(Date rewardDate) {
        this.rewardDate = rewardDate;
    }

    public float getBonus() {
        return bonus;
    }

    public void setBonus(float bonus) {
        this.bonus = bonus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
