package dto.employee.discipline_employee;

import bo.employee.discipline.DisciplineType;
import java.util.Date;

/**
 *
 * @author truongbb
 */
public class DisciplineEmployeeDto {

    private int id;
    private String name;
    private DisciplineType disciplineType;
    private java.util.Date disciplineDate;
    private float forfeit;
    private String description;

    public DisciplineEmployeeDto() {
    }

    public DisciplineEmployeeDto(int id, String name, DisciplineType disciplineType, Date disciplineDate, float forfeit, String description) {
        this.id = id;
        this.name = name;
        this.disciplineType = disciplineType;
        this.disciplineDate = disciplineDate;
        this.forfeit = forfeit;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DisciplineType getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(DisciplineType disciplineType) {
        this.disciplineType = disciplineType;
    }

    public Date getDisciplineDate() {
        return disciplineDate;
    }

    public void setDisciplineDate(Date disciplineDate) {
        this.disciplineDate = disciplineDate;
    }

    public float getForfeit() {
        return forfeit;
    }

    public void setForfeit(float forfeit) {
        this.forfeit = forfeit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
