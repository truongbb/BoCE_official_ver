package dto.product;

import java.util.Date;

/**
 *
 * @author cuongnd
 */
public class ProductDto {

    private int id;
    private int productTypeId;
    private String productTypeName;
    private float unitPrice;
    private int discountId;
    private float discountRate;
    private java.util.Date discountBeginDate;
    private java.util.Date discountEndDate;
    private String description;
    private int quantity;
    private String pictureThumb;
    private int warehouseId;
    private String warehouseName;
    private String warehouseCode;
    private String houseNumber;
    private String street;
    private String district;
    private String province;
    private String warehousePhone;
    private String warehouseEmail;

    public ProductDto() {
    }

    public ProductDto(int id, int productTypeId, String productTypeName, float unitPrice, int discountId, float discountRate, Date discountBeginDate, Date discountEndDate, String description, int quantity, String pictureThumb, int warehouseId, String warehouseName, String warehouseCode, String houseNumber, String street, String district, String province, String warehousePhone, String warehouseEmail) {
        this.id = id;
        this.productTypeId = productTypeId;
        this.productTypeName = productTypeName;
        this.unitPrice = unitPrice;
        this.discountId = discountId;
        this.discountRate = discountRate;
        this.discountBeginDate = discountBeginDate;
        this.discountEndDate = discountEndDate;
        this.description = description;
        this.quantity = quantity;
        this.pictureThumb = pictureThumb;
        this.warehouseId = warehouseId;
        this.warehouseName = warehouseName;
        this.warehouseCode = warehouseCode;
        this.houseNumber = houseNumber;
        this.street = street;
        this.district = district;
        this.province = province;
        this.warehousePhone = warehousePhone;
        this.warehouseEmail = warehouseEmail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public float getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(float discountRate) {
        this.discountRate = discountRate;
    }

    public Date getDiscountBeginDate() {
        return discountBeginDate;
    }

    public void setDiscountBeginDate(Date discountBeginDate) {
        this.discountBeginDate = discountBeginDate;
    }

    public Date getDiscountEndDate() {
        return discountEndDate;
    }

    public void setDiscountEndDate(Date discountEndDate) {
        this.discountEndDate = discountEndDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPictureThumb() {
        return pictureThumb;
    }

    public void setPictureThumb(String pictureThumb) {
        this.pictureThumb = pictureThumb;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getWarehousePhone() {
        return warehousePhone;
    }

    public void setWarehousePhone(String warehousePhone) {
        this.warehousePhone = warehousePhone;
    }

    public String getWarehouseEmail() {
        return warehouseEmail;
    }

    public void setWarehouseEmail(String warehouseEmail) {
        this.warehouseEmail = warehouseEmail;
    }

}
