package dto.transaction.order;

import bo.common.person.Address;
import bo.customer.Customer;
import bo.product.Product;
import bo.transaction.PurchaseMethod;
import java.util.Date;

/**
 *
 * @author truongbb
 */
public class OrderDto {

    private int id;
    private Customer customer;
    private Address receivingAddress;
    private float additionalCharge;
    private java.util.Date expiredDate;
    private java.util.Date expectedReceivingDate;
    private float cash;
    private PurchaseMethod purchaseMethod;
    private Product product;
    private int buyingQuantity;

    public OrderDto() {
    }

    public OrderDto(int id, Customer customer, Address receivingAddress, float additionalCharge, Date expiredDate, Date expectedReceivingDate, float cash, PurchaseMethod purchaseMethod, Product product, int buyingQuantity) {
        this.id = id;
        this.customer = customer;
        this.receivingAddress = receivingAddress;
        this.additionalCharge = additionalCharge;
        this.expiredDate = expiredDate;
        this.expectedReceivingDate = expectedReceivingDate;
        this.cash = cash;
        this.purchaseMethod = purchaseMethod;
        this.product = product;
        this.buyingQuantity = buyingQuantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getReceivingAddress() {
        return receivingAddress;
    }

    public void setReceivingAddress(Address receivingAddress) {
        this.receivingAddress = receivingAddress;
    }

    public float getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(float additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getExpectedReceivingDate() {
        return expectedReceivingDate;
    }

    public void setExpectedReceivingDate(Date expectedReceivingDate) {
        this.expectedReceivingDate = expectedReceivingDate;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public PurchaseMethod getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(PurchaseMethod purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getBuyingQuantity() {
        return buyingQuantity;
    }

    public void setBuyingQuantity(int buyingQuantity) {
        this.buyingQuantity = buyingQuantity;
    }

    @Override
    public String toString() {
        return "OrderDto{" + "id=" + id + ", customer=" + customer + ", receivingAddress=" + receivingAddress + ", additionalCharge=" + additionalCharge + ", expiredDate=" + expiredDate + ", expectedReceivingDate=" + expectedReceivingDate + ", cash=" + cash + ", purchaseMethod=" + purchaseMethod + ", product=" + product + ", buyingQuantity=" + buyingQuantity + '}';
    }

}
