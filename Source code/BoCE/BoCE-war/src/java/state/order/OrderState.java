package state.order;

/**
 *
 * @author truongbb
 */
public interface OrderState {

    boolean updateState(OrderContext orderContext);
}
