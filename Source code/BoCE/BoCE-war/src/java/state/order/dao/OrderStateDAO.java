package state.order.dao;

import state.order.OrderContext;

/**
 *
 * @author truongbb
 */
public interface OrderStateDAO {

    boolean saveState(OrderContext orderContext);
}
