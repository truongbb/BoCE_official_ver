package state.order;

import state.order.dao.OrderStateDAOImpl;

/**
 *
 * @author truongbb
 */
public class Pending implements OrderState {

    @Override
    public boolean updateState(OrderContext orderContext) {
        orderContext.setOrderState(this);
        return new OrderStateDAOImpl().saveState(orderContext);
    }

}
