package builder.importation;

import bo.common.Pentad;
import bo.common.Unit;
import bo.employee.Employee;
import bo.importation.ImportationReport;
import bo.product.Product;
import bo.product.Supplier;
import java.util.Date;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class ImportationReportBuilderImpl implements ImportationReportBuilder {

    private ImportationReport report;

    public ImportationReportBuilderImpl() {
    }

    public ImportationReportBuilderImpl(ImportationReport report) {
        this.report = report;
    }

    public ImportationReport getReport() {
        return report;
    }

    public void setReport(ImportationReport report) {
        this.report = report;
    }

    @Override
    public void buildId(int id) {
        this.report.setId(id);
    }

    @Override
    public void buildWarehouseManager(Employee warehouseManager) {
        this.report.setEmployee(warehouseManager);
    }

    @Override
    public void buildImportedProducts(List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts) {
        this.report.setImportedProducts(importedProducts);
    }

    @Override
    public void buildCreatedDate(Date createdDate) {
        this.report.setCreatedDate(createdDate);
    }

    @Override
    public void buildNote(String note) {
        this.report.setNote(note);
    }

    public ImportationReport getImportationReport() {
        return this.getReport();
    }

}
