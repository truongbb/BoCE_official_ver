package builder.importation;

import bo.common.Pentad;
import bo.common.Unit;
import bo.employee.Employee;
import bo.product.Product;
import bo.product.Supplier;
import java.util.Date;
import java.util.List;

/**
 *
 * @author truongbb
 */
public class ImportationReportDirector {

    public void construct(ImportationReportBuilder builder, int id, Employee warehouseManager,
            List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts, Date createdDate, String note) {
        builder.buildId(id);
        builder.buildImportedProducts(importedProducts);
        builder.buildWarehouseManager(warehouseManager);
        builder.buildCreatedDate(createdDate);
        builder.buildNote(note);
    }
}
