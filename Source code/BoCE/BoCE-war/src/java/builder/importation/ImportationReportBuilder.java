package builder.importation;

import bo.common.Pentad;
import bo.common.Unit;
import bo.employee.Employee;
import bo.product.Product;
import bo.product.Supplier;
import java.util.List;

/**
 *
 * @author truongbb
 */
public interface ImportationReportBuilder {

    void buildId(int id);

    void buildWarehouseManager(Employee warehouseManager);

    void buildImportedProducts(List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts);

    void buildCreatedDate(java.util.Date createdDate);

    void buildNote(String note);

}
