package servlet.client.login;

import bo.common.Account;
import bo.transaction.Cart;
import business.account.AccountBusiness;
import business.account.AccountBusinessImpl;
import dao.common.OracleDAOFactory;
import dao.common.account.OracleAccountDAOImpl;
import entities.transaction.order.OrdersProduct;
import java.io.IOException;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.common.person.AccountFacadeLocal;

/**
 *
 * @author truongbb
 */
public class LoginClient extends HttpServlet {

    @EJB
    private AccountFacadeLocal accountFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("pass");

//        AccountBusiness accountBusiness = new AccountBusinessImpl(new OracleAccountDAOImpl(new OracleDAOFactory()));
//        Account account = accountBusiness.login(username, password);
//        if (account != null) {
//            request.setAttribute("username", account.getUsername());
//            HttpSession session = request.getSession();
//            session.setAttribute("cart", new Cart(username, new ArrayList<>()));
//            session.setAttribute("account", account);
//            request.getRequestDispatcher("Cart_Preparation").forward(request, response);
//        } else {
//            request.setAttribute("error", "Username or password's invalid!");
//            request.setAttribute("username", username);
//            request.setAttribute("pass", password);
//            request.getRequestDispatcher("client_login.jsp").forward(request, response);
//        }
        entities.common.person.Account account = this.accountFacade.login(username, password);

        if (account != null) {
            request.setAttribute("username", account.getUsername());
            HttpSession session = request.getSession();
            session.setAttribute("cart", new ArrayList<OrdersProduct>());
            session.setAttribute("account", account);
            request.getRequestDispatcher("Cart_Preparation").forward(request, response);
        } else {
            request.setAttribute("error", "Username or password's invalid!");
            request.setAttribute("username", username);
            request.setAttribute("pass", password);
            request.getRequestDispatcher("client_login.jsp").forward(request, response);
        }
    }

}
