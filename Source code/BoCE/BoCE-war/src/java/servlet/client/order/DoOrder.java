package servlet.client.order;

import dao.transaction.order.OracleOrderDAOImpl;
import dao.transaction.order.OrderDAO;
import entities.transaction.order.Orders;
import entities.transaction.order.PurchaseMethod;
import java.io.IOException;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.transaction.order.OrdersFacadeLocal;
import sessionbeans.transaction.order.PurchaseMethodFacadeLocal;

/**
 *
 * @author truongbb
 */
public class DoOrder extends HttpServlet {

    @EJB
    private PurchaseMethodFacadeLocal purchaseMethodFacade;

    @EJB
    private OrdersFacadeLocal ordersFacade;

    private OrderDAO orderDAO;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String receivingAddress = request.getParameter("receivingAddress");
        String purchaseMethodStr = request.getParameter("purchaseMethod");

        HttpSession session = request.getSession(false);
        Orders order = (Orders) session.getAttribute("order");

        order.setReceivingAddress(receivingAddress);

        PurchaseMethod purchaseMethod = purchaseMethodFacade.find(new BigDecimal(purchaseMethodStr));
        order.setPurchaseMethodId(purchaseMethod);

        int count = ordersFacade.count();
        order.setId(new BigDecimal(count + 1));

        orderDAO = new OracleOrderDAOImpl();
//        ordersFacade.create(order);

        boolean result = orderDAO.insertIntoFullTable(order);

        if (result) {
            request.getRequestDispatcher("client_after_order.jsp").forward(request, response);
        } else {
            System.out.println("3333333333");
        }

    }

}
