package servlet.client.order;

import entities.common.person.Account;
import entities.transaction.order.Orders;
import entities.transaction.order.OrdersProduct;
import entities.common.person.Person;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.common.person.PersonFacadeLocal;

/**
 *
 * @author truongbb
 */
public class PreOrder extends HttpServlet {

    @EJB
    private PersonFacadeLocal personFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            HttpSession session = request.getSession(false);

            Account account = (Account) session.getAttribute("account");
            
            Person person = personFacade.find(account.getPersonId().getId());

            Orders order = new Orders();
            order.setCustomerId(person.getCustomer());

            List<OrdersProduct> cart = (List<OrdersProduct>) session.getAttribute("cart");
            double cash = cart.stream().mapToDouble(t -> t.getProductId().getUnitPrice() * t.getQuantity().intValue()).sum();
            order.setAdditionalCharge(cash * 0.1);
            order.setCash(cash + cash * 0.1);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            Date plusDate = calendar.getTime();
            order.setExpextedReceivingDate(plusDate);
            order.setOrdersProductList(cart);

            session.setAttribute("order", order);

            request.getRequestDispatcher("client_pre_order.jsp").forward(request, response);
    }

}
