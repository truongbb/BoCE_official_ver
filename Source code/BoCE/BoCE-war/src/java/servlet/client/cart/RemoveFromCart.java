package servlet.client.cart;

import bo.common.Pair;
import bo.transaction.Cart;
import entities.transaction.order.OrdersProduct;
import entities.product.Product;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.product.ProductFacadeLocal;

/**
 *
 * @author truongbb
 */
public class RemoveFromCart extends HttpServlet {

    @EJB
    private ProductFacadeLocal productFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String productId = request.getParameter("productId");
        Product product = productFacade.find(new BigDecimal(productId));

        if (product != null) {
            HttpSession session = request.getSession(false);
            List<OrdersProduct> cart = (List<OrdersProduct>) session.getAttribute("cart");

            cart = cart.stream().filter(t -> t.getProductId().getProductId().intValue() != Integer.valueOf(productId)).collect(Collectors.toList());

            session.setAttribute("cart", cart);
            request.getRequestDispatcher("client_cart.jsp").forward(request, response);
        }
    }

}
