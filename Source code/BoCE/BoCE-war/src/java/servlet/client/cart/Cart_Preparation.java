package servlet.client.cart;

import entities.product.Product;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.product.ProductFacadeLocal;

/**
 *
 * @author truongbb
 */
public class Cart_Preparation extends HttpServlet {

    @EJB
    private ProductFacadeLocal productFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Product> products = productFacade.findAll();
        HttpSession session = request.getSession(false);
        session.setAttribute("products", products);
        request.getRequestDispatcher("client_cart.jsp").forward(request, response);

    }

}
