package servlet.client.product;

import entities.product.Product;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionbeans.product.ProductFacadeLocal;

/**
 *
 * @author truongbb
 */
public class SearchProduct extends HttpServlet {

    @EJB
    private ProductFacadeLocal productFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productName = request.getParameter("product-name");
        List<Product> searchResult = productFacade.searchByName(productName);

        HttpSession session = request.getSession(false);

        List<Product> products = (List<Product>) session.getAttribute("products");

        products = searchResult;

        session.setAttribute("products", products);

        request.getRequestDispatcher("client_cart.jsp").forward(request, response);
    }

}
