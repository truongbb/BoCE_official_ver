package servlet.admin.login;

import bo.common.Account;
import business.account.AccountBusiness;
import business.account.AccountBusinessImpl;
import dao.common.OracleDAOFactory;
import dao.common.account.OracleAccountDAOImpl;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionbeans.common.person.AccountFacadeLocal;

/**
 *
 * @author truongbb
 */
public class LoginAdmin extends HttpServlet {

    @EJB
    private AccountFacadeLocal accountFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

//        AccountBusiness accountBusiness = new AccountBusinessImpl(new OracleAccountDAOImpl(new OracleDAOFactory()));
//        Account account = accountBusiness.login(username, password);
        entities.common.person.Account account = this.accountFacade.login(username, password);
        if (account != null) {
            request.setAttribute("username", account.getUsername());
            request.getRequestDispatcher("admin/index.jsp").forward(request, response);

        }

    }

}
