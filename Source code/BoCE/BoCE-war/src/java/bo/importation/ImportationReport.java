package bo.importation;

import bo.common.Pentad;
import bo.common.Unit;
import bo.employee.Employee;
import bo.product.Product;
import bo.product.Supplier;
import java.util.Date;
import java.util.List;

/**
 *
 * @author andqt
 *
 * @editor truongbb on 18/02/2018: change warehouse manager to employee
 */
public class ImportationReport {

    private int id;
    private Employee employee;
    private List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts;
    /*
        Product - Supplier - Unit - Quantity - UnitPrice
    */
    private Date createdDate;
    private String note;

    public ImportationReport() {
    }

    public ImportationReport(int id, Employee employee, List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts, Date createdDate, String note) {
        this.id = id;
        this.employee = employee;
        this.importedProducts = importedProducts;
        this.createdDate = createdDate;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Pentad<Product, Supplier, Unit, Integer, Float>> getImportedProducts() {
        return importedProducts;
    }

    public void setImportedProducts(List<Pentad<Product, Supplier, Unit, Integer, Float>> importedProducts) {
        this.importedProducts = importedProducts;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ImportationReport{" + "id=" + id + ", employee=" + employee + ", importedProducts=" + importedProducts + ", createdDate=" + createdDate + ", note=" + note + '}';
    }

}
