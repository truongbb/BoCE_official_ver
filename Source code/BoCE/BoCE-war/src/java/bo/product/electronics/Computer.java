package bo.product.electronics;

import bo.common.Color;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 *
 * @editor truongbb on 19/02/2018: add constructors
 */
public class Computer extends Electronics {

    private String cpu;
    private String ram;
    private String hdd;
    private String ssd;

    public Computer() {
    }

    public Computer(String cpu, String ram, String hdd, String ssd) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.ssd = ssd;
    }

    public Computer(String cpu, String ram, String hdd, String ssd, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh) {
        super(electronicSku, name, electronicCategory, color, weigh);
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.ssd = ssd;
    }

    public Computer(String cpu, String ram, String hdd, String ssd, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(electronicSku, name, electronicCategory, color, weigh, productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.ssd = ssd;
    }

    public String getCPU() {
        return cpu;
    }

    public void setCPU(String CPU) {
        this.cpu = CPU;
    }

    public String getRAM() {
        return ram;
    }

    public void setRAM(String RAM) {
        this.ram = RAM;
    }

    public String getHDD() {
        return hdd;
    }

    public void setHDD(String HDD) {
        this.hdd = HDD;
    }

    public String getSSD() {
        return ssd;
    }

    public void setSSD(String SSD) {
        this.ssd = SSD;
    }

    @Override
    public String toString() {
        return "Computer{" + "CPU=" + cpu + ", RAM=" + ram + ", HDD=" + hdd + ", SSD=" + ssd + '}';
    }

}
