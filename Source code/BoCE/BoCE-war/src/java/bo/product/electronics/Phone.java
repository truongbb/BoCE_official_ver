package bo.product.electronics;

import bo.common.Color;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 * @editor truongbb on 19/02/2018: add constructors
 */
public class Phone extends Electronics {

    public Phone() {
    }

    public Phone(int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh) {
        super(electronicSku, name, electronicCategory, color, weigh);
    }

    public Phone(int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(electronicSku, name, electronicCategory, color, weigh, productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
    }

}
