package bo.product.electronics;

import bo.common.Color;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 * 
 * @editor truongbb on 19/02/2018: add constructors
 */
public class Television extends Electronics {

    private float screenSize;
    private TVScreenQuality screenQuality;

    public Television() {
    }

    public Television(float screenSize, TVScreenQuality screenQuality) {
        this.screenSize = screenSize;
        this.screenQuality = screenQuality;
    }

    public Television(float screenSize, TVScreenQuality screenQuality, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh) {
        super(electronicSku, name, electronicCategory, color, weigh);
        this.screenSize = screenSize;
        this.screenQuality = screenQuality;
    }

    public Television(float screenSize, TVScreenQuality screenQuality, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(electronicSku, name, electronicCategory, color, weigh, productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
        this.screenSize = screenSize;
        this.screenQuality = screenQuality;
    }

    public float getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(float screenSize) {
        this.screenSize = screenSize;
    }

    public TVScreenQuality getScreenQuality() {
        return screenQuality;
    }

    public void setScreenQuality(TVScreenQuality screenQuality) {
        this.screenQuality = screenQuality;
    }

    @Override
    public String toString() {
        return "Television{" + "screenSize=" + screenSize + ", screenQuality=" + screenQuality + '}';
    }

}
