package bo.product.electronics;

/**
 *
 * @author hongtt
 */
public class TVScreenQuality {

    private int id;
    private String quality;

    public TVScreenQuality() {
    }

    public TVScreenQuality(int id, String quality) {
        this.id = id;
        this.quality = quality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {
        return "TVScreenQuality{" + "id=" + id + ", quality=" + quality + '}';
    }

}
