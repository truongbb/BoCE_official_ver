package bo.product.electronics;

import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;
import bo.common.Color;

/**
 *
 * @author hongtt
 * @editor truongbb on 16/02/2018: change modifier to protected, add
 * constructors
 */
public class Electronics extends Product {

    protected int electronicSku;
    protected String name;
    protected ElectronicCategory electronicCategory;
    protected Color color;
    protected float weigh;

    public Electronics() {
    }

    public Electronics(int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh) {
        this.electronicSku = electronicSku;
        this.name = name;
        this.electronicCategory = electronicCategory;
        this.color = color;
        this.weigh = weigh;
    }

    public Electronics(int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
        this.electronicSku = electronicSku;
        this.name = name;
        this.electronicCategory = electronicCategory;
        this.color = color;
        this.weigh = weigh;
    }

    public int getElectronicSku() {
        return electronicSku;
    }

    public void setElectronicSku(int electronicSku) {
        this.electronicSku = electronicSku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ElectronicCategory getElectronicCategory() {
        return electronicCategory;
    }

    public void setElectronicCategory(ElectronicCategory electronicCategory) {
        this.electronicCategory = electronicCategory;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getWeigh() {
        return weigh;
    }

    public void setWeigh(float weigh) {
        this.weigh = weigh;
    }

    @Override
    public String toString() {
        return "Electronics{" + "electronicSku=" + electronicSku + ", name=" + name + ", electronicCategory=" + electronicCategory + ", color=" + color + ", weigh=" + weigh + '}';
    }

}
