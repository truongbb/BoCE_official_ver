package bo.product.electronics;

import bo.common.Color;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 *
 * @editor truongbb on 19/02/2018: add constructors
 */
public class PC extends Computer {

    public PC() {
    }

    public PC(String CPU, String RAM, String HDD, String SSD) {
        super(CPU, RAM, HDD, SSD);
    }

    public PC(String cpu, String ram, String hdd, String ssd, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh) {
        super(cpu, ram, hdd, ssd, electronicSku, name, electronicCategory, color, weigh);
    }

    public PC(String cpu, String ram, String hdd, String ssd, int electronicSku, String name, ElectronicCategory electronicCategory, Color color, float weigh, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(cpu, ram, hdd, ssd, electronicSku, name, electronicCategory, color, weigh, productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
    }

}
