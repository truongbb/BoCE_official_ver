package bo.product;

/**
 *
 * @author truongbb
 * @editor truongbb on 16/02/2018: change modifier to protected
 */
public class Product {

    protected int id;
    protected ProductType productType;
    protected float unitPrice;
    protected Discount discount;
    protected String description;
    protected int quantity;
    protected String pictureThumb;
    protected Warehouse warehouse;

    public Product() {
    }

    public Product(int id, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        this.id = id;
        this.productType = productType;
        this.unitPrice = unitPrice;
        this.discount = discount;
        this.description = description;
        this.quantity = quantity;
        this.pictureThumb = pictureThumb;
        this.warehouse = warehouse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPictureThumb() {
        return pictureThumb;
    }

    public void setPictureThumb(String pictureThumb) {
        this.pictureThumb = pictureThumb;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", productType=" + productType + ", unitPrice=" + unitPrice + ", discount=" + discount + ", description=" + description + ", quantity=" + quantity + ", pictureThumb=" + pictureThumb + ", warehouse=" + warehouse + '}';
    }

}
