package bo.product.book;

import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 *
 * @editor truongbb on 19/02/2018: add constructors
 */
public class TechnicalBook extends Book {

    public TechnicalBook() {
    }

    public TechnicalBook(int id, String name, Author author, Publisher publisher, BookCategory bookCategory, int publishedYear, int totalPage) {
        super(id, name, author, publisher, bookCategory, publishedYear, totalPage);
    }

    public TechnicalBook(int id, String name, Author author, Publisher publisher, BookCategory bookCategory, int publishedYear, int totalPage, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(id, name, author, publisher, bookCategory, publishedYear, totalPage, productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
    }

}
