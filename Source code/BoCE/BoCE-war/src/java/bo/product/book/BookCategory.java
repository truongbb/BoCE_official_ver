package bo.product.book;

/**
 *
 * @author hongtt
 */
public class BookCategory {

    private int id;
    private String name;

    public BookCategory() {
    }

    public BookCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BookCategory{" + "id=" + id + ", name=" + name + '}';
    }

}
