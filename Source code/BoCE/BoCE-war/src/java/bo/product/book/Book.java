package bo.product.book;

import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 * @editor truongbb on 16/02/2018: change modifier to protected
 * @editor truongbb on 18/02/2018: add a constructor
 */
public class Book extends Product {

    protected int bookId;
    protected String name;
    protected Author author;
    protected Publisher publisher;
    protected BookCategory bookCategory;
    protected int publishedYear;
    protected int totalPage;

    public Book() {
    }

    public Book(int id, String name, Author author, Publisher publisher, BookCategory bookCategory, int publishedYear, int totalPage) {
        this.bookId = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.bookCategory = bookCategory;
        this.publishedYear = publishedYear;
        this.totalPage = totalPage;
    }

    public Book(int id, String name, Author author, Publisher publisher, BookCategory bookCategory, int publishedYear, int totalPage, int productId, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(productId, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
        this.bookId = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.bookCategory = bookCategory;
        this.publishedYear = publishedYear;
        this.totalPage = totalPage;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public BookCategory getBookCategory() {
        return bookCategory;
    }

    public void setBookCategory(BookCategory bookCategory) {
        this.bookCategory = bookCategory;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(int publishedYear) {
        this.publishedYear = publishedYear;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public String toString() {
        return "Book{" + "bookId=" + bookId + ", name=" + name + ", author=" + author + ", publisher=" + publisher + ", bookCategory=" + bookCategory + ", publishedYear=" + publishedYear + ", totalPage=" + totalPage + '}';
    }

}
