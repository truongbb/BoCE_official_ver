package bo.product;

import java.util.Date;

/**
 *
 * @author truongbb
 */
public class Discount {

    private int id;
    private float rate;
    private java.util.Date beginDate;
    private java.util.Date endDate;

    public Discount() {
    }

    public Discount(int id, float rate, Date beginDate, Date endDate) {
        this.id = id;
        this.rate = rate;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Discount{" + "id=" + id + ", rate=" + rate + ", beginDate=" + beginDate + ", endDate=" + endDate + '}';
    }

}
