package bo.product;

import bo.common.person.Address;

/**
 *
 * @author truongbb
 */
public class Warehouse {

    private int id;
    private String name;
    private String code;
    private Address location;
    private String phone;

    public Warehouse() {
    }

    public Warehouse(int id, String name, String code, Address location, String phone) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.location = location;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Warehouse{" + "id=" + id + ", name=" + name + ", code=" + code + ", location=" + location + ", phone=" + phone + '}';
    }

}
