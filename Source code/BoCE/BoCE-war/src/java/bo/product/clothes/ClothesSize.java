package bo.product.clothes;

/**
 *
 * @author hongtt
 */
public class ClothesSize {

    private int id;
    private String size;

    public ClothesSize() {
    }

    public ClothesSize(int id, String size) {
        this.id = id;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ClothesSize{" + "id=" + id + ", size=" + size + '}';
    }

}
