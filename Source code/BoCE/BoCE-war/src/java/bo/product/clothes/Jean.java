package bo.product.clothes;

import bo.common.Color;
import bo.product.Discount;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 *
 * @editor truongbb on 19/02/2018: add constructors
 */
public class Jean extends Clothes {

    public Jean() {
    }

    public Jean(int clothesId, String name, ClothesCategory clothesCategory, Color mainColor, String clothesDescription, Style style, ClothesSize size, int id, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(clothesId, name, clothesCategory, mainColor, clothesDescription, style, size, id, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
    }

    public Jean(int clothesId, String name, ClothesCategory clothesCategory, Color mainColor, String clothesDescription, Style style, ClothesSize size) {
        super(clothesId, name, clothesCategory, mainColor, clothesDescription, style, size);
    }

}
