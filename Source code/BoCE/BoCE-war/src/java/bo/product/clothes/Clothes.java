package bo.product.clothes;

import bo.common.Color;
import bo.product.Discount;
import bo.product.Product;
import bo.product.ProductType;
import bo.product.Warehouse;

/**
 *
 * @author hongtt
 * @editor truongbb on 16/02/2018: change modifier to protected, add
 * constructors, change some fields' name
 */
public class Clothes extends Product {

    protected int clothesId;
    protected String name;
    protected ClothesCategory clothesCategory;
    protected Color mainColor;
    protected String clothesDescription;
    protected Style style;
    protected ClothesSize size;

    public Clothes() {
    }

    public Clothes(int clothesId, String name, ClothesCategory clothesCategory, Color mainColor, String clothesDescription, Style style, ClothesSize size, int id, ProductType productType, float unitPrice, Discount discount, String description, int quantity, String pictureThumb, Warehouse warehouse) {
        super(id, productType, unitPrice, discount, description, quantity, pictureThumb, warehouse);
        this.clothesId = clothesId;
        this.name = name;
        this.clothesCategory = clothesCategory;
        this.mainColor = mainColor;
        this.clothesDescription = clothesDescription;
        this.style = style;
        this.size = size;
    }

    public Clothes(int clothesId, String name, ClothesCategory clothesCategory, Color mainColor, String clothesDescription, Style style, ClothesSize size) {
        this.clothesId = clothesId;
        this.name = name;
        this.clothesCategory = clothesCategory;
        this.mainColor = mainColor;
        this.clothesDescription = clothesDescription;
        this.style = style;
        this.size = size;
    }

    public int getClothesId() {
        return clothesId;
    }

    public void setClothesId(int clothesId) {
        this.clothesId = clothesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClothesCategory getClothesCategory() {
        return clothesCategory;
    }

    public void setClothesCategory(ClothesCategory clothesCategory) {
        this.clothesCategory = clothesCategory;
    }

    public Color getMainColor() {
        return mainColor;
    }

    public void setMainColor(Color mainColor) {
        this.mainColor = mainColor;
    }

    public String getClothesDescription() {
        return clothesDescription;
    }

    public void setClothesDescription(String clothesDescription) {
        this.clothesDescription = clothesDescription;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public ClothesSize getSize() {
        return size;
    }

    public void setSize(ClothesSize size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Clothes{" + "clothesId=" + clothesId + ", name=" + name + ", clothesCategory=" + clothesCategory + ", mainColor=" + mainColor + ", clothesDescription=" + clothesDescription + ", style=" + style + ", size=" + size + '}';
    }

}
