package bo.employee.cashier;

import bo.common.Account;
import bo.common.Ethnic;
import bo.common.Tetrad;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.employee.Department;
import bo.employee.Literacy;
import bo.employee.Position;
import bo.employee.discipline.Discipline;
import bo.employee.reward.Reward;
import java.util.Date;
import java.util.List;

/**
 *
 * @author andqt
 *
 * @editor truongbb on 19/02/2018: add constructors
 */
public class OnlineCashier extends Cashier {

    public OnlineCashier() {
    }

    public OnlineCashier(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, Account account, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList) {
        super(id, ethnic, startWorkingDate, literacy, position, department, account, disciplineList, rewardList);
    }

    public OnlineCashier(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList) {
        super(id, ethnic, startWorkingDate, literacy, position, department, disciplineList, rewardList);
    }

    public OnlineCashier(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList, FullName fullName, Address address, Gender gender, Date birthDay, String phone, Account account) {
        super(id, ethnic, startWorkingDate, literacy, position, department, disciplineList, rewardList, fullName, address, gender, birthDay, phone, account);
    }

}
