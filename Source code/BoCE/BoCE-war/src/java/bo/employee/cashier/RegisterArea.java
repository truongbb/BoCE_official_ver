package bo.employee.cashier;

/**
 *
 * @author andqt
 */
public class RegisterArea {

    private int id;
    private int floor;
    private int registerAreaNumber;

    public RegisterArea() {
    }

    public RegisterArea(int id, int floor, int registerAreaNumber) {
        this.id = id;
        this.floor = floor;
        this.registerAreaNumber = registerAreaNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getRegisterAreaNumber() {
        return registerAreaNumber;
    }

    public void setRegisterAreaNumber(int registerAreaNumber) {
        this.registerAreaNumber = registerAreaNumber;
    }

    @Override
    public String toString() {
        return "RegisterArea{" + "id=" + id + ", floor=" + floor + ", registerAreaNumber=" + registerAreaNumber + '}';
    }

}
