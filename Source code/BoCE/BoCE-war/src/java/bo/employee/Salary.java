package bo.employee;

/**
 *
 * @author andqt
 *
 * update by thucpn on 11/02: đổi kiểu dữ liệu của basicSalary từ Salary thành
 * float
 */
public class Salary {

    private int salaryLevel;
    private float basicSalary;
    private float salaryRate;
    private float allowanceRate;

    public Salary() {
    }

    public Salary(int salaryLevel, float basicSalary, float salaryRate, float allowanceRate) {
        this.salaryLevel = salaryLevel;
        this.basicSalary = basicSalary;
        this.salaryRate = salaryRate;
        this.allowanceRate = allowanceRate;
    }

    public int getSalaryLevel() {
        return salaryLevel;
    }

    public void setSalaryLevel(int salaryLevel) {
        this.salaryLevel = salaryLevel;
    }

    public float getBasicSalary() {
        return basicSalary;
    }

    public float getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryRate(float salaryRate) {
        this.salaryRate = salaryRate;
    }

    public float getAllowanceRate() {
        return allowanceRate;
    }

    public void setAllowanceRate(float allowanceRate) {
        this.allowanceRate = allowanceRate;
    }

    @Override
    public String toString() {
        return "Salary{" + "salaryLevel=" + salaryLevel + ", basicSalary=" + basicSalary + ", salaryRate=" + salaryRate + ", allowanceRate=" + allowanceRate + '}';
    }

}
