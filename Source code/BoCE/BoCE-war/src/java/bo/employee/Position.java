package bo.employee;

/**
 *
 * @author andqt
 */
public class Position {

    private int id;
    private String name;
    private Salary salary;

    public Position() {
    }

    public Position(int id, String name, Salary salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Salary getSalary() {
        return salary;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Position{" + "id=" + id + ", name=" + name + ", salary=" + salary + '}';
    }

}
