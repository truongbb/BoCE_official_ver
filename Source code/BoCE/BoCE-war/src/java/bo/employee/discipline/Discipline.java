package bo.employee.discipline;

/**
 *
 * @author andqt
 * @editor truongbb on 115/02/2018: remove some fields
 */
public class Discipline {

    private int id;
    private String name;
    private DisciplineType disciplineType;

    public Discipline() {
    }

    public Discipline(int id, String name, DisciplineType disciplineType) {
        this.id = id;
        this.name = name;
        this.disciplineType = disciplineType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DisciplineType getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(DisciplineType disciplineType) {
        this.disciplineType = disciplineType;
    }

    @Override
    public String toString() {
        return "Displine{" + "id=" + id + ", name=" + name + ", disciplineType=" + disciplineType + '}';
    }

}
