package bo.employee;

import bo.employee.reward.Reward;
import bo.employee.discipline.Discipline;
import bo.common.Account;
import bo.common.Ethnic;
import bo.common.Tetrad;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.common.person.Person;
import java.util.Date;
import java.util.List;

/**
 *
 * @author hongtt
 *
 * update by thucpn on 09/02
 * @editor truongbb on 15/02/2018: edit some fields
 * @editor truongbb on 16/02/2018: change modifier to protected, add more
 * constructor
 * @editor truongbb on 19/02/2018: edit reward and discipline
 */
public class Employee extends Person {

    protected int id;
    protected Ethnic ethnic;
    protected Date startWorkingDate;
    protected Literacy literacy;
    protected Position position;
    protected Department department;
    protected List<Tetrad<Discipline, String, Date, Float>> disciplineList;
    /*
        Discipline - Descritpion - DisciplineDate - Forfeit
     */
    protected List<Tetrad<Reward, String, Date, Float>> rewardList;

    /*
        Reward - Description - RewardDate - Bonus
     */
    public Employee() {
    }

    public Employee(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, Account account, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList) {
        this.id = id;
        this.ethnic = ethnic;
        this.startWorkingDate = startWorkingDate;
        this.literacy = literacy;
        this.position = position;
        this.department = department;
        this.account = account;
        this.disciplineList = disciplineList;
        this.rewardList = rewardList;
    }

    public Employee(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList) {
        this.id = id;
        this.ethnic = ethnic;
        this.startWorkingDate = startWorkingDate;
        this.literacy = literacy;
        this.position = position;
        this.department = department;
        this.disciplineList = disciplineList;
        this.rewardList = rewardList;
    }

    public Employee(int id, Ethnic ethnic, Date startWorkingDate, Literacy literacy, Position position, Department department, List<Tetrad<Discipline, String, Date, Float>> disciplineList, List<Tetrad<Reward, String, Date, Float>> rewardList, FullName fullName, Address address, Gender gender, Date birthDay, String phone, Account account) {
        super(fullName, address, gender, birthDay, phone, account);
        this.id = id;
        this.ethnic = ethnic;
        this.startWorkingDate = startWorkingDate;
        this.literacy = literacy;
        this.position = position;
        this.department = department;
        this.disciplineList = disciplineList;
        this.rewardList = rewardList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ethnic getEthnic() {
        return ethnic;
    }

    public void setEthnic(Ethnic ethnic) {
        this.ethnic = ethnic;
    }

    public Date getStartWorkingDate() {
        return startWorkingDate;
    }

    public void setStartWorkingDate(Date startWorkingDate) {
        this.startWorkingDate = startWorkingDate;
    }

    public Literacy getLiteracy() {
        return literacy;
    }

    public void setLiteracy(Literacy literacy) {
        this.literacy = literacy;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Tetrad<Discipline, String, Date, Float>> getDisciplineList() {
        return disciplineList;
    }

    public void setDisciplineList(List<Tetrad<Discipline, String, Date, Float>> disciplineList) {
        this.disciplineList = disciplineList;
    }

    public List<Tetrad<Reward, String, Date, Float>> getRewardList() {
        return rewardList;
    }

    public void setRewardList(List<Tetrad<Reward, String, Date, Float>> rewardList) {
        this.rewardList = rewardList;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", ethnic=" + ethnic + ", startWorkingDate=" + startWorkingDate + ", literacy=" + literacy + ", position=" + position + ", department=" + department + ", disciplineList=" + disciplineList + ", rewardList=" + rewardList + '}';
    }

}
