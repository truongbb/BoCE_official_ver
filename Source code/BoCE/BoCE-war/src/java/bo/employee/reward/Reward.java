package bo.employee.reward;

/**
 *
 * @author andqt
 * @editor truongbb on 115/02/2018: remove some fields
 */
public class Reward {

    private int id;
    private String name;
    private RewardType type;

    public Reward() {
    }

    public Reward(int id, String name, RewardType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RewardType getType() {
        return type;
    }

    public void setType(RewardType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Reward{" + "id=" + id + ", name=" + name + ", type=" + type + '}';
    }

}
