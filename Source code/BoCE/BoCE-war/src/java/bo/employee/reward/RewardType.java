package bo.employee.reward;

/**
 *
 * @author andqt
 */
public class RewardType {

    private int id;
    private String type;

    public RewardType() {
    }

    public RewardType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "RewardType{" + "id=" + id + ", type=" + type + '}';
    }

}
