package bo.exportation;

import bo.common.Tetrad;
import bo.common.Unit;
import bo.employee.Employee;
import bo.product.Product;
import java.util.Date;
import java.util.List;

/**
 *
 * @author andqt
 * @editor truongbb on 17/02/2018: change warehouseManger to employee
 */
public class ExportationReport {

    private int id;
    private Employee employee;
    private List<Tetrad<Product, Unit, Integer, Float>> exportedProducts;
    /*
        Product - Unit  - Quantity - UnitPrice
     */
    private Date createdDate;
    private String note;

    public ExportationReport() {
    }

    public ExportationReport(int id, Employee employee, List<Tetrad<Product, Unit, Integer, Float>> exportedProducts, Date createdDate, String note) {
        this.id = id;
        this.employee = employee;
        this.exportedProducts = exportedProducts;
        this.createdDate = createdDate;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Tetrad<Product, Unit, Integer, Float>> getExportedProducts() {
        return exportedProducts;
    }

    public void setExportedProducts(List<Tetrad<Product, Unit, Integer, Float>> exportedProducts) {
        this.exportedProducts = exportedProducts;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ExportationReport{" + "id=" + id + ", employee=" + employee + ", exportedProducts=" + exportedProducts + ", createdDate=" + createdDate + ", note=" + note + '}';
    }

}
