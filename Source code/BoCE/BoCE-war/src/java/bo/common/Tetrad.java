package bo.common;

/**
 *
 * @author truongbb
 * @param <T>
 * @param <E>
 * @param <S>
 * @param <U>
 */
public class Tetrad<T, E, S, U> {

    private T t;
    private E e;
    private S s;
    private U u;

    public Tetrad() {
    }

    public Tetrad(T t, E e, S s, U u) {
        this.t = t;
        this.e = e;
        this.s = s;
        this.u = u;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    @Override
    public String toString() {
        return "Tetrad{" + "t=" + t + ", e=" + e + ", s=" + s + ", u=" + u + '}';
    }

}
