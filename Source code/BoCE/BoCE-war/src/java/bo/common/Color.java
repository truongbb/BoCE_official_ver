package bo.common;

/**
 *
 * @author hongtt
 * @editor truongbb: moving into package common
 */
public class Color {

    private int id;
    private String code;
    private String color;

    public Color() {
    }

    public Color(int id, String color, String code) {
        this.id = id;
        this.color = color;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Color{" + "id=" + id + ", color=" + color + ", code=" + code + '}';
    }
}
