package bo.common;

/**
 *
 * @author andqt
 */
public class Unit {

    private int id;
    private String value;

    public Unit() {
    }

    public Unit(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Unit{" + "id=" + id + ", value=" + value + '}';
    }

}
