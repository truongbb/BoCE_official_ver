package bo.common;

/**
 *
 * @author andqt
 *
 * update by thucpn on 09/02
 * @editor truongbb on 16/02/2018: add toString
 */
public class Ethnic {

    private int id;
    private String value;

    public Ethnic() {
    }

    public Ethnic(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Ethnic{" + "id=" + id + ", value=" + value + '}';
    }

}
