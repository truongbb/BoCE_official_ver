package bo.common.person;

/**
 *
 * @author truongbb
 */
public class Address {

    private String houseNumber;
    private String street;
    private String district;
    private String province;

    public Address() {
    }

    public Address(String houseNumber, String street, String district, String province) {
        this.houseNumber = houseNumber;
        this.street = street;
        this.district = district;
        this.province = province;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return houseNumber + "#" + street + "#" + district + "#" + province;
    }

}
