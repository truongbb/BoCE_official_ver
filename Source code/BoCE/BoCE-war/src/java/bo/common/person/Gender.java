package bo.common.person;

/**
 *
 * @author andqt
 */
public class Gender {

    private int id;
    private String gender;

    public Gender() {
    }

    public Gender(int id, String value) {
        this.id = id;
        this.gender = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Gender{" + "id=" + id + ", value=" + gender + '}';
    }

}
