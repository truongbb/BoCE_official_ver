package bo.common.person;

import bo.common.Account;
import java.util.Date;

/**
 *
 * @author andqt
 *
 * @editor truongbb on 16/02/2018: change modifier to protected
 */
public class Person {

    protected FullName fullName;
    protected Address address;
    protected Gender gender;
    protected Date birthDay;
    protected String phone;
    protected Account account;

    public Person() {
    }

    public Person(FullName fullName, Address address, Gender gender, Date birthDay, String phone, Account account) {
        this.fullName = fullName;
        this.address = address;
        this.gender = gender;
        this.birthDay = birthDay;
        this.phone = phone;
        this.account = account;
    }

    public FullName getFullName() {
        return fullName;
    }

    public void setFullName(FullName fullName) {
        this.fullName = fullName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Person{" + "fullName=" + fullName + ", address=" + address + ", gender=" + gender + ", birthDay=" + birthDay + ", phone=" + phone + ", account=" + account + '}';
    }

}
