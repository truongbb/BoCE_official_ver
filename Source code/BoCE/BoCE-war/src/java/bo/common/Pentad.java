package bo.common;

/**
 *
 * @author truongbb
 * @param <T>
 * @param <S>
 * @param <U>
 * @param <E>
 * @param <K>
 */
public class Pentad<T, S, U, E, K> {

    private T t;
    private S s;
    private U u;
    private E e;
    private K k;

    public Pentad() {
    }

    public Pentad(T t, S s, U u, E e, K k) {
        this.t = t;
        this.s = s;
        this.u = u;
        this.e = e;
        this.k = k;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public K getK() {
        return k;
    }

    public void setK(K k) {
        this.k = k;
    }

    @Override
    public String toString() {
        return "Pentad{" + "t=" + t + ", s=" + s + ", u=" + u + ", e=" + e + ", k=" + k + '}';
    }

}
