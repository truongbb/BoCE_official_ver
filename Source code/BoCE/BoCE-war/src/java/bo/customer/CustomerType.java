package bo.customer;

/**
 *
 * @author andqt
 * @editor truongbb on 3/3/2018: apply singleton pattern
 */
public class CustomerType {

    private int id;
    private String type;

    // <editor-fold desc="apply singleton pattern">
    private static CustomerType customerType = new CustomerType();

    private CustomerType() {
    }

    public static CustomerType getInstance() {
        return customerType;
    }
    // </editor-fold>

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CustomerType{" + "id=" + id + ", type=" + type + '}';
    }

}
