package bo.customer;

import bo.common.Account;
import bo.common.person.Address;
import bo.common.person.FullName;
import bo.common.person.Gender;
import bo.common.person.Person;
import java.util.Date;

/**
 *
 * @author andqt
 *
 * update by thucpn on 11/02: them extents Person
 *
 * @editor truongbb on 19/02/2018: change something in constructors
 */
public class Customer extends Person {

    private int id;
    private String email;
    private CustomerType customerType;

    public Customer() {
    }

    public Customer(int id, String email, CustomerType customerType, Account account) {
        this.id = id;
        this.email = email;
        this.customerType = customerType;
        this.account = account;
    }

    public Customer(int id, String email, CustomerType customerType) {
        this.id = id;
        this.email = email;
        this.customerType = customerType;
    }

    public Customer(int id, String email, CustomerType customerType, FullName fullName, Address address, Gender gender, Date birthDay, String phone, Account account) {
        super(fullName, address, gender, birthDay, phone, account);
        this.id = id;
        this.email = email;
        this.customerType = customerType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", email=" + email + ", customerType=" + customerType + '}';
    }

}
