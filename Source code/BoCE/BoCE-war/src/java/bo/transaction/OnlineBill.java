package bo.transaction;

import java.util.Date;
import bo.employee.cashier.Cashier;

/**
 *
 * @author hongtt
 */
public class OnlineBill extends Bill {

    private Order order;
    private Cashier onlineCashier;
    private Date receivedDate;

    public OnlineBill() {
    }

    public OnlineBill(Order order, Cashier onlineCashier, Date receivedDate) {
        this.order = order;
        this.onlineCashier = onlineCashier;
        this.receivedDate = receivedDate;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Cashier getOnlineCashier() {
        return onlineCashier;
    }

    public void setOnlineCashier(Cashier onlineCashier) {
        this.onlineCashier = onlineCashier;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    @Override
    public String toString() {
        return "OnlineBill{" + "order=" + order + ", onlineCashier=" + onlineCashier + ", receivedDate=" + receivedDate + '}';
    }

}
