package bo.transaction;

/**
 *
 * @author hongtt
 */
public class PurchaseMethod {

    private int id;
    private String type;

    public PurchaseMethod() {
    }

    public PurchaseMethod(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PurchaseMethod{" + "id=" + id + ", type=" + type + '}';
    }

}
