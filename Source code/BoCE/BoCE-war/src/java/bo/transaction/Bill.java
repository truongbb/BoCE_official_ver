package bo.transaction;

import java.util.Date;

/**
 *
 * @author hongtt
 * @editor truongbb on 16/02/2018: change modifier to protected
 */
public class Bill {

    protected int id;
    protected Date transactionDate;

    public Bill() {
    }

    public Bill(int id, Date transactionDate) {
        this.id = id;
        this.transactionDate = transactionDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", transactionDate=" + transactionDate + '}';
    }

}
