package bo.transaction;

import bo.common.person.Address;
import java.util.Date;
import bo.customer.Customer;

/**
 *
 * @author hongtt
 *
 * @editor truongbb on 19/02/2018: change some fields
 */
public class Order {

    private int id;
    private Customer customer;
    private Cart cart;
    private Address receivingAddress;
    private float additionalCharge;
    private Date expiredDate;
    private Date expectedReceivingDate;
    private float cash;
    private PurchaseMethod purchaseMethod;

    public Order() {
    }

    public Order(int id, Customer customer, Cart cart, Address receivingAddress, float additionalCharge, Date expiredDate, Date expectedReceivingDate, float cash, PurchaseMethod purchaseMethod) {
        this.id = id;
        this.customer = customer;
        this.cart = cart;
        this.receivingAddress = receivingAddress;
        this.additionalCharge = additionalCharge;
        this.expiredDate = expiredDate;
        this.expectedReceivingDate = expectedReceivingDate;
        this.cash = cash;
        this.purchaseMethod = purchaseMethod;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Address getReceivingAddress() {
        return receivingAddress;
    }

    public void setReceivingAddress(Address receivingAddress) {
        this.receivingAddress = receivingAddress;
    }

    public float getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(float additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getExpectedReceivingDate() {
        return expectedReceivingDate;
    }

    public void setExpectedReceivingDate(Date expectedReceivingDate) {
        this.expectedReceivingDate = expectedReceivingDate;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public PurchaseMethod getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(PurchaseMethod purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", customer=" + customer + ", cart=" + cart + ", receivingAddress=" + receivingAddress + ", additionalCharge=" + additionalCharge + ", expiredDate=" + expiredDate + ", expectedReceivingDate=" + expectedReceivingDate + ", cash=" + cash + ", purchaseMethod=" + purchaseMethod + '}';
    }

}
