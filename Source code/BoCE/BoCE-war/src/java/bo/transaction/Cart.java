package bo.transaction;

import bo.common.Pair;
import bo.product.Product;
import java.util.List;

/**
 *
 * @author hongtt
 */
public class Cart {

    private String userName;
    private List<Pair<Product, Integer>> productList;

    public Cart() {
    }

    public Cart(String userName, List<Pair<Product, Integer>> productList) {
        this.userName = userName;
        this.productList = productList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Pair<Product, Integer>> getProductList() {
        return productList;
    }

    public void setProductList(List<Pair<Product, Integer>> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "Cart{" + "userName=" + userName + ", productList=" + productList + '}';
    }

}
