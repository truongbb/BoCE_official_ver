package bo.transaction;

import bo.common.Pair;
import bo.employee.cashier.Cashier;
import bo.product.Product;
import java.util.Date;
import java.util.List;

/**
 *
 * @author hongtt
 */
public class OfflineBill extends Bill {

    private Cashier offlineCashier;
    private List<Pair<Product, Integer>> productList;
    private float additionalCharge;
    private float cash;

    public OfflineBill() {
    }

    public OfflineBill(Cashier offlineCashier, List<Pair<Product, Integer>> productList, float additionalCharge, float cash) {
        this.offlineCashier = offlineCashier;
        this.productList = productList;
        this.additionalCharge = additionalCharge;
        this.cash = cash;
    }

    public OfflineBill(Cashier offlineCashier, List<Pair<Product, Integer>> productList, float additionalCharge, float cash, int id, Date transactionDate) {
        super(id, transactionDate);
        this.offlineCashier = offlineCashier;
        this.productList = productList;
        this.additionalCharge = additionalCharge;
        this.cash = cash;
    }

    public Cashier getOfflineCashier() {
        return offlineCashier;
    }

    public void setOfflineCashier(Cashier offlineCashier) {
        this.offlineCashier = offlineCashier;
    }

    public List<Pair<Product, Integer>> getProductList() {
        return productList;
    }

    public void setProductList(List<Pair<Product, Integer>> productList) {
        this.productList = productList;
    }

    public float getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(float additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

}
