<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BoCE - login</title>
    </head>
    <body>
        <form class="login100-form validate-form" action="LoginClient" method="POST">
            <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                <span class="label-input100">Username</span>
                <input class="input100" type="text" name="username" placeholder="Enter username" value="${requestScope.username}">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                <span class="label-input100">Password</span>
                <input class="input100" type="password" name="pass" placeholder="Enter password" value="${requestScope.pass}">
                <span class="focus-input100"></span>
            </div>

<!--            <div class="flex-sb-m w-full p-b-30">
                <div class="contact100-form-checkbox">
                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                    <label class="label-checkbox100" for="ckb1">
                        Remember me
                    </label>
                </div>

                <div>
                    <a href="#" class="txt1">
                        Forgot Password?
                    </a>
                </div>
            </div>-->

            <div class="container-login100-form-btn">
                <button class="login100-form-btn" type="submit">
                    Login
                </button>
            </div>
            <div style="color: red;padding-top:5%;">${requestScope.error}</div>
        </form>
    </body>
</html>
