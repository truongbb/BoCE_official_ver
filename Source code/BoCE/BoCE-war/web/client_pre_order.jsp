<%@page import="java.util.List"%>
<%@page import="entities.transaction.order.Orders"%>
<%@page import="entities.transaction.order.OrdersProduct"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BoCE - Order</title>
    </head>

    <style>
        table, th, tr, td{
            border: 1px solid black;
        }
        th, td{
            width: 15px;
            text-align: center;
        }
    </style>
    <%
        List<OrdersProduct> cart = (List<OrdersProduct>) session.getAttribute("cart");
        Orders order = (Orders) session.getAttribute("order");
    %>
    <body>
        <form action="DoOrder" method="POST">
            <br>
            <br>
            <br>
            <p>ORDER</p>
            <br>
            <br>
            <br>
            PRODUCT
            <table>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Quantity</th>
                </tr>
                <c:set var="cart" value="<%=cart%>"/>
                <c:if test="${not empty cart}">
                    <c:forEach var="ordersProduct" items="${cart}" varStatus="counter">
                        <tr>
                            <td>${counter.index+1}</td>
                            <c:if test="${not empty ordersProduct.productId.clothes}">
                                <td style="width: 400px;">${ordersProduct.productId.clothes.name}</td>
                            </c:if>
                            <c:if test="${not empty ordersProduct.productId.electronics}">
                                <td style="width: 400px;">${ordersProduct.productId.electronics.name}</td>
                            </c:if>
                            <c:if test="${not empty ordersProduct.productId.book}">
                                <td style="width: 400px;">${ordersProduct.productId.book.name}</td>
                            </c:if>
                            <td>${ordersProduct.quantity}</td>
                        </tr>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <br>
            <br>
            Additional charge: ${sessionScope.order.additionalCharge}
            <br>
            Cash: ${sessionScope.order.cash}
            <br>
            Expected Receiving Date: ${sessionScope.order.expextedReceivingDate}
            <br>
            Customer's name: ${sessionScope.order.customerId.personId.firstName} ${sessionScope.order.customerId.personId.midName} ${sessionScope.order.customerId.personId.lastName}
            <br>
            <br>
            Receiving address: <input type="text" name="receivingAddress" placeholder="Receiving addess"/>
            <br>

            <select name="purchaseMethod">
                <option value="1">ATM card</option>
                <option value="2">Pay after receiving</option>
            </select>

            <br>
            <br>
            <br>
            <button type="submit">Order</button>
        </form>
    </body>
</html>
