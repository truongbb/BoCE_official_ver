<%@page import="java.util.List"%>
<%@page import="entities.transaction.order.OrdersProduct"%>
<%@page import="entities.product.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BoCE - Cart</title>
    </head>
    <style>
        table, th, tr, td{
            border: 1px solid black;
        }
        th, td{
            width: 15px;
            text-align: center;
        }
    </style>
    <%
        List<Product> products = (List<Product>) session.getAttribute("products");
        List<OrdersProduct> cart = (List<OrdersProduct>) session.getAttribute("cart");
    %>
    <body>
        <br>
        <br>
        <p>CART</p>

        <a href="PreOrder">
            <button type="button">Order</button>
        </a>

        <table>
            <tr>
                <th></th>
                <th>No.</th>
                <th>Name</th>
                <th>Quantity</th>
            </tr>
            <c:set var="cart" value="<%=cart%>"/>
            <c:if test="${not empty cart}">
                <c:forEach var="ordersProduct" items="${cart}" varStatus="counter">
                    <tr>
                        <td style="width: 100px;">
                            <a href="RemoveFromCart?productId=${ordersProduct.productId.productId}">
                                <button type="button">Remove</button>
                            </a>
                        </td>
                        <td>${counter.index+1}</td>
                        <c:if test="${not empty ordersProduct.productId.clothes}">
                            <td style="width: 400px;">${ordersProduct.productId.clothes.name}</td>
                        </c:if>
                        <c:if test="${not empty ordersProduct.productId.electronics}">
                            <td style="width: 400px;">${ordersProduct.productId.electronics.name}</td>
                        </c:if>
                        <c:if test="${not empty ordersProduct.productId.book}">
                            <td style="width: 400px;">${ordersProduct.productId.book.name}</td>
                        </c:if>
                        <td><input type="number" name="quantity" min="1" max="100" value="${ordersProduct.quantity}"></td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
        <br>
        <br>
        <br>
        <form action="SearchProduct" method="POST">
            Search product: 
            <input type="text" placeholder="product name" name="product-name"/>
            <br>
            <br>
            <input type="submit" value="Search"/>
        </form>

        <br>
        <br>
        <p>PRODUCT LIST</p>
        <table>
            <tr>
                <th></th>
                <th>No.</th>
                <th>Book</th>
                <th>Electronics</th>
                <th>Clothes</th>
                <th>Name</th>
                <th>Unit price</th>
                <th>Discount</th>
            </tr>
            <c:set var="products" value="<%=products%>"/>
            <c:if test="${not empty products}">
                <c:forEach var="product" items="${products}" varStatus="counter">
                    <tr>
                        <td style="width: 100px;">
                            <a href="AddToCart?productId=${product.productId}">
                                <button type="button">Add to cart</button>
                            </a>
                        </td>
                        <td>${counter.index+1}</td>
                        <c:if test="${not empty product.book}">
                            <td>x</td>
                        </c:if>
                        <c:if test="${empty product.book}">
                            <td></td>
                        </c:if>
                        <c:if test="${not empty product.electronics}">
                            <td>x</td>
                        </c:if>
                        <c:if test="${empty product.electronics}">
                            <td></td>
                        </c:if>
                        <c:if test="${not empty product.clothes}">
                            <td>x</td>
                        </c:if>
                        <c:if test="${empty product.clothes}">
                            <td></td>
                        </c:if>
                        <c:if test="${not empty product.clothes}">
                            <td style="width: 400px;">${product.clothes.name}</td>
                        </c:if>
                        <c:if test="${not empty product.electronics}">
                            <td style="width: 400px;">${product.electronics.name}</td>
                        </c:if>
                        <c:if test="${not empty product.book}">
                            <td style="width: 400px;">${product.book.name}</td>
                        </c:if>
                        <td>${product.unitPrice}</td>
                        <c:if test="${not empty product.discountId}">
                            <td>${product.discountId.rate}</td>
                        </c:if>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
    </body>
</html>
