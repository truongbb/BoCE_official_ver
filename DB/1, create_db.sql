create table customer_type(
    id int primary key,
    type nvarchar2(200) not null
);

create table gender(
    id int primary key,
    gender nvarchar2(100) not null
);

create table person(
    id int primary key,
    first_name nvarchar2(50) not null,
    mid_name nvarchar2(50),
    last_name nvarchar2(50) not null,
    birthday date not null,
    address nvarchar2(1000) not null,
    email nvarchar2(300),
    phone nvarchar2(15),
    gender_id int not null constraint gender_id references gender(id) on delete cascade
);

create table account(
    id int primary key,
    username nvarchar2(200) not null,
    password nvarchar2(200) not null,
    person_id int not null constraint person_id references person(id) on delete cascade
);

create table customer(
    id int primary key,
    customer_type_id int not null constraint customer_type_id references customer_type(id) on delete cascade,
    person_id int not null constraint customer_person_id unique references person(id) on delete cascade
);

create table department(
    id int primary key,
    name nvarchar2(500) not null,
    phone nvarchar2(15) not null
);

create table ethnic(
    id int primary key,
    value nvarchar2(100) not null
);

create table salary(
    salary_level int primary key,
    basic_salary float not null,
    salary_rate float not null,
    allowance_rate float not null
);

create table position(
    id int primary key,
    name nvarchar2(200) not null,
    salary_level int not null constraint salary_level references salary(salary_level) on delete cascade
);

create table literacy(
    id int primary key,
    value nvarchar2(100) not null
);

create table discipline_type(
    id int primary key,
    type nvarchar2(500) not null
);

create table discipline(
    id int primary key,
    name nvarchar2(1000) not null,
    discipline_type_id int not null constraint discipline_type_id references discipline_type(id) on delete cascade
);

create table reward_type(
    id int primary key,
    type nvarchar2(500) not null
);

create table reward(
    id int primary key,
    name nvarchar2(1000) not null,
    reward_type_id int not null constraint reward_type_id references reward_type(id) on delete cascade
);

create table employee(
    id int primary key,
    start_working_date date not null,
    literacy_id int not null constraint literacy_id references literacy(id) on delete cascade,
    position_id int not null constraint position_id references position(id) on delete cascade,
    department_id int not null constraint department_id references department(id) on delete cascade,
    ethnic_id int not null constraint ethnic_id references ethnic(id) on delete cascade,
    person_id int not null constraint employee_person_id unique references person(id) on delete cascade
);

create table employee_discipline(
    id int primary key,
    discipline_date date not null,
    description nvarchar2(1000) not null,
    forfeit float not null,
    discipline_id int not null constraint employee_discipline_id references discipline(id) on delete cascade,
    employee_id int not null constraint employee_discipline references employee(id) on delete cascade
);

create table employee_reward(
    id int primary key,
    reward_date date not null,
    description nvarchar2(1000) not null,
    bonus float not null,
    reward_id int not null constraint employee_reward_id references reward(id) on delete cascade,
    employee_id int not null constraint employee_reward references employee(id) on delete cascade
);

create table supplier(
    id int primary key,
    name nvarchar2(500) not null,
    address nvarchar2(500) not null,
    email nvarchar2(500) not null,
    phone nvarchar2(15) not null
);

create table unit(
    id int primary key,
    value nvarchar2(255) not null
);

create table product_type(
    id int primary key,
    type nvarchar2(500) not null
);

create table discount(
    id int primary key,
    rate float not null,
    begin_date date not null,
    end_date date not null
);

create table warehouse(
    id int primary key,
    code nvarchar2(50) not null,
    name nvarchar2(200) not null,
    loaction nvarchar2(500) not null,
    phone nvarchar2(15) not null
);

create table product(
    product_id int primary key,
    unit_price float not null,
    description nvarchar2(2000) not null,
    quantity int not null,
    picture_thumb nvarchar2(500),
    warehouse_id int not null constraint warehouse_id references warehouse(id) on delete cascade,
    discount_id int not null constraint discount_id references discount(id) on delete cascade,
    prodcut_type_id int not null constraint product_type_id references product_type(id) on delete cascade
);

create table importation_report(
    id int primary key,
    created_date date not null,
    note nvarchar2(1000) not null,
    employee_id int not null constraint employee_id references employee(id) on delete cascade
);

create table product_importation_report(
    id int primary key,
    importation_report_id int not null constraint importation_report_id references importation_report(id) on delete cascade,
    product_id int not null constraint importation_product_id references product(product_id) on delete cascade,
    unit_id int not null constraint importation_unit_id references unit(id) on delete cascade,
    supplier_id int not null constraint supplier_id references supplier(id) on delete cascade,
    imported_quantity int not null,
    unit_price float not null
);

create table exportation_report(
    id int primary key,
    created_date date not null,
    note nvarchar2(1000) not null,
    employee_id int not null constraint employee_exportation_report references employee(id) on delete cascade
);

create table product_exportation_report(
    id int primary key,
    exported_report_id int not null constraint exportation_report references exportation_report(id) on delete cascade,
    product_id int not null constraint exportation_product_id references product(product_id) on delete cascade,
    unit_id int not null constraint exportation_unit_id references unit(id) on delete cascade,
    exported_quantity int not null,
    unit_price float not null
);

create table color(
    id int primary key,
    code nvarchar2(50) not null,
    color nvarchar2(200) not null
);

create table style(
    id int primary key,
    style nvarchar2(2000) not null
);

create table clothes_category(
    id int primary key,
    name nvarchar2(500) not null
);

create table clothes_size(
    id int primary key,
    clothes_size nvarchar2(10) not null
);

create table clothes(
    clothes_id int primary key,
    name nvarchar2(500) not null,
    description nvarchar2(1000) not null,
    clothes_category_id int not null constraint clothes_category_id references clothes_category(id) on delete cascade,
    style_id int not null constraint style_id references style(id) on delete cascade,
    color_id int not null constraint color_id references color(id) on delete cascade,
    clothes_size_id int not null constraint clothes_size_id references clothes_size(id) on delete cascade,
    product_id int not null constraint product_id unique references product(product_id) on delete cascade
);

create table electronics_category(
    id int primary key,
    name nvarchar2(500) not null
);

create table electronics(
    electronics_id int primary key,
    name nvarchar2(500) not null,
    weight float not null,
    color_id int not null constraint electronics_color_id references color(id) on delete cascade,
    electronics_category_id int not null constraint electronics_category_id references electronics_category(id) on delete cascade,
    product_id int not null constraint electronics_product_id unique references product(product_id) on delete cascade
);

create table book_category(
    id int primary key,
    name nvarchar2(500) not null
);

create table author(
    id int primary key,
    name nvarchar2(500) not null,
    description nvarchar2(1000) not null
);

create table publisher(
    id int primary key,
    name nvarchar2(300) not null,
    phone nvarchar2(15) not null,
    email nvarchar2(200) not null,
    address nvarchar2(500) not null
);

create table book(
    book_id int primary key,
    name nvarchar2(500) not null,
    published_year int not null,
    total_page int not null,
    book_category_id int not null constraint book_category_id references book_category(id) on delete cascade,
    author_id int not null constraint author_id references author(id) on delete cascade,
    publisher_id int not null constraint publisher_id references publisher(id) on delete cascade,
    product_id int not null constraint book_product_id unique references product(product_id) on delete cascade
);

create table purchase_method(
    id int primary key,
    purchase_method nvarchar2(500) not null
);

create table orders(
    id int primary key,
    additional_charge float not null,
    cash float not null,
    receiving_address nvarchar2(500) not null,
    expexted_receiving_date date not null,
    purchase_method_id int not null constraint purchase_method references purchase_method(id) on delete cascade,
    customer_id int not null constraint customer_orders references customer(id) on delete cascade
);

create table orders_product(
    id int primary key,
    orders_id int not null constraint orders references orders(id) on delete cascade,
    product_id int not null constraint product references product(product_id) on delete cascade,
    quantity int not null
);

create table bill(
    id int primary key,
    employee_id int not null constraint employee_bill references employee(id) on delete cascade,
    transaction_date date not null
);

create table online_bill(
    online_bill_id int primary key,
    bill_id int not null constraint bill_online references bill(id) on delete cascade,
    order_id int not null constraint order_online references orders(id) on delete cascade,
    received_date date not null
);

create table offline_bill(
    offline_bill_id int primary key,
    bill_id int not null constraint bill_offline references bill(id) on delete cascade,
    additional_charge float not null,
    cash float not null
);

create table offline_bill_product(
    id int primary key,
    offline_bill_id int not null constraint product_offline_bill_id references offline_bill(offline_bill_id) on delete cascade,
    product_id int not null constraint product_offline_bill references product(product_id) on delete cascade,
    quantity int not null
);

commit;