insert into gender (id, gender) values (gender_seq.nextval, 'Nam');
insert into gender (id, gender) values (gender_seq.nextval, 'Nữ');
insert into gender (id, gender) values (gender_seq.nextval, 'Không xác định');

insert into customer_type (id, type) values (customer_type_seq.nextval, 'Thường');
insert into customer_type (id, type) values (customer_type_seq.nextval, 'Thân thiết');
insert into customer_type (id, type) values (customer_type_seq.nextval, 'VIP');

insert into department (id, name, phone) values (department_seq.nextval, 'Giám đốc', '0912345678');
insert into department (id, name, phone) values (department_seq.nextval, 'Tài chính', '0147852369');
insert into department (id, name, phone) values (department_seq.nextval, 'Bảo vệ', '0159372648');

insert into literacy (id, value) values (literacy_seq.nextval, 'Trung học phổ thông');
insert into literacy (id, value) values (literacy_seq.nextval, 'Đai học');
insert into literacy (id, value) values (literacy_seq.nextval, 'Cao đẳng');
insert into literacy (id, value) values (literacy_seq.nextval, 'Cao học');

insert into salary (salary_level, basic_salary, salary_rate, allowance_rate) values (salary_seq.nextval, 6000000, 1.0, 0.5);
insert into salary (salary_level, basic_salary, salary_rate, allowance_rate) values (salary_seq.nextval, 8500000, 1.2, 0.7);
insert into salary (salary_level, basic_salary, salary_rate, allowance_rate) values (salary_seq.nextval, 10000000, 1.5, 1);

insert into position (id, name, salary_level) values (position_seq.nextval, 'NV chính thức', 1);
insert into position (id, name, salary_level) values (position_seq.nextval, 'NV xuất sắc', 2);
insert into position (id, name, salary_level) values (position_seq.nextval, 'Giám đốc', 3);

insert into unit (id, value) values (unit_seq.nextval, 'Chiếc');
insert into unit (id, value) values (unit_seq.nextval, 'Cái');
insert into unit (id, value) values (unit_seq.nextval, 'Cuốn');

insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'S');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'M');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'L');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XL');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XXL');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XXXL');


insert into person (id, first_name, mid_name, last_name, birthday, address, email, phone, gender_id)
    values (person_seq.nextval, 'Bùi', 'Bá', 'Trường', to_date('19960108', 'YYYYMMDD'), 'Số 16 ngõ 5 Ao Sen, Mộ Lao, Hà Đông, Hà Nội', 'truongbb@itsol.vn', '01648954110', 1);
insert into person (id, first_name, mid_name, last_name, birthday, address, email, phone, gender_id)
    values (person_seq.nextval, 'Nguyễn', 'Văn', 'Nam', to_date('19940514', 'YYYYMMDD'), 'Số 37A, Đan Áo, Đan Phượng, Hà Nội', 'namnv@gmail.com', '0147852369', 1);
insert into person (id, first_name, mid_name, last_name, birthday, address, email, phone, gender_id)
    values (person_seq.nextval, 'Hoàng', 'Thị', 'Huế', to_date('19951203', 'YYYYMMDD'), 'Số 3, Mễ Trì, Mỹ Đình, Hà Nội', 'hueht@itsol.vn', '0245673189', 2);

insert into account (id, username, password, person_id) values (account_seq.nextval, 'truongbb', 'admin', 1);
insert into account (id, username, password, person_id) values (account_seq.nextval, 'namnv', '123456789', 2);
insert into account (id, username, password, person_id) values (account_seq.nextval, 'hueht', '123456789', 3);

insert into product_type (id, type) values (product_type_seq.nextval, 'Book');
insert into product_type (id, type) values (product_type_seq.nextval, 'Electronics');
insert into product_type (id, type) values (product_type_seq.nextval, 'Clothes');

insert into warehouse (id, code, name, location, phone)  values (warehouse_seq.nextval, 'HN01', 'Kho Hà Đông', 'Số 37, Xa La, Hà Đông, Hà Nội', '0145326987');
insert into warehouse (id, code, name, location, phone)  values (warehouse_seq.nextval, 'HN02', 'Kho Mỹ Đình', 'Số 2A, Mễ Trì Hạ, Mỹ Đình, Hà Nội', '0145326987');

insert into discount (id, rate, begin_date, end_date) values (discount_seq.nextval, 30.0, to_date('20180401', 'YYYYMMDD'), to_date('20180410', 'YYYYMMDD'));
insert into discount (id, rate, begin_date, end_date) values (discount_seq.nextval, 10.0, to_date('20180101', 'YYYYMMDD'), to_date('20181231', 'YYYYMMDD'));

insert into product (product_id, unit_price, description, quantity, picture_thumb, warehouse_id, discount_id, product_type_id)
    values (product_seq.nextval, 53200, 'Việt Nam Danh Tác - Thương Nhớ Mười Hai

Trong số tác phẩm của Vũ Bằng, Thương Nhớ Mười Hai là tác phẩm đặc sắc nhất, tiêu biểu cho tình cảm và phong cách viết của ông. Tác phẩm được đặt bút từ Tháng Giêng 1960 và mất mười một năm mới hoàn thành vào năm 1971 với độ dày 250 trang.

Mười hai ở đây là mười hai tháng trong năm mà theo lời tác giả “mỗi tháng lại có những cái đẹp não nùng riêng, nỗi nhớ nhung riêng…”. Thông qua mười hai tháng ấy, Vũ Bằng đã gởi gắm những hồi ức đẹp đẽ của mình về Hà Nội, nơi chốn xa xôi ông luôn hướng về với những phong tục của người Bắc Việt, những thói quen sinh hoạt, những thú vui ẩm thực giản dị mà đầy tính nghệ thuật và trên tất cả là hình bóng người vợ đảm đang dịu hiền đang còn xa cách…

Nhân vật tôi ở miền Nam mà lòng luôn nhớ thương miền Bắc. Nỗi lòng ấy da diết và khắc khoải tựa thanh gỗ mục, bề ngoài đẹp đẽ mà bên trong đã rệu nát tự bao giờ. Ông biết thế là bất công nhưng tình yêu mà, đã yêu thì bao giờ người mình yêu cũng là đẹp nhất. Ông còn thầm cảm ơn sự bất công ấy bởi nó đã cho ông nhận ra mình yêu Bắc Việt đến dường nào…mà càng yêu thì lại càng nhớ…',
5200, 'D:\University\Architect and software design\Assignments\BoCE (official ver.)\Images\Books\sach-thuong-nho-muoi-hai.jpg', 1, 1, 1);


insert into electronics_category (id, name) values (electronics_category_seq.nextval, 'PC');
insert into electronics_category (id, name) values (electronics_category_seq.nextval, 'Laptop');
insert into electronics_category (id, name) values (electronics_category_seq.nextval, 'Mouse');
insert into electronics_category (id, name) values (electronics_category_seq.nextval, 'Keyboard');

insert into color (id, color, code) values (color_seq.nextval, 'Silver', '#E5E7E9');
insert into color (id, color, code) values (color_seq.nextval, 'Brown', '#938A71');
insert into color (id, color, code) values (color_seq.nextval, 'Yellow', '#F9F965');

insert into clothes_category (id, name) values (clothes_category_seq.nextval, 'Bag');
insert into clothes_category (id, name) values (clothes_category_seq.nextval, 'Jean');
insert into clothes_category (id, name) values (clothes_category_seq.nextval, 'Sweater');
insert into clothes_category (id, name) values (clothes_category_seq.nextval, 'Shoes');

insert into style (id, style) values (style_seq.nextval, 'Fucking up style');

insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'S');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'M');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'L');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XL');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XXL');
insert into clothes_size (id, clothes_size) values (clothes_size_seq.nextval, 'XXXL');

insert into author (id, name, description) values (author_seq.nextval, 'Vũ Bằng', 'description of Vũ Bằng');

insert into book_category (id, name) values (book_category_seq.nextval, 'Văn học Việt Nam');
insert into book_category (id, name) values (book_category_seq.nextval, 'Tiểu thuyết');
insert into book_category (id, name) values (book_category_seq.nextval, 'Lịch sử');

insert into publisher (id, name, phone, email, address) values (publisher_seq.nextval, 'Nhã Nam', '0123456789', 'nhanampublisher@gmail.com', 'Hanoi');

commit;